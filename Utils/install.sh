sudo apt install python3 -y
sudo apt install python3-pip -y
sudo apt install python3-tk -y
sudo python3 -m pip install pyserial
sudo python3 -m pip install requests

sudo pip3 install arabic-reshaper
sudo pip3 install python-bidi
sudo pip3 install matplotlib
sudo pip3 install fpdf
sudo pip3 install xlsxwriter
sudo python3 pip -m install -U matplotlib
sudo pip3 install numpy --upgrade 
sudo apt install libatlas-base-dev -y
sudo cp $PWD/*.ttf /usr/local/share/fonts
fc-cache -f -v

sudo cp $PWD/zoufan.service /etc/systemd/system

sudo systemctl daemon-reload
sudo systemctl enable zoufan.service
sudo systemctl start zoufan.service
