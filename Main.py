from tkinter import *
from Globals import *
import Globals
from tkinter import ttk
import sqlite3
import serial
import serial.tools.list_ports
from time import sleep
import time
import ssl
from requests import Session, Request
from tkinter import messagebox
from tkinter.filedialog import asksaveasfilename, askopenfilename
from tkinter.colorchooser import askcolor

from matplotlib.figure import Figure
from matplotlib.backends.backend_tkagg import FigureCanvasTkAgg

import os, sys
import datetime

from fpdf import Template, FPDF

from bidi.algorithm import get_display
from arabic_reshaper import reshape
import xlsxwriter

import datetime
import json

os.environ["MATPLOTLIBDATA"] = os.path.join(os.path.split(sys.executable)[0], "Lib/site-packages/matplotlib/mpl-data")

ssl._create_default_https_context = ssl._create_unverified_context

root = None
DATABASE = None
HW = None
last_time = 0


class Simfa:
    def __init__(self):
        self.URL = 'http://s4.symfa.ir/api/ReceptionResult'

        # آزمایشگاه
        self.Username = 'test'
        self.Password = 'test'

        # تجهیز کننده
        self.Username2 = 'test'
        self.Password2 = 'test'

        self.LoginUrl = '/Login'
        self.SubmitResult = '/SubmitResult'
        self.IsConnect = False
        # self.database = database

        self.session = Session()
        self.tabel = (
            'YearProduction',
            'YearExpiration',
            'WeightFirst',
            'WeightSecond',
            'PressureValueNormal',
            'PressureTimeNormal',
            'PressureValueTest',
            'PressureTimeTest',
            'VolumeFirst',
            'VolumeSecond',
            'VolumeNominal',
            'DischargeDuration',
            'CylinderType',
            'CylinderNumber',
            'CylinderSN',
            'CylinderCompany',
            'CylinderImage',
            'ReceptionNumber',
            'IsLegible',
            'IsNoDamage',
            'IsNoSpecialDamage',
            'IsLeakTestOk',
            'IsThreadOk',
            'IsWallThicknessOk',
            'Username',
            'Password',
            'ChartPT',
            'ChartVT',
            'LineID'
        )

        self.data = {
            'YearProduction': '2001/01/02',
            'YearExpiration': '2001/01/02',
            'WeightFirst': 10,
            'WeightSecond': 10,
            'PressureValueNormal': 200,
            'PressureTimeNormal': 4,
            'PressureValueTest': 300,
            'PressureTimeTest': 4,
            'VolumeFirst': 0,
            'VolumeSecond': 10,
            'VolumeNominal': 100,
            'DischargeDuration': 4,
            'CylinderType': 1,
            'CylinderNumber': 10005,
            'CylinderSN': 109093,
            'CylinderCompany': 'GT',
            'CylinderImage': '',
            'ReceptionNumber': 1,
            'IsLegible': False,
            'IsNoDamage': False,
            'IsNoSpecialDamage': False,
            'IsLeakTestOk': False,
            'IsThreadOk': False,
            'IsWallThicknessOk': False,
            'Username': 'test',
            'Password': 'test',
            'ChartPT': [],
            'ChartVT': [],
            'LineID': 0
        }

        try:
            f = open(FILE_NAME.SimfaUser1, 'r', encoding='utf-8')
            txt = f.read()
            self.Username = txt.split('\n')[0]
            self.Password = txt.split('\n')[1]
            f.close()
        except Exception as ex:
            f = open(FILE_NAME.SimfaUser1, 'w', encoding='utf-8')
            f.write(self.Username + '\n')
            f.write(self.Password2)
            f.close()

        try:
            f = open(FILE_NAME.SimfaUser2, 'r', encoding='utf-8')
            txt = f.read()
            self.Username2 = txt.split('\n')[0]
            self.Password2 = txt.split('\n')[1]
            f.close()
        except Exception as ex:
            f = open(FILE_NAME.SimfaUser2, 'w', encoding='utf-8')
            self.Username2 = 'test'
            self.Password2 = 'test'
            f.write(self.Username2 + '\n')
            f.write(self.Password2)

    # self.connect_to_simfa()

    def connect_to_simfa(self):
        val = {"Username": self.Username2, 'Password': self.Password2}

        try:
            response = self.session.post(self.URL + self.LoginUrl, json=val)
            messagebox.showinfo('Simfa', cft(response.content.decode()))

            if response.status_code == 200:
                self.IsConnect = True
            else:
                self.IsConnect = False
        except:
            messagebox.showinfo('Simfa', cft('اینترنت خود را کنترل نمایید'))

    def send_simfa(self, data, id_number):

        try:
            for item in data:
                if item == 'YearProduction':
                    dd = int(data['YearProduction'].split('/')[2])
                    mm = int(data['YearProduction'].split('/')[1])
                    yy = int(data['YearProduction'].split('/')[0])
                    data['YearProduction'] = datetime.datetime(yy, mm, dd, 12, 0).__str__()
                    # print(data['YearProduction'])
                elif item == 'YearExpiration':
                    dd = int(data['YearExpiration'].split('/')[2])
                    mm = int(data['YearExpiration'].split('/')[1])
                    yy = int(data['YearExpiration'].split('/')[0])
                    data['YearExpiration'] = datetime.datetime(yy, mm, dd, 12, 0).__str__()
                elif item == 'CylinderType' or item == 'CylinderNumber' or item == 'DischargeDuration' or item == 'ReceptionNumber':
                    data[item] = int(data[item])
                elif item == 'Username' or item == 'Password' or item == 'CylinderSN' or item == 'CylinderCompany' or item == 'LineID':
                    data[item] = str(data[item])
                elif 'Is' in item:
                    if data[item] == 'True':
                        data[item] = True
                    else:
                        data[item] = False
                elif 'Chart' in item:
                    continue
                elif 'Image' in item:
                    data[item] = ''
                else:
                    data[item] = float(data[item])
            data['IsNoSpecialDamag'] = data['IsNoSpecialDamage']
            d = json.dumps(data)
            # payload = {'json_payload': d}
            headers = {'Content-type': 'application/json', 'Accept': 'text/plain'}
            print('H', d)
            response = self.session.post(self.URL + self.SubmitResult, data=d, headers=headers)

            messagebox.showinfo('Simfa', cft(response.content.decode()))

            f = open(FILE_NAME.SimfaFile + str(id_number) + '.HsS', 'w', encoding='utf-8')

            f.write('Test ID : {}\n'.format(id_number))
            f.write('Simfa Response : {}\n'.format(response.content.decode()))
            for item in data:
                st = "{} : {}\n".format(item, data[item])
                f.write(st)
            f.close()

            return response.content.decode()

        except Exception as ex:
            print(ex)
            messagebox.showerror('Inter Connection', cft('اینترنت خود را کنترل نمایید'))


class Setting:
    def __init__(self, window):
        global DATABASE
        self.root = window
        self.window = None

        self.setting_value = None
        self.setting_string = None
        self.setting_Entry = None
        self.setting_default = None

        self.TabelSelect = 'Simfa'
        self.TableFrame = None
        self.btnframe2 = None
        self.list = None
        self.vsb = None
        self.hsb = None

        self.simfa2_visible = False

        self.Setting_head = (
            'طول زمان محاسبه بدون ژاکت',
            'فشار برای شروع محاسبه بدون ژاکت',
            'طول زمان محاسبه با ژاکت',
            'فشار برای شروع محاسبه با ژاکت',
            'دقت ترازو بر حسب گرم',
            'زمان برای ذخیره عدد وزن برای مقایسه نشتی',
            'زمان مقایسه عدد وزن با وزن ذخیره شده',
            'طول زمان معکوس شمار اول',
            'مقیاس نمودار وزن بر حسب گرم',
            'فشار را تا این عدد صفر نشان دهد',
            'وزن را تا این عدد صفر نشان دهد',
            'طول زمان معکوس شمار دوم در فشار آزمون',
            'حداکثر فشار ‌ذخیره در دیتابیس',
            'طول زمان معکوس شمار نشتی سوم',
            'فشار تنظیمی جهت شروع نشتیابی سوم',
            'فشار سوپاپ اطمینان',
            'شروع شماره گزاری تست ها',
            'زمان شیر برقی',
        )
        self.Simfa_head = ('Username', 'Password')
        self.simfa1_data = [DATABASE.SimfaConnection.Username, DATABASE.SimfaConnection.Password]
        self.simfa2_data = [DATABASE.SimfaConnection.Username2, DATABASE.SimfaConnection.Password2]

        self.change_head = (
            'میزان مجاز تغییرات', 'تلرانس قطع پمپ', 'ضریب وزن', 'تلورانس وزن', 'ضریب فشار', 'تلورانس فشار',
            'تعداد نمایش', 'K', 'میزان آب درون سیلندر')
        self.change_data = [Globals.DEFAULT_WEIGHT_CHANGE, Globals.DEFAULT_PRESSURE_CHANGE, Globals.WEIGHT_ZARIB,
                            Globals.WEIGHT_TELO, Globals.PRESSURE_ZARIB, Globals.PRESSURE_TELO, Globals.SHOW_NUMBER,
                            Globals.DEFAULT_K, Globals.DEFAULT_WATER_WEIGHT]

        self.lab_head = ('نام آزمایشگاه', 'مدیر آزمایشگاه', 'مدیر فنی', 'تلفن', 'آدرس', 'لوگو')
        self.lab_data = [Globals.LAB_NAME, Globals.LAB_CEO, Globals.LAB_CTO, Globals.LAB_PHONE, Globals.LAB_ADDRESS,
                         Globals.LAB_LOGO]

    def simfa2_visible_enable(self, event):
        if self.simfa2_visible:
            messagebox.showinfo('Simfa 2', cft('عدم نمایش مشخصات تجهیز کننده'))
            self.simfa2_visible = False
        else:
            messagebox.showinfo('Simfa 2', cft('نمایش مشخصات تجهیز کننده'))
            self.simfa2_visible = True

    def create_window(self):
        global root
        self.window = Frame(self.root, bg=COLOR.CL1, height=WINDOWS_HEIGHT)
        self.window.pack(side='top', padx='5m', pady='1m', fill='x', expand='true')

        btnframe = Frame(self.window, bg=COLOR.CL1)
        btnframe.pack(side='top', padx='1m', pady='1m', fill='x')
        #
        # Button(btnframe, text=cft('تنظیمات اصلی'), bg=COLOR.CL1, fg=COLOR.CL2, font=FONT.PERSIAN_SMALL, width=10,
        #        command=self.create_list_setting).pack(
        #     side='right', padx='5m', pady='3m')
        #
        # Button(btnframe, text=cft('تنظیمات پورت'), bg=COLOR.CL1, fg=COLOR.CL2, font=FONT.PERSIAN_SMALL, width=10,
        #        command=self.create_list_port).pack(
        #     side='right', padx='5m', pady='3m')
        #
        # Button(btnframe, text=cft('آزمایشگاه'), bg=COLOR.CL1, fg=COLOR.CL2, font=FONT.PERSIAN_SMALL, width=10,
        #        command=self.create_list_simfa_1).pack(
        #     side='right', padx='5m', pady='3m')
        #
        # Button(btnframe, text=cft('مجاز تغییرات'), bg=COLOR.CL1, fg=COLOR.CL2, font=FONT.PERSIAN_SMALL, width=10,
        #        command=self.create_list_change).pack(
        #     side='right', padx='5m', pady='3m')
        #
        # Button(btnframe, text=cft('مشخصات آزمایشگاه'), bg=COLOR.CL1, fg=COLOR.CL2, font=FONT.PERSIAN_SMALL, width=15,
        #        command=self.create_list_lab).pack(
        #     side='right', padx='5m', pady='3m')
        #
        Button(btnframe, text=cft('تنظیمات اصلی'), bg=COLOR.CL1, fg=COLOR.CL2, font=FONT.PERSIAN_SMALL, width=10,
               command=self.add_window).pack(
            side='right', padx='5m', pady='3m')

        Button(btnframe, text=cft('تنظیمات پورت'), bg=COLOR.CL1, fg=COLOR.CL2, font=FONT.PERSIAN_SMALL, width=10,
               command=HW.create_connect_window).pack(
            side='right', padx='5m', pady='3m')

        Button(btnframe, text=cft('آزمایشگاه'), bg=COLOR.CL1, fg=COLOR.CL2, font=FONT.PERSIAN_SMALL, width=10,
               command=self.edit_simfa1).pack(
            side='right', padx='5m', pady='3m')

        Button(btnframe, text=cft('مجاز تغییرات'), bg=COLOR.CL1, fg=COLOR.CL2, font=FONT.PERSIAN_SMALL, width=10,
               command=self.edit_change).pack(
            side='right', padx='5m', pady='3m')

        Button(btnframe, text=cft('مشخصات آزمایشگاه'), bg=COLOR.CL1, fg=COLOR.CL2, font=FONT.PERSIAN_SMALL, width=15,
               command=self.edit_lab).pack(
            side='right', padx='5m', pady='3m')

        if self.simfa2_visible:
            Button(btnframe, text=cft('تجهیزکننده'), bg=COLOR.CL1, fg=COLOR.CL2, font=FONT.PERSIAN_SMALL, width=10,
                   command=self.edit_simfa2).pack(
                side='right', padx='5m', pady='3m')

        self.create_list_setting()

    def create_list_setting(self):
        if self.TableFrame is not None:
            self.TableFrame.destroy()
            self.TableFrame.pack_forget()
            self.TableFrame = None
            self.btnframe2.destroy()
            self.btnframe2.pack_forget()
            self.btnframe2 = None

        self.TabelSelect = 'setting'
        self.btnframe2 = Frame(self.window, bg=COLOR.CL1)
        self.btnframe2.pack(side='top', padx='1m', pady='1m', fill='x')

        Button(self.btnframe2, text=cft('ویرایش'), bg=COLOR.CL1, fg=COLOR.CL2, font=FONT.PERSIAN_SMALL, width=10,
               command=self.add_window).pack(
            side='right', padx='5m', pady='3m')

        style = ttk.Style()
        style.configure("mystyle.Treeview", highlightthickness=0, bd=0,
                        font=('Calibri', 11), rowheight=50)  # Modify the font of the body
        style.configure("mystyle.Treeview.Heading", font=('Calibri', 14, 'bold'))  # Modify the font of the headings
        style.layout("mystyle.Treeview", [('mystyle.Treeview.treearea', {'sticky': 'nswe'})])  # Remove the borders

        self.TableFrame = Frame(self.window, bg=COLOR.CL1)
        self.TableFrame.pack(side='top', fill='x')

        list = ttk.Treeview(self.TableFrame, selectmode='browse', show='headings', style="mystyle.Treeview", height=5)
        vsb = Scrollbar(self.TableFrame, orient='vertical', command=list.yview, width='10m')
        hsb = Scrollbar(self.TableFrame, orient='horizontal', command=list.xview, width='10m')

        list.configure(xscrollcommand=hsb.set)
        list.configure(yscrollcommand=vsb.set)
        vsb.pack(side='right', fill='y')
        list.pack(side='top', fill='both')
        hsb.pack(side='bottom', fill='x')

        set_head = []
        for item in self.Simfa_head:
            set_head.append(cft(item))
        list['columns'] = self.Setting_head
        # list['columns'] = set_head
        list.tag_configure('odd', background='#E8E8E8', font=FONT.ENGLISH_SMALL)
        list.tag_configure('even', background='#DFDFDF', font=FONT.ENGLISH_SMALL)

        for item in self.Setting_head:
            list.column(item, width=int(100 * Globals.ppm))
            list.heading(item, text=cft(item))
        data = DATABASE.setting_data[1:]
        list.insert('', 'end', text='item{}'.format(0), values=DATABASE.setting_data[0][1:], tags='odd')

    def create_list_simfa_1(self):
        if self.TableFrame is not None:
            self.TableFrame.destroy()
            self.TableFrame.pack_forget()
            self.TableFrame = None
            self.btnframe2.destroy()
            self.btnframe2.pack_forget()
            self.btnframe2 = None

        self.TabelSelect = 'setting'
        self.btnframe2 = Frame(self.window, bg=COLOR.CL1)
        self.btnframe2.pack(side='top', padx='1m', pady='1m', fill='x')

        Button(self.btnframe2, text=cft('ویرایش'), bg=COLOR.CL1, fg=COLOR.CL2, font=FONT.PERSIAN_SMALL, width=10,
               command=self.edit_simfa1).pack(
            side='right', padx='5m', pady='3m')

        style = ttk.Style()
        style.configure("mystyle.Treeview", highlightthickness=0, bd=0,
                        font=('Calibri', 11), rowheight=50)  # Modify the font of the body
        style.configure("mystyle.Treeview.Heading", font=('Calibri', 14, 'bold'))  # Modify the font of the headings
        style.layout("mystyle.Treeview", [('mystyle.Treeview.treearea', {'sticky': 'nswe'})])  # Remove the borders

        self.TableFrame = Frame(self.window, bg=COLOR.CL1)
        self.TableFrame.pack(side='top', fill='x')

        list = ttk.Treeview(self.TableFrame, selectmode='browse', show='headings', style="mystyle.Treeview", height=5)
        vsb = Scrollbar(self.TableFrame, orient='vertical', command=list.yview, width='10m')
        hsb = Scrollbar(self.TableFrame, orient='horizontal', command=list.xview, width='10m')

        list.configure(xscrollcommand=hsb.set)
        list.configure(yscrollcommand=vsb.set)
        vsb.pack(side='right', fill='y')
        list.pack(side='top', fill='both')
        hsb.pack(side='bottom', fill='x')

        list['columns'] = self.Simfa_head
        list.tag_configure('odd', background='#E8E8E8', font=FONT.ENGLISH_SMALL)
        list.tag_configure('even', background='#DFDFDF', font=FONT.ENGLISH_SMALL)

        for item in self.Simfa_head:
            list.column(item, width=int(100 * Globals.ppm))
            list.heading(item, text=item)
        data = DATABASE.setting_data[1:]
        self.simfa1_data = [DATABASE.SimfaConnection.Username, DATABASE.SimfaConnection.Password]
        list.insert('', 'end', text='item{}'.format(0), values=self.simfa1_data, tags='odd')

    def create_list_simfa_2(self):
        if self.TableFrame is not None:
            self.TableFrame.destroy()
            self.TableFrame.pack_forget()
            self.TableFrame = None
            self.btnframe2.destroy()
            self.btnframe2.pack_forget()
            self.btnframe2 = None

        self.TabelSelect = 'setting'
        self.btnframe2 = Frame(self.window, bg=COLOR.CL1)
        self.btnframe2.pack(side='top', padx='1m', pady='1m', fill='x')

        Button(self.btnframe2, text=cft('ویرایش'), bg=COLOR.CL1, fg=COLOR.CL2, font=FONT.PERSIAN_SMALL, width=10,
               command=self.edit_simfa2).pack(
            side='right', padx='5m', pady='3m')

        style = ttk.Style()
        style.configure("mystyle.Treeview", highlightthickness=0, bd=0,
                        font=('Calibri', 11), rowheight=50)  # Modify the font of the body
        style.configure("mystyle.Treeview.Heading", font=('Calibri', 14, 'bold'))  # Modify the font of the headings
        style.layout("mystyle.Treeview", [('mystyle.Treeview.treearea', {'sticky': 'nswe'})])  # Remove the borders

        self.TableFrame = Frame(self.window, bg=COLOR.CL1)
        self.TableFrame.pack(side='top', fill='x')

        list = ttk.Treeview(self.TableFrame, selectmode='browse', show='headings', style="mystyle.Treeview", height=5)
        vsb = Scrollbar(self.TableFrame, orient='vertical', command=list.yview, width='10m')
        hsb = Scrollbar(self.TableFrame, orient='horizontal', command=list.xview, width='10m')

        list.configure(xscrollcommand=hsb.set)
        list.configure(yscrollcommand=vsb.set)
        vsb.pack(side='right', fill='y')
        list.pack(side='top', fill='both')
        hsb.pack(side='bottom', fill='x')

        list['columns'] = self.Simfa_head
        list.tag_configure('odd', background='#E8E8E8', font=FONT.ENGLISH_SMALL)
        list.tag_configure('even', background='#DFDFDF', font=FONT.ENGLISH_SMALL)

        for item in self.Simfa_head:
            list.column(item, width=int(100 * Globals.ppm))
            list.heading(item, text=item)
        self.simfa2_data = [DATABASE.SimfaConnection.Username2, DATABASE.SimfaConnection.Password2]
        list.insert('', 'end', text='item{}'.format(0), values=self.simfa2_data, tags='odd')

    def create_list_change(self):
        if self.TableFrame is not None:
            self.TableFrame.destroy()
            self.TableFrame.pack_forget()
            self.TableFrame = None
            self.btnframe2.destroy()
            self.btnframe2.pack_forget()
            self.btnframe2 = None

        self.TabelSelect = 'setting'
        self.btnframe2 = Frame(self.window, bg=COLOR.CL1)
        self.btnframe2.pack(side='top', padx='1m', pady='1m', fill='x')

        Button(self.btnframe2, text=cft('ویرایش'), bg=COLOR.CL1, fg=COLOR.CL2, font=FONT.PERSIAN_SMALL, width=10,
               command=self.edit_change).pack(
            side='right', padx='5m', pady='3m')

        style = ttk.Style()
        style.configure("mystyle.Treeview", highlightthickness=0, bd=0,
                        font=('Calibri', 11), rowheight=50)  # Modify the font of the body
        style.configure("mystyle.Treeview.Heading", font=('Calibri', 14, 'bold'))  # Modify the font of the headings
        style.layout("mystyle.Treeview", [('mystyle.Treeview.treearea', {'sticky': 'nswe'})])  # Remove the borders

        self.TableFrame = Frame(self.window, bg=COLOR.CL1)
        self.TableFrame.pack(side='top', fill='x')

        list = ttk.Treeview(self.TableFrame, selectmode='browse', show='headings', style="mystyle.Treeview", height=5)
        vsb = Scrollbar(self.TableFrame, orient='vertical', command=list.yview, width='10m')
        hsb = Scrollbar(self.TableFrame, orient='horizontal', command=list.xview, width='10m')

        list.configure(xscrollcommand=hsb.set)
        list.configure(yscrollcommand=vsb.set)
        vsb.pack(side='right', fill='y')
        list.pack(side='top', fill='both')
        hsb.pack(side='bottom', fill='x')

        list['columns'] = self.change_head
        list.tag_configure('odd', background='#E8E8E8', font=FONT.ENGLISH_SMALL)
        list.tag_configure('even', background='#DFDFDF', font=FONT.ENGLISH_SMALL)

        for item in self.change_head:
            list.column(item, width=int(100 * Globals.ppm))
            list.heading(item, text=cft(item))
        # self.simfa2_data = [DATABASE.SimfaConnection.Username2, DATABASE.SimfaConnection.Password2]
        list.insert('', 'end', text='item{}'.format(0), values=self.change_data, tags='odd')

    def create_list_port(self):
        global HW
        if self.TableFrame is not None:
            self.TableFrame.destroy()
            self.TableFrame.pack_forget()
            self.TableFrame = None
            self.btnframe2.destroy()
            self.btnframe2.pack_forget()
            self.btnframe2 = None

        self.TabelSelect = 'setting'
        self.btnframe2 = Frame(self.window, bg=COLOR.CL1)
        self.btnframe2.pack(side='top', padx='1m', pady='1m', fill='x')

        Button(self.btnframe2, text=cft('ویرایش'), bg=COLOR.CL1, fg=COLOR.CL2, font=FONT.PERSIAN_SMALL, width=10,
               command=HW.create_connect_window).pack(
            side='right', padx='5m', pady='3m')

        style = ttk.Style()
        style.configure("mystyle.Treeview", highlightthickness=0, bd=0,
                        font=('Calibri', 11), rowheight=50)  # Modify the font of the body
        style.configure("mystyle.Treeview.Heading", font=('Calibri', 14, 'bold'))  # Modify the font of the headings
        style.layout("mystyle.Treeview", [('mystyle.Treeview.treearea', {'sticky': 'nswe'})])  # Remove the borders

        self.TableFrame = Frame(self.window, bg=COLOR.CL1)
        self.TableFrame.pack(side='top', fill='x')

        list = ttk.Treeview(self.TableFrame, selectmode='browse', show='headings', style="mystyle.Treeview", height=5)
        vsb = Scrollbar(self.TableFrame, orient='vertical', command=list.yview, width='10m')
        hsb = Scrollbar(self.TableFrame, orient='horizontal', command=list.xview, width='10m')

        list.configure(xscrollcommand=hsb.set)
        list.configure(yscrollcommand=vsb.set)
        vsb.pack(side='right', fill='y')
        list.pack(side='top', fill='both')
        hsb.pack(side='bottom', fill='x')

        list['columns'] = ('PortName', 'Baudrate')
        list.tag_configure('odd', background='#E8E8E8', font=FONT.ENGLISH_SMALL)
        list.tag_configure('even', background='#DFDFDF', font=FONT.ENGLISH_SMALL)

        for item in ('PortName', 'Baudrate'):
            list.column(item, width=int(100 * Globals.ppm))
            list.heading(item, text=item)
        data = [HW.port_name, HW.baudrate]
        list.insert('', 'end', text='item{}'.format(0), values=data, tags='odd')

    def create_list_lab(self):
        if self.TableFrame is not None:
            self.TableFrame.destroy()
            self.TableFrame.pack_forget()
            self.TableFrame = None
            self.btnframe2.destroy()
            self.btnframe2.pack_forget()
            self.btnframe2 = None

        self.TabelSelect = 'setting'
        self.btnframe2 = Frame(self.window, bg=COLOR.CL1)
        self.btnframe2.pack(side='top', padx='1m', pady='1m', fill='x')

        Button(self.btnframe2, text=cft('ویرایش'), bg=COLOR.CL1, fg=COLOR.CL2, font=FONT.PERSIAN_SMALL, width=10,
               command=self.edit_lab).pack(
            side='right', padx='5m', pady='3m')

        style = ttk.Style()
        style.configure("mystyle.Treeview", highlightthickness=0, bd=0,
                        font=('Calibri', 11), rowheight=50)  # Modify the font of the body
        style.configure("mystyle.Treeview.Heading", font=('Calibri', 14, 'bold'))  # Modify the font of the headings
        style.layout("mystyle.Treeview", [('mystyle.Treeview.treearea', {'sticky': 'nswe'})])  # Remove the borders

        self.TableFrame = Frame(self.window, bg=COLOR.CL1)
        self.TableFrame.pack(side='top', fill='x')

        list = ttk.Treeview(self.TableFrame, selectmode='browse', show='headings', style="mystyle.Treeview", height=5)
        vsb = Scrollbar(self.TableFrame, orient='vertical', command=list.yview, width='10m')
        hsb = Scrollbar(self.TableFrame, orient='horizontal', command=list.xview, width='10m')

        list.configure(xscrollcommand=hsb.set)
        list.configure(yscrollcommand=vsb.set)
        vsb.pack(side='right', fill='y')
        list.pack(side='top', fill='both')
        hsb.pack(side='bottom', fill='x')

        list['columns'] = self.lab_head
        list.tag_configure('odd', background='#E8E8E8', font=FONT.ENGLISH_SMALL)
        list.tag_configure('even', background='#DFDFDF', font=FONT.ENGLISH_SMALL)

        for item in self.lab_head:
            list.column(item, width=int(100 * Globals.ppm))
            list.heading(item, text=cft(item))
        # self.simfa2_data = [DATABASE.SimfaConnection.Username2, DATABASE.SimfaConnection.Password2]
        l_d = []
        for item in self.lab_data:
            l_d.append(cft(item))
        list.insert('', 'end', text='item{}'.format(0), values=l_d, tags='odd')

    def edit_simfa1(self, event=None):
        global root, HW

        win = Toplevel(root)
        win.geometry('+100+100')
        win.config(bg=COLOR.CL1)
        win.title("Simfa1")

        entry = []
        var = [StringVar(), StringVar()]
        var[0].set(self.simfa1_data[0])
        var[1].set(self.simfa1_data[1])
        Label(win, text='UserName : ', font=FONT.ENGLISH_BOLD, width=15, bg=COLOR.CL1, fg=COLOR.CL2).grid(row=0,
                                                                                                          column=0,
                                                                                                          padx='5m',
                                                                                                          pady='5m')
        entry.append(Entry(win, width=15, textvariable=var[0], bg=COLOR.CL1, fg=COLOR.CL2, font=FONT.ENGLISH_BOLD))
        entry[0].grid(row=0, column=1, padx='5m', pady='5m')
        Label(win, text='PassWord : ', font=FONT.ENGLISH_BOLD, width=15, bg=COLOR.CL1, fg=COLOR.CL2).grid(row=1,
                                                                                                          column=0,
                                                                                                          padx='5m',
                                                                                                          pady='5m')
        entry.append(Entry(win, width=15, textvariable=var[1], bg=COLOR.CL1, fg=COLOR.CL2, font=FONT.ENGLISH_BOLD))
        entry[1].grid(row=1, column=1, padx='5m', pady='5m')

        Button(win, text='OK', bg=COLOR.CL1, fg=COLOR.CL2, width=15, font=FONT.ENGLISH_SMALL,
               command=lambda: self.ok_simfa1(win, var)).grid(row=2, column=0,
                                                              padx='5m', pady='5m')
        Button(win, text='Cancel', bg=COLOR.CL1, fg=COLOR.CL2, width=15, font=FONT.ENGLISH_SMALL,
               command=lambda: self.cancel_btn(win)).grid(row=2, column=1,
                                                          padx='5m',
                                                          pady='5m')

    def edit_simfa2(self, event=None):
        global root, HW

        win = Toplevel(root)
        win.geometry('+100+100')
        win.config(bg=COLOR.CL1)
        win.title("Simfa2")

        entry = []
        var = [StringVar(), StringVar()]
        var[0].set(self.simfa2_data[0])
        var[1].set(self.simfa2_data[1])
        Label(win, text='UserName : ', font=FONT.ENGLISH_BOLD, width=15, bg=COLOR.CL1, fg=COLOR.CL2).grid(row=0,
                                                                                                          column=0,
                                                                                                          padx='5m',
                                                                                                          pady='5m')
        entry.append(Entry(win, width=15, textvariable=var[0], bg=COLOR.CL1, fg=COLOR.CL2, font=FONT.ENGLISH_BOLD))
        entry[0].grid(row=0, column=1, padx='5m', pady='5m')
        Label(win, text='PassWord : ', font=FONT.ENGLISH_BOLD, width=15, bg=COLOR.CL1, fg=COLOR.CL2).grid(row=1,
                                                                                                          column=0,
                                                                                                          padx='5m',
                                                                                                          pady='5m')
        entry.append(Entry(win, width=15, textvariable=var[1], bg=COLOR.CL1, fg=COLOR.CL2, font=FONT.ENGLISH_BOLD))
        entry[1].grid(row=1, column=1, padx='5m', pady='5m')

        Button(win, text='OK', bg=COLOR.CL1, fg=COLOR.CL2, width=15, font=FONT.ENGLISH_SMALL,
               command=lambda: self.ok_simfa2(win, var)).grid(row=2, column=0,
                                                              padx='5m', pady='5m')
        Button(win, text='Cancel', bg=COLOR.CL1, fg=COLOR.CL2, width=15, font=FONT.ENGLISH_SMALL,
               command=lambda: self.cancel_btn(win)).grid(row=2, column=1,
                                                          padx='5m',
                                                          pady='5m')

    def edit_change(self, event=None):
        global root, HW

        win = Toplevel(root)
        win.geometry('+100+100')
        win.config(bg=COLOR.CL1)
        win.title("Change")

        entry = []
        var = [StringVar(), StringVar(), StringVar(), StringVar(), StringVar(), StringVar(), StringVar(), StringVar(),
               StringVar()]
        var[0].set(str(self.change_data[0]))
        var[1].set(str(self.change_data[1]))
        var[2].set(str(self.change_data[2]))
        var[3].set(str(self.change_data[3]))
        var[4].set(str(self.change_data[4]))
        var[5].set(str(self.change_data[5]))
        var[6].set(str(self.change_data[6]))
        var[7].set(str(self.change_data[7]))
        var[8].set(str(self.change_data[8]))

        Label(win, text=cft('میزان مجاز تغییرات : '), font=FONT.ENGLISH_BOLD, width=15, bg=COLOR.CL1,
              fg=COLOR.CL2).grid(
            row=0,
            column=0,
            padx='5m',
            pady='5m')
        entry.append(Entry(win, width=15, textvariable=var[0], bg=COLOR.CL1, fg=COLOR.CL2, font=FONT.ENGLISH_BOLD))
        entry[0].grid(row=0, column=1, padx='5m', pady='5m')

        Label(win, text=cft('تلرانس قطع پمپ : '), font=FONT.ENGLISH_BOLD, width=15, bg=COLOR.CL1, fg=COLOR.CL2).grid(
            row=1,
            column=0,
            padx='5m',
            pady='5m')
        entry.append(Entry(win, width=15, textvariable=var[1], bg=COLOR.CL1, fg=COLOR.CL2, font=FONT.ENGLISH_BOLD))
        entry[1].grid(row=1, column=1, padx='5m', pady='5m')

        Label(win, text=cft('ضریب وزن : '), font=FONT.ENGLISH_BOLD, width=15, bg=COLOR.CL1, fg=COLOR.CL2).grid(
            row=2,
            column=0,
            padx='5m',
            pady='5m')
        entry.append(Entry(win, width=15, textvariable=var[2], bg=COLOR.CL1, fg=COLOR.CL2, font=FONT.ENGLISH_BOLD))
        entry[2].grid(row=2, column=1, padx='5m', pady='5m')

        Label(win, text=cft('تلرانس وزن : '), font=FONT.ENGLISH_BOLD, width=15, bg=COLOR.CL1, fg=COLOR.CL2).grid(
            row=3,
            column=0,
            padx='5m',
            pady='5m')
        entry.append(Entry(win, width=15, textvariable=var[3], bg=COLOR.CL1, fg=COLOR.CL2, font=FONT.ENGLISH_BOLD))
        entry[3].grid(row=3, column=1, padx='5m', pady='5m')

        Label(win, text=cft('ضریب فشار : '), font=FONT.ENGLISH_BOLD, width=15, bg=COLOR.CL1, fg=COLOR.CL2).grid(
            row=4,
            column=0,
            padx='5m',
            pady='5m')
        entry.append(Entry(win, width=15, textvariable=var[4], bg=COLOR.CL1, fg=COLOR.CL2, font=FONT.ENGLISH_BOLD))
        entry[4].grid(row=4, column=1, padx='5m', pady='5m')

        Label(win, text=cft('تلرانس فشار : '), font=FONT.ENGLISH_BOLD, width=15, bg=COLOR.CL1, fg=COLOR.CL2).grid(
            row=5,
            column=0,
            padx='5m',
            pady='5m')
        entry.append(Entry(win, width=15, textvariable=var[5], bg=COLOR.CL1, fg=COLOR.CL2, font=FONT.ENGLISH_BOLD))
        entry[5].grid(row=5, column=1, padx='5m', pady='5m')

        Label(win, text=cft('تعداد نمایش : '), font=FONT.ENGLISH_BOLD, width=15, bg=COLOR.CL1, fg=COLOR.CL2).grid(
            row=6,
            column=0,
            padx='5m',
            pady='5m')
        entry.append(Entry(win, width=15, textvariable=var[6], bg=COLOR.CL1, fg=COLOR.CL2, font=FONT.ENGLISH_BOLD))
        entry[6].grid(row=6, column=1, padx='5m', pady='5m')

        Label(win, text=cft('K : '), font=FONT.ENGLISH_BOLD, width=15, bg=COLOR.CL1, fg=COLOR.CL2).grid(
            row=7,
            column=0,
            padx='5m',
            pady='5m')
        entry.append(Entry(win, width=15, textvariable=var[7], bg=COLOR.CL1, fg=COLOR.CL2, font=FONT.ENGLISH_BOLD))
        entry[7].grid(row=7, column=1, padx='5m', pady='5m')

        Label(win, text=cft('میزان آب درون سیلندر :'), font=FONT.ENGLISH_BOLD, width=15, bg=COLOR.CL1,
              fg=COLOR.CL2).grid(
            row=8,
            column=0,
            padx='5m',
            pady='5m')
        entry.append(Entry(win, width=15, textvariable=var[8], bg=COLOR.CL1, fg=COLOR.CL2, font=FONT.ENGLISH_BOLD))
        entry[8].grid(row=8, column=1, padx='5m', pady='5m')

        Button(win, text='OK', bg=COLOR.CL1, fg=COLOR.CL2, width=15, font=FONT.ENGLISH_SMALL,
               command=lambda: self.ok_change(win, var)).grid(row=9, column=0,
                                                              padx='5m', pady='5m')
        Button(win, text='Cancel', bg=COLOR.CL1, fg=COLOR.CL2, width=15, font=FONT.ENGLISH_SMALL,
               command=lambda: self.cancel_btn(win)).grid(row=9, column=1,
                                                          padx='5m',
                                                          pady='5m')

    def edit_lab(self, event=None):
        global root, HW

        win = Toplevel(root)
        win.geometry('+100+100')
        win.config(bg=COLOR.CL1)
        win.title("LAB")

        entry = []
        var = []
        for num, item in enumerate(self.lab_head):
            var.append(StringVar())
            var[num].set(self.lab_data[num])
            Label(win, text=cft(item), font=FONT.ENGLISH_BOLD, width=15, bg=COLOR.CL1, fg=COLOR.CL2).grid(
                row=num,
                column=0,
                padx='5m',
                pady='5m')
            if 'لوگو' in item:
                entry.append(
                    Button(win, text=cft('باز کردن'), font=FONT.PERSIAN_SMALL, width=15, bg=COLOR.CL1, fg=COLOR.CL2,
                           activebackground=COLOR.CL1, command=self.logo_lab_open))
                entry[num].grid(row=num, column=1, padx='5m', pady='5m')
            else:
                entry.append(
                    Entry(win, width=15, textvariable=var[num], bg=COLOR.CL1, fg=COLOR.CL2, font=FONT.ENGLISH_BOLD))
                entry[num].grid(row=num, column=1, padx='5m', pady='5m')

        Button(win, text='OK', bg=COLOR.CL1, fg=COLOR.CL2, width=15, font=FONT.ENGLISH_SMALL,
               command=lambda: self.ok_lab(win, var)).grid(row=6, column=0,
                                                           padx='5m', pady='5m')

        Button(win, text='Cancel', bg=COLOR.CL1, fg=COLOR.CL2, width=15, font=FONT.ENGLISH_SMALL,
               command=lambda: self.cancel_btn(win)).grid(row=6, column=1,
                                                          padx='5m',
                                                          pady='5m')

    def logo_lab_open(self):
        filename = askopenfilename(initialdir="/", title="Select file", filetypes=[("png files", "*.png")])
        Globals.LAB_LOGO = filename
        print(filename)

    def cancel_btn(self, win):
        win.destroy()

    def ok_simfa1(self, win, var):
        global DATABASE
        win.destroy()
        DATABASE.SimfaConnection.Username = var[0].get()
        DATABASE.SimfaConnection.Password = var[1].get()

        f = open(FILE_NAME.SimfaUser1, 'w', encoding='utf-8')
        st = '{}\n{}'.format(DATABASE.SimfaConnection.Username, DATABASE.SimfaConnection.Password)
        f.write(st)
        f.close()
        self.create_list_simfa_1()

    def ok_simfa2(self, win, var):
        global DATABASE
        win.destroy()
        DATABASE.SimfaConnection.Username2 = var[0].get()
        DATABASE.SimfaConnection.Password2 = var[1].get()

        f = open(FILE_NAME.SimfaUser2, 'w', encoding='utf-8')
        st = '{}\n{}'.format(DATABASE.SimfaConnection.Username2, DATABASE.SimfaConnection.Password2)
        f.write(st)
        f.close()
        self.create_list_simfa_2()

    def ok_change(self, win, var):
        global DATABASE

        try:
            int(var[0].get())
        except:
            messagebox.showerror('wrong value', cft('میزان مجاز تغییرات را با فرمت درست وارد کنید '),
                                 parent=win)
            return

        try:
            int(var[1].get())
        except:
            messagebox.showerror('wrong value', cft('تلرانس قطع پمپ را با فرمت درست وارد کنید '),
                                 parent=win)
            return
        try:
            float(var[2].get())
        except:
            messagebox.showerror('wrong value', cft('ضریب وزن را با فرمت درست وارد کنید '),
                                 parent=win)
            return
        try:
            float(var[3].get())
        except:
            messagebox.showerror('wrong value', cft('تلرانس وزن را با فرمت درست وارد کنید '),
                                 parent=win)
            return
        try:
            float(var[4].get())
        except:
            messagebox.showerror('wrong value', cft('tضریب فشار را با فرمت درست وارد کنید '),
                                 parent=win)
            return
        try:
            float(var[5].get())
        except:
            messagebox.showerror('wrong value', cft('تلرانس فشار را با فرمت درست وارد کنید '),
                                 parent=win)
            return

        try:
            int(var[6].get())
        except:
            messagebox.showerror('wrong value', cft('تعداد نمایش را با فرمت درست وارد کنید '),
                                 parent=win)
            return

        try:
            float(var[7].get())
        except:
            messagebox.showerror('wrong value', cft('مقدار K را با فرمت درست وارد کنید '),
                                 parent=win)
            return

        try:
            float(var[8].get())
        except:
            messagebox.showerror('wrong value', cft('میزان آب درون سیلندر را با فرمت درست وارد کنید '),
                                 parent=win)
            return

        win.destroy()
        Globals.DEFAULT_WEIGHT_CHANGE = var[0].get()
        Globals.DEFAULT_PRESSURE_CHANGE = var[1].get()
        Globals.WEIGHT_ZARIB = float(var[2].get())
        Globals.WEIGHT_TELO = float(var[3].get())
        Globals.PRESSURE_ZARIB = float(var[4].get())
        Globals.PRESSURE_TELO = float(var[5].get())
        Globals.SHOW_NUMBER = int(var[6].get())
        Globals.DEFAULT_K = var[7].get()
        Globals.DEFAULT_WATER_WEIGHT = var[8].get()

        self.change_data = [Globals.DEFAULT_WEIGHT_CHANGE, Globals.DEFAULT_PRESSURE_CHANGE, Globals.WEIGHT_ZARIB,
                            Globals.WEIGHT_TELO, Globals.PRESSURE_ZARIB, Globals.PRESSURE_TELO, Globals.SHOW_NUMBER,
                            Globals.DEFAULT_K, Globals.DEFAULT_WATER_WEIGHT]

        f = open(FILE_NAME.DefaultChange, 'w', encoding='utf-8')
        st = '{}\n{}\n{}\n{}\n{}\n{}\n{}\n{}\n{}'.format(Globals.DEFAULT_WEIGHT_CHANGE, Globals.DEFAULT_PRESSURE_CHANGE,
                                                         Globals.WEIGHT_ZARIB, Globals.WEIGHT_TELO,
                                                         Globals.PRESSURE_ZARIB,
                                                         Globals.PRESSURE_TELO, Globals.SHOW_NUMBER, Globals.DEFAULT_K,
                                                         Globals.DEFAULT_WATER_WEIGHT)
        f.write(st)
        f.close()
        self.create_list_change()

    def ok_lab(self, win, var):
        global DATABASE
        win.destroy()
        Globals.LAB_NAME = var[0].get()
        Globals.LAB_CEO = var[1].get()
        Globals.LAB_CTO = var[2].get()
        Globals.LAB_PHONE = var[3].get()
        Globals.LAB_ADDRESS = var[4].get()

        self.lab_data = []
        self.lab_data = [Globals.LAB_NAME, Globals.LAB_CEO, Globals.LAB_CTO, Globals.LAB_PHONE, Globals.LAB_ADDRESS,
                         Globals.LAB_LOGO]

        f = open(FILE_NAME.LabConfig, 'w', encoding='utf-8')
        st = '{}\n{}\n{}\n{}\n{}\n{}'.format(Globals.LAB_NAME, Globals.LAB_CEO, Globals.LAB_CTO, Globals.LAB_PHONE,
                                             Globals.LAB_ADDRESS, Globals.LAB_LOGO)
        f.write(st)
        f.close()
        self.create_list_lab()

    def add_window(self, event=None):
        global root, DATABASE
        window = Toplevel(root, bg=COLOR.CL1)
        window.geometry('+100+100')
        window.title('Setting')
        self.setting_value = {
            'setting1': StringVar(),
            'setting2': StringVar(),
            'setting3': StringVar(),
            'setting4': StringVar(),
            'setting5': StringVar(),
            'setting6': StringVar(),
            'setting7': StringVar(),
            'setting8': StringVar(),
            'setting9': StringVar(),
            'setting10': StringVar(),
            'setting11': StringVar(),
            'setting12': StringVar(),
            'setting13': StringVar(),
            'setting14': StringVar(),
            'setting15': StringVar(),
            'setting16': StringVar(),
            'setting17': StringVar(),
            'setting18': StringVar()
        }
        self.setting_string = {
            'setting1': 'طول زمان محاسبه بدون ژاکت',
            'setting2': 'فشار برای شروع محاسبه بدون ژاکت',
            'setting3': 'طول زمان محاسبه با ژاکت',
            'setting4': 'فشار برای شروع محاسبه با ژاکت',
            'setting5': 'دقت ترازو بر حسب گرم',
            'setting6': 'زمان برای ذخیره عدد وزن برای مقایسه نشتی',
            'setting7': 'زمان مقایسه عدد وزن با وزن ذخیره شده و وصل کنتاکتور',
            'setting8': 'طول زمان معکوس شمار اول',
            'setting9': 'مقیاس نمودار وزن بر حسب گرم',
            'setting10': 'فشار را تا این عدد صفر نشان دهد',
            'setting11': 'وزن را تا این عدد صفر نشان دهد',
            'setting12': 'طول زمان معکوس شمار دوم در فشار آزمون',
            'setting13': 'حداکثر فشار ذخیره در دیتابیس',
            'setting14': 'طول زمان معکوس شمار نشتی سوم',
            'setting15': 'فشار تنظیمی جهت شروع نشتیابی سوم ',
            'setting16': 'فشار باز شدن کنتاکتور دوم - سوپاپ اطمینان',
            'setting17': 'شروع شماره گذاری تست ها',
            'setting18': 'زمان شیر برقی',

        }
        self.setting_Entry = {
            'setting1': Entry(window, bg=COLOR.CL1, bd=1, fg=COLOR.CL2, width=10, font=FONT.ENGLISH_SMALL,
                              insertbackground=COLOR.CL2),
            'setting2': Entry(window, bg=COLOR.CL1, bd=1, fg=COLOR.CL2, width=10, font=FONT.ENGLISH_SMALL,
                              insertbackground=COLOR.CL2),
            'setting3': Entry(window, bg=COLOR.CL1, bd=1, fg=COLOR.CL2, width=10, font=FONT.ENGLISH_SMALL,
                              insertbackground=COLOR.CL2),
            'setting4': Entry(window, bg=COLOR.CL1, bd=1, fg=COLOR.CL2, width=10, font=FONT.ENGLISH_SMALL,
                              insertbackground=COLOR.CL2),
            'setting5': Entry(window, bg=COLOR.CL1, bd=1, fg=COLOR.CL2, width=10, font=FONT.ENGLISH_SMALL,
                              insertbackground=COLOR.CL2),
            'setting6': Entry(window, bg=COLOR.CL1, bd=1, fg=COLOR.CL2, width=10, font=FONT.ENGLISH_SMALL,
                              insertbackground=COLOR.CL2),
            'setting7': Entry(window, bg=COLOR.CL1, bd=1, fg=COLOR.CL2, width=10, font=FONT.ENGLISH_SMALL,
                              insertbackground=COLOR.CL2),
            'setting8': Entry(window, bg=COLOR.CL1, bd=1, fg=COLOR.CL2, width=10, font=FONT.ENGLISH_SMALL,
                              insertbackground=COLOR.CL2),
            'setting9': Entry(window, bg=COLOR.CL1, bd=1, fg=COLOR.CL2, width=10, font=FONT.ENGLISH_SMALL,
                              insertbackground=COLOR.CL2),
            'setting10': Entry(window, bg=COLOR.CL1, bd=1, fg=COLOR.CL2, width=10, font=FONT.ENGLISH_SMALL,
                               insertbackground=COLOR.CL2),
            'setting11': Entry(window, bg=COLOR.CL1, bd=1, fg=COLOR.CL2, width=10, font=FONT.ENGLISH_SMALL,
                               insertbackground=COLOR.CL2),
            'setting12': Entry(window, bg=COLOR.CL1, bd=1, fg=COLOR.CL2, width=10, font=FONT.ENGLISH_SMALL,
                               insertbackground=COLOR.CL2),
            'setting13': Entry(window, bg=COLOR.CL1, bd=1, fg=COLOR.CL2, width=10, font=FONT.ENGLISH_SMALL,
                               insertbackground=COLOR.CL2),
            'setting14': Entry(window, bg=COLOR.CL1, bd=1, fg=COLOR.CL2, width=10, font=FONT.ENGLISH_SMALL,
                               insertbackground=COLOR.CL2),
            'setting15': Entry(window, bg=COLOR.CL1, bd=1, fg=COLOR.CL2, width=10, font=FONT.ENGLISH_SMALL,
                               insertbackground=COLOR.CL2),
            'setting16': Entry(window, bg=COLOR.CL1, bd=1, fg=COLOR.CL2, width=10, font=FONT.ENGLISH_SMALL,
                               insertbackground=COLOR.CL2),
            'setting17': Entry(window, bg=COLOR.CL1, bd=1, fg=COLOR.CL2, width=10, font=FONT.ENGLISH_SMALL,
                               insertbackground=COLOR.CL2),
            'setting18': Entry(window, bg=COLOR.CL1, bd=1, fg=COLOR.CL2, width=10, font=FONT.ENGLISH_SMALL,
                               insertbackground=COLOR.CL2),

        }

        self.setting_default = {
            'setting1': '30',
            'setting2': '0',
            'setting3': '30',
            'setting4': '0',
            'setting5': '0.5',
            'setting6': '6',
            'setting7': '9',
            'setting8': '5',
            'setting9': '2000',
            'setting10': '0',
            'setting11': '0',
            'setting12': '10',
            'setting13': '300',
            'setting14': '15',
            'setting15': '9999',
            'setting16': '900',
            'setting17': '1',
            'setting18': '10'
        }

        if len(DATABASE.setting_data) > 0:
            for num, item in enumerate(self.setting_default):
                self.setting_default[item] = DATABASE.setting_data[0][num + 1]

        column = 0
        for num, item in enumerate(self.setting_string):
            if num % 9 == 0:
                column += 2
            self.setting_Entry[item].grid(row=num % 9, column=column, pady='5m', padx='5m')
            Label(window, text=cft(self.setting_string[item]), font=FONT.PERSIAN_SMALL, bg=COLOR.CL1,
                  fg=COLOR.CL2, justify='left').grid(row=num % 9, column=column + 1,
                                                     pady='5m', padx='5m')
            self.setting_Entry[item].config(textvariable=self.setting_value[item])
            self.setting_value[item].set(self.setting_default[item])

        Button(window, width=10, height=2, font=FONT.ENGLISH_BOLD, text='Save', bg=COLOR.CL1, fg=COLOR.CL2,
               activebackground=COLOR.CL2, activeforeground=COLOR.CL1,
               command=lambda: self.save_button(None, window)).grid(
            row=10, column=5,
            padx='10m',
            pady='10m')
        window.bind('<Return>', lambda event: self.save_button(event, window))

    def save_button(self, event, window):
        global DATABASE

        try:
            int(self.setting_Entry['setting1'].get())
        except:
            messagebox.showerror('wrong value', cft('طول زمان محاسبه بدون ژاکت را با فرمت درست وارد کنید '),
                                 parent=window)
            return
        try:
            int(self.setting_Entry['setting2'].get())
        except:
            messagebox.showerror('wrong value', cft('فشار برای شروع محاسبه بدون ژاکت را با فرمت درست وارد کنید '),
                                 parent=window)
            return
        try:
            int(self.setting_Entry['setting3'].get())
        except:
            messagebox.showerror('wrong value', cft('طول زمان محاسبه با ژاکت را با فرمت درست وارد کنید '),
                                 parent=window)
            return
        try:
            int(self.setting_Entry['setting4'].get())
        except:
            messagebox.showerror('wrong value', cft('tفشار برای محاسبه با ژاکت را با فرمت درست وارد کنید '),
                                 parent=window)
            return
        try:
            float(self.setting_Entry['setting5'].get())
        except:
            messagebox.showerror('wrong value', cft('دقت ترازو را با فرمت درست وارد کنید '), parent=window)
            return
        try:
            int(self.setting_Entry['setting6'].get())
        except:
            messagebox.showerror('wrong value', cft(' زمان وزن مقایسه اول را با فرمت درست وارد کنید '), parent=window)
            return
        try:
            int(self.setting_Entry['setting7'].get())
        except:
            messagebox.showerror('wrong value', cft('زمان وزن مقایسه دوم را با فرمت درست وارد کنید '), parent=window)
            return
        try:
            int(self.setting_Entry['setting8'].get())
        except:
            messagebox.showerror('wrong value', cft('طول زمان معکوس شمار اول را با فرمت درست وارد کنید '),
                                 parent=window)
            return
        try:
            int(self.setting_Entry['setting9'].get())
        except:
            messagebox.showerror('wrong value', cft('مقیاس نمودار وزن را با فرمت درست وارد کنید '), parent=window)
            return
        try:
            int(self.setting_Entry['setting10'].get())
        except:
            messagebox.showerror('wrong value', cft('فشار را تا این عدد صفر نشان دهد را با فرمت درست وارد کنید '),
                                 parent=window)
            return
        try:
            float(self.setting_Entry['setting11'].get())
        except:
            messagebox.showerror('wrong value', cft('وزن را تا این عدد صفر نشان دهد را با فرمت درست وارد کنید '),
                                 parent=window)
            return

        try:
            int(self.setting_Entry['setting12'].get())
        except:
            messagebox.showerror('wrong value', cft('معکوس شمار دوم را با فرمت درست وارد کنید '), parent=window)
            return

        try:
            int(self.setting_Entry['setting13'].get())
        except:
            messagebox.showerror('wrong value', cft('حداکثر فشار ذخیره در دیتابیس را با فرمت درست وارد کنید '),
                                 parent=window)
            return

        try:
            int(self.setting_Entry['setting14'].get())
        except:
            messagebox.showerror('wrong value', cft('طول زمان معکوس شمار سوم را با فرمت درست وارد کنید '),
                                 parent=window)
            return
        try:
            int(self.setting_Entry['setting15'].get())
        except:
            messagebox.showerror('wrong value', cft('فشار تنظیمی جهت شروع نشتیابی سوم را با فرمت درست وارد کنید '),
                                 parent=window)
            return
        try:
            int(self.setting_Entry['setting16'].get())
        except:
            messagebox.showerror('wrong value', cft('سوپاپ اطمینان را با فرمت درست وارد کنید '),
                                 parent=window)
            return
        try:
            int(self.setting_Entry['setting17'].get())
        except:
            messagebox.showerror('wrong value', cft('شروع شماره گذازی تست ها را با فرمت درست وارد کنید '),
                                 parent=window)
            return
        try:
            int(self.setting_Entry['setting18'].get())
        except:
            messagebox.showerror('wrong value', cft('زمان شیر برقی را با فرمت درست وارد کنید '),
                                 parent=window)
            return

        st = ''
        for item in self.setting_default:
            self.setting_default[item] = self.setting_Entry[item].get()
            st += "'{}',".format(self.setting_Entry[item].get())
        st = st[:-1]
        st = "0," + st
        DATABASE.connection.execute("DELETE from Setting where ID = 0;")
        DATABASE.database.commit()
        DATABASE.connection.execute("INSERT INTO Setting VALUES ({});".format(st))
        DATABASE.database.commit()
        DATABASE.read_data()

        window.destroy()

    def destroy_window(self):
        if self.window is not None:
            self.window.destroy()
            self.window = None


class HardWare:
    def __init__(self):

        try:
            f = open(FILE_NAME.SerialPort, 'r', encoding='utf-8')
            read = f.read()
            f.close()
        except:
            f = open(FILE_NAME.SerialPort, 'w', encoding='utf-8')
            f.write("COM28,4800")
            f.close()
            read = "COM28,4800"
        self.port_name = read.split(',')[0]
        self.port = None
        self.baudrate = read.split(',')[1]
        self._isOpen = False
        self.data = ''
        self.temp = 0
        self.pressure = 0.0
        self.value = 0.0
        self.last_value = 0.0
        self.hx_val = 0.0
        self.control_relay = {'pressure': 'off', 'relieve': 'off', 'water': 'off'}

        self.type = Globals.SYSTEM_TYPE

    def create_connect_window(self, event=None):
        global root
        win = Toplevel(root, bg=COLOR.CL1)
        com_list = serial.tools.list_ports.comports()
        show_box = Listbox(win, font=FONT.ENGLISH_BOLD, width=50, height=10, bg=COLOR.CL1, fg=COLOR.CL2)
        show_box.pack(side='top', padx='5m', pady='5m')

        for num, item in enumerate(com_list):
            show_box.insert(num,
                            'PORT NUMBER ' + str(num + 1) + ' : ' + str(item.device) + '   ' + str(item.manufacturer))

        var = IntVar()

        r1 = Radiobutton(win, text='9600', fg=COLOR.CL2, font=FONT.ENGLISH_BIG, bg=COLOR.CL1, variable=var, value=1,
                         activebackground=COLOR.CL1, activeforeground=COLOR.CL2)
        r1.pack(side='left', padx='5m', pady='5m')

        r2 = Radiobutton(win, text='4800', fg=COLOR.CL2, font=FONT.ENGLISH_BIG, bg=COLOR.CL1, variable=var, value=2,
                         activebackground=COLOR.CL1, activeforeground=COLOR.CL2)
        r2.pack(side='left', padx='5m', pady='5m')
        r2.select()

        Button(win, text='Done', width=10, bg=COLOR.CL1, fg=COLOR.CL2, font=FONT.ENGLISH_SMALL,
               activebackground=COLOR.CL1,
               activeforeground=COLOR.CL2,
               command=lambda: self.connect_to_hw(win, var, show_box.curselection(), com_list)).pack(side='left',
                                                                                                     padx='5m',
                                                                                                     pady='5m')

    def connect_to_hw(self, win, var, show, lst):
        if len(show) != 0:
            st = ''
            self.port_name = lst[show[0]].device
            st += self.port_name + ','
            if var.get() == 1:
                self.baudrate = 9600
                st += '9600'
            else:
                self.baudrate = 4800
                st += '4800'

            f = open(FILE_NAME.SerialPort, 'w', encoding='utf-8')
            f.write(st)
            f.close()

            self.open_port()
        else:
            messagebox.showerror('No selection', cft('هیچ پورتی انتخاب نشده است'), parent=win)
            return
        win.destroy()

    def open_port(self):
        try:
            if self.port is not None:
                self.port.close()
            self.port = serial.Serial(self.port_name, baudrate=self.baudrate, timeout=0.5)
            # self.port.timeout = 1
            self._isOpen = True

            return True
        except:
            self._isOpen = False
            return False

    def read_data(self):
        global DATABASE
        if self._isOpen:
            try:
                if self.port.inWaiting() != 0:
                    self.data = self.port.readline()
                    self.data = self.data.decode('utf-8')
                    try:
                        self.value = float(self.data.split('/')[0])
                        self.value *= float(Globals.WEIGHT_ZARIB)
                        self.value -= float(Globals.WEIGHT_TELO)

                        self.value = str(round(self.value, 2))

                        self.pressure = float(self.data.split('/')[1])
                        self.pressure *= float(Globals.PRESSURE_ZARIB)
                        self.pressure -= float(Globals.PRESSURE_TELO)

                        self.pressure = str(int(self.pressure))

                        self.temp = float(self.data.split('/')[2])
                        self.temp = str(int(self.temp))

                        self.hx_val = float(self.data.split('/')[4])
                        self.hx_val = str(int(self.hx_val))

                        if float(self.pressure) <= float(DATABASE.setting_val['setting10']):
                            self.pressure = '0'

                        if abs(float(self.value)) <= float(DATABASE.setting_val['setting11']):
                            self.value = '0'

                        if abs(float(self.value) - float(self.last_value)) >= float(DATABASE.setting_val['setting5']):
                            self.last_value = self.value
                        else:
                            self.value = self.last_value

                        if int(self.pressure) >= int(DATABASE.setting_val['setting16']):
                            self.relay2_on()

                    except Exception as ex:
                        self.value = '-'
                        self.pressure = '-'
                        self.temp = '-'
            except Exception as ex:
                if 'codec' in str(ex):
                    self.create_connect_window()
                    messagebox.showerror('wrong baudrate', 'wrog baudrate')

                self.port.close()
                self.port = None
                self._isOpen = False
        else:
            try:
                if self.port is None:
                    self.open_port()
                else:
                    self.port.open()
                    self._isOpen = True
            except:
                pass

    def relay1_on(self):
        try:
            if self._isOpen:
                self.port.write(b"On1")
                self.control_relay['pressure'] = 'on'
                if self.type == 1:
                    sleep(1)

        except Exception as ex:
            self._isOpen = False

    def relay1_off(self):
        try:
            if self._isOpen:
                self.port.write(b"Off1")
                self.control_relay['pressure'] = 'off'
                if self.type == 1:
                    sleep(1)
        except Exception as ex:
            self._isOpen = False

    def relay2_on(self):
        try:
            if self._isOpen:
                self.port.write(b"OnE")
                self.control_relay['relieve'] = 'on'
                if self.type == 1:
                    sleep(1)
        except:
            self._isOpen = False

    def relay2_off(self):
        try:
            if self._isOpen:
                self.port.write(b"OffW")
                self.control_relay['relieve'] = 'off'
                if self.type == 1:
                    sleep(1)
        except:
            self._isOpen = False

    def relay3_on(self):
        try:
            if self._isOpen:
                self.port.write(b"OnY")
                self.control_relay['water'] = 'on'
                if self.type == 1:
                    sleep(1)
        except:
            self._isOpen = False

    def relay3_off(self):
        try:
            if self._isOpen:
                self.port.write(b"OffU")
                self.control_relay['water'] = 'off'
                if self.type == 1:
                    sleep(1)
        except:
            self._isOpen = False

    def send_tare(self):
        try:
            if self._isOpen:
                self.port.write(b'Tare')
                if self.type == 1:
                    sleep(1)
        except:
            self._isOpen = False

    def off_all(self):
        try:
            if self._isOpen:
                self.port.write(b"OffJ")
                self.control_relay['water'] = 'off'
                self.control_relay['pressure'] = 'off'
                self.control_relay['relieve'] = 'off'
                if self.type == 1:
                    sleep(1)
        except:
            self._isOpen = False
        # self.relay1_off()
        # self.relay2_off()
        # self.relay3_off()

    def calibrate_pressure(self, var):
        try:
            if self._isOpen:
                self.port.write(b'Cal_p')
                sleep(2)
                self.port.write(bytes(var, 'utf-8'))
                sleep(2)
        except:
            self._isOpen = False

    def calibrate_weight(self, var):
        try:
            if self._isOpen:
                self.port.write(b'Cal_w')
                sleep(2)
                var = str(round(int(self.hx_val) / float(var), 2))
                self.port.write(bytes(var, 'utf-8'))
                sleep(2)
        except:
            self._isOpen = False


class Manual:
    def __init__(self, window):
        self.root = window
        self.window = None
        self.pressure_val = None
        self.weight_val = None

    def create_window(self):
        global root
        self.window = Frame(self.root, bg=COLOR.CL1, height=WINDOWS_HEIGHT)
        self.window.pack(side='top', padx='5m', pady='1m', fill='x', expand='true')

        # self.pressure_val = Label
        call_frame = Frame(self.window, bg=COLOR.CL1)
        call_frame.pack(side='bottom', padx='5m', pady='5m')
        Button(call_frame, text='Calibrate-p', width=15, font=FONT.ENGLISH_BOLD, bg=COLOR.CL1, fg=COLOR.CL2,
               activebackground=COLOR.CL1, command=self.click_cal_p).pack(side='left', padx='5m', pady='5m')

        Button(call_frame, text='Calibrate-w', width=15, font=FONT.ENGLISH_BOLD, bg=COLOR.CL1, fg=COLOR.CL2,
               activebackground=COLOR.CL1, command=self.click_cal_w).pack(side='left', padx='5m', pady='5m')

        Button(call_frame, text='Tare', width=15, font=FONT.ENGLISH_BOLD, bg=COLOR.CL1, fg=COLOR.CL2,
               activebackground=COLOR.CL1, command=lambda: self.control_btn_click('Tare', None)).pack(
            side='left', padx='5m', pady='5m')

        btn_frame = Frame(self.window, bg=COLOR.CL1)
        btn_frame.pack(side='right', padx='10m', pady='5')
        cl1 = 'red'
        cl2 = 'red'
        cl3 = 'red'
        if HW.control_relay['pressure'] == 'on':
            cl1 = 'green'
        if HW.control_relay['relieve'] == 'on':
            cl2 = 'green'
        if HW.control_relay['water'] == 'on':
            cl3 = 'green'

        pressure_btn = Button(btn_frame, text='Pressure Pump', width=15, font=FONT.ENGLISH_BOLD, bg=cl1,
                              activebackground=cl1, command=lambda: self.control_btn_click('pressure', pressure_btn))
        pressure_btn.pack(side='top', padx='5m', pady='5m')

        relieve_btn = Button(btn_frame, text='Relieve', width=15, font=FONT.ENGLISH_BOLD, bg=cl2,
                             activebackground=cl2,
                             command=lambda: self.control_btn_click('relieve', relieve_btn))
        relieve_btn.pack(side='top', padx='5m', pady='5m')

        water_btn = Button(btn_frame, text='WaterFiling', width=15, font=FONT.ENGLISH_BOLD, bg=cl3,
                           activebackground=cl3,
                           command=lambda: self.control_btn_click('water', water_btn))
        water_btn.pack(side='top', padx='5m', pady='5m')

        label_frame = Frame(self.window, bg=COLOR.CL1)
        label_frame.pack(side='right', padx='10m', pady='5m')

        Label(label_frame, text=cft('فشار آزمون'), font=FONT.PERSIAN_BOLD, bg=COLOR.CL1, fg=COLOR.CL2).grid(row=0,
                                                                                                            column=0,
                                                                                                            padx='20m',
                                                                                                            pady='5m')
        Label(label_frame, text=cft('حجم آب تزریق شده'), font=FONT.PERSIAN_BOLD, bg=COLOR.CL1, fg=COLOR.CL2).grid(row=0,
                                                                                                                  column=1,
                                                                                                                  padx='20m',
                                                                                                                  pady='5m')

        self.pressure_val = Label(label_frame, text='0', font=FONT.ENGLISH_NUMBER, bg=COLOR.CL1, fg='#E84C3D')
        self.pressure_val.grid(row=1, column=0, padx='10m', pady='10')

        self.weight_val = Label(label_frame, text='0', font=FONT.ENGLISH_NUMBER, bg=COLOR.CL1, fg='#E84C3D')
        self.weight_val.grid(row=1, column=1, padx='10m', pady='10')

    def control_btn_click(self, name, btn):
        global HW

        if not HW._isOpen:
            messagebox.showerror('Not Connect', cft('دستگاه متصل نمی باشد'))
            return
        color = 'red'
        if btn is not None:
            color = btn.cget('bg')

        if 'pressure' in name:
            if color == 'red':
                btn.config(bg='green', activebackground='green')
                HW.relay1_on()
            else:
                btn.config(bg='red', activebackground='red')
                HW.relay1_off()
        elif 'relieve' in name:
            if color == 'red':
                btn.config(bg='green', activebackground='green')
                HW.relay2_on()
            else:
                btn.config(bg='red', activebackground='red')
                HW.relay2_off()
        elif 'water' in name:
            if color == 'red':
                btn.config(bg='green', activebackground='green')
                HW.relay3_on()
            else:
                btn.config(bg='red', activebackground='red')
                HW.relay3_off()
        elif 'Tare' in name:
            HW.send_tare()

    def click_cal_p(self):
        global root, HW

        if not HW._isOpen:
            messagebox.showerror('Not Connect', cft('دستگاه متصل نمی باشد'))
            return

        win = Toplevel(root)
        win.geometry('+100+100')
        win.config(bg=COLOR.CL1)
        win.title("pressure Calibration")

        var = StringVar()
        Label(win, text=cft('عدد سنسور فشار را وارد کنید'), bg=COLOR.CL1, fg=COLOR.CL2, font=FONT.PERSIAN_BOLD).pack(
            side='top', padx='5m', pady='5m')
        e = Entry(win, bg=COLOR.CL1, bd=1, fg=COLOR.CL2, width=10, font=FONT.ENGLISH_BIG,
                  insertbackground=COLOR.CL2, textvariable=var)
        e.pack(side='top', padx='5m', pady='5m')

        Button(win, text='Cancel', width=15, font=FONT.ENGLISH_BOLD, bg=COLOR.CL1, fg=COLOR.CL2,
               activebackground=COLOR.CL1,
               command=lambda: self.cancel_click(win)).pack(side='right', padx='10m', pady='5m')

        Button(win, text='OK', width=15, font=FONT.ENGLISH_BOLD, bg=COLOR.CL1, fg=COLOR.CL2,
               activebackground=COLOR.CL1,
               command=lambda: self.calibrate_click(win, var, 'pressure')).pack(side='right', padx='10m', pady='5m')

    def click_cal_w(self):
        global root, HW

        if not HW._isOpen:
            messagebox.showerror('Not Connect', cft('دستگاه متصل نمی باشد'))
            return

        win = Toplevel(root)
        win.geometry('+100+100')
        win.config(bg=COLOR.CL1)
        win.title("Weight Calibration")

        var = StringVar()
        Label(win, text=cft('وزن را وارد کنید'), bg=COLOR.CL1, fg=COLOR.CL2, font=FONT.PERSIAN_BOLD).pack(
            side='top', padx='5m', pady='5m')
        e = Entry(win, bg=COLOR.CL1, bd=1, fg=COLOR.CL2, width=10, font=FONT.ENGLISH_BIG,
                  insertbackground=COLOR.CL2, textvariable=var)
        e.pack(side='top', padx='5m', pady='5m')

        Button(win, text='Cancel', width=15, font=FONT.ENGLISH_BOLD, bg=COLOR.CL1, fg=COLOR.CL2,
               activebackground=COLOR.CL1,
               command=lambda: self.cancel_click(win)).pack(side='right', padx='10m', pady='5m')

        Button(win, text='OK', width=15, font=FONT.ENGLISH_BOLD, bg=COLOR.CL1, fg=COLOR.CL2,
               activebackground=COLOR.CL1,
               command=lambda: self.calibrate_click(win, var, 'weight')).pack(side='right', padx='10m', pady='5m')

    def cancel_click(self, window):
        window.destroy()

    def calibrate_click(self, window, var, tag):
        global HW
        if 'pressure' in tag:
            HW.calibrate_pressure(var.get())
        if 'weight' in tag:
            HW.calibrate_weight(var.get())
        window.destroy()

    def destroy_window(self):
        if self.window is not None:
            self.window.destroy()
            self.window = None

    def update(self):
        global HW
        if self.window is not None:
            self.pressure_val.config(text=str(HW.pressure))
            self.weight_val.config(text=str(HW.value))


class DataBaseSql:
    def __init__(self):
        self.database = sqlite3.connect(FILE_NAME.DataBase)
        self.connection = self.database.cursor()

        self.customer_data = []
        self.customer_id = []
        self.CustomerTable = ('CustomerID', 'Customer', 'PhoneNumber')

        self.operator_data = []
        self.operator_id = []
        self.OperatorTable = ('OperatorID', 'Operator')

        self.test_data = []
        self.Test = ('TestID', 'MaxP', 'MaxW', 'V1', 'V2', 'PE', 'TE', 'K', 'WaterWeight', 'Percent', 'Mode', 'Result')

        self.simfa_data = []
        self.Simfa = (
            'ID',
            'YearProduction',
            'YearExpiration',
            'WeightFirst',
            'WeightSecond',
            'PressureValueNormal',
            'PressureTimeNormal',
            'PressureValueTest',
            'PressureTimeTest',
            'VolumeFirst',
            'VolumeSecond',
            'VolumeNominal',
            'DischargeDuration',
            'CylinderType',
            'CylinderNumber',
            'CylinderSN',
            'CylinderCompany',
            'CylinderImage',
            'ReceptionNumber',
            'IsLegible',
            'IsNoDamage',
            'IsNoSpecialDamage',
            'IsLeakTestOk',
            'IsThreadOk',
            'IsWallThicknessOk',
            'Username',
            'Password',
            'ChartPT',
            'ChartVT',
            'LineID'
        )

        self.ReportTable = (
            'ID',
            'CustomerName',
            'OperatorName',
            'State',
            'Date',
            'Send',
            'DateDone'
        )
        self.report_data = []

        self.setting_data = []
        self.setting_val = {
            'setting1': None,
            'setting2': None,
            'setting3': None,
            'setting4': None,
            'setting5': None,
            'setting6': None,
            'setting7': None,
            'setting8': None,
            'setting9': None,
            'setting10': None,
            'setting11': None,
            'setting12': None,
            'setting13': None,
            'setting14': None,
            'setting15': None,
            'setting16': None,
            'setting17': None,
            'setting18': None
        }

        self.SimfaConnection = Simfa()

        self.pdf_tabel_head = {
            'ID': 'شماره تست',
            'ReceptionNumber': 'شماره پذیرش',
            'CylinderSN': 'سریال مخزن',
            'Date': 'تاریخ پذیرش',
            'CustomerName': 'نام مشتری ',
            'PhoneNumber': 'شماره تلفن',
            'YearProduction': 'تاریخ تولید سیلندر‌',
            'YearExpiration': 'تاریخ انقضا سیلندر',
            'CylinderType': 'نوع مخزن',
            'CylinderCompany': 'برند سازنده',
            'WeightFirst': 'وزن حک شده :',
            'WeightSecond': 'وزن اندازه گیری شده :',
            'VolumeNominal': 'حجم اسمی مخزن‌ :',
            'PressureValueTest': 'فشار تست :',
            'CylinderNumber': 'شماره مخزن:',
            'IsLegible': 'خوانا بودن اطلاعات مخزن :',
            'IsNoDamage': 'عدم عیب ظاهری',
            'IsLeakTestOk': 'عدم نشتی',
            'IsThreadOk': 'عدم مشکل رزوه',
            'Mode': 'نوع تست',
            'PressureValueTest': 'فشار تست',
            'TE': 'TE',
            'PE': 'PE',
            'Percent': 'درصد انبساط حجمی',
            'OperatorName': 'نام اپراتور تست ',
            'Result': 'وضعیت تست',
            'Send': 'وضعیت ارسال اطلاعات'
        }
        self.pdf_head = (
            'ID',
            'ReceptionNumber',
            'CylinderSN',
            'Date',
            'CustomerName',
            'PhoneNumber',
            'YearProduction',
            'YearExpiration',
            'CylinderType',
            'CylinderCompany',
            'WeightFirst',
            'WeightSecond',
            'VolumeNominal',
            'PressureValueTest',
            'CylinderNumber',
            'IsLegible',
            'IsNoDamage',
            'IsLeakTestOk',
            'IsThreadOk',
            'Mode',
            'PressureValueTest',
            'TE',
            'PE',
            'Percent',
            'OperatorName',
            'Result',
            'Send'
        )
        self.pdf = FPDF(orientation='P', unit='mm', format='A4')
        self.pdf.add_font('B Nazanin', '', FILE_NAME.Font_farsi, uni=True)

        self.create_tables()
        self.read_data()

    def create_pdf(self, id_number=0):

        self.pdf = FPDF(orientation='P', unit='mm', format='A4')
        self.pdf.add_font('B Nazanin', '', FILE_NAME.Font_farsi, uni=True)

        data_simfa = None
        data_test = None
        data_report = None

        data_pdf = {'ID': str(id_number)}
        for item in self.report_data:
            if item[0] == id_number:
                data_report = item
                break
        for item in self.simfa_data:
            if item[0] == id_number:
                data_simfa = item
                break
        for item in self.test_data:
            if item[0] == id_number:
                data_test = item
                break

        for num, item in enumerate(self.pdf_head):
            if item == 'ID':
                continue
            if item == 'PhoneNumber':
                flag = False
                for i in self.customer_data:
                    if i[1] == data_report[1]:
                        data_pdf[item] = i[2]
                        flag = True
                        break
                if not flag:
                    data_pdf[item] = '-'

            if item in self.Simfa:
                if 'Is' in item:
                    if data_simfa[self.Simfa.index(item)] == 'True':
                        data_pdf[item] = 'بله'
                    else:
                        data_pdf[item] = 'خیر'
                else:
                    data_pdf[item] = data_simfa[self.Simfa.index(item)]

            if item in self.ReportTable:
                if item == 'Send':
                    if 'Message' in data_report[self.ReportTable.index(item)]:
                        data_pdf[item] = data_report[self.ReportTable.index(item)][:-1].split(':')[1]
                        data_pdf[item] = data_pdf[item][1:-1]
                    elif '-' in data_report[self.ReportTable.index(item)]:
                        data_pdf[item] = 'ارسال نشده'
                    else:
                        data_pdf[item] = data_report[self.ReportTable.index(item)]

                else:
                    data_pdf[item] = data_report[self.ReportTable.index(item)]

            if item in self.Test:
                if item == 'Mode':
                    if data_test[self.Test.index(item)] == 'Jackat':
                        data_pdf[item] = 'با ژاکت'
                    else:
                        data_pdf[item] = 'بدون ژاکت'
                elif item == 'Result':
                    if data_test[self.Test.index(item)] == 'FAIL':
                        data_pdf[item] = 'مردود'
                    else:
                        data_pdf[item] = 'قبول'
                else:
                    data_pdf[item] = data_test[self.Test.index(item)]
        self.pdf.add_page()
        self.pdf.set_line_width(0.7)
        self.pdf.rect(5, 5, 200, 285)
        self.pdf.set_line_width(0.3)
        self.pdf.set_font('B Nazanin', '', 25)
        self.pdf.set_xy(5, 25)
        self.pdf.cell(185, 15, get_display(reshape('گواهی نتایج آزمون هیدرو استاتیک')), border=False, align='R', ln=1)
        try:
            self.pdf.image(Globals.LAB_LOGO, 15, 15, 30, 30)
        except:
            self.pdf.image(os.path.join(r + '/Utils/LOGO.png'), 15, 15, 30, 30)

        self.pdf.set_font('B Nazanin', '', 15)
        self.pdf.set_xy(140, 35)
        self.pdf.cell(40, 15, get_display(reshape('نام آزمایشگاه:')), border=False, align='R', ln=1)

        if isPersian(Globals.LAB_NAME):
            self.pdf.set_font('B Nazanin', '', 10)
            self.pdf.set_xy(115, 35)
            self.pdf.cell(40, 15, get_display(reshape(Globals.LAB_NAME)), border=False, align='R', ln=1)
        else:
            self.pdf.set_font('Arial', '', 10)
            self.pdf.set_xy(115, 35)
            self.pdf.cell(40, 15, Globals.LAB_NAME, border=False, align='R', ln=1)

        self.pdf.set_xy(150, 50)
        self.pdf.set_font('B Nazanin', '', 10)
        for num, item in enumerate(self.pdf_tabel_head):
            if num == 13:
                self.pdf.set_xy(70, 50)
            if isPersian(self.pdf_tabel_head[item]):
                self.pdf.set_font('B Nazanin', '', 10)
                self.pdf.cell(40, 7, get_display(reshape(self.pdf_tabel_head[item])), border=True, align='C', ln=2)
            else:
                self.pdf.set_font('Arial', '', 10)
                self.pdf.cell(40, 7, self.pdf_tabel_head[item], border=True, align='C', ln=2)

        self.pdf.set_xy(110, 50)
        self.pdf.set_font('B Nazanin', '', 10)
        for num, item in enumerate(data_pdf):
            if num == 13:
                self.pdf.set_xy(30, 50)
            if isPersian(data_pdf[item]):
                self.pdf.set_font('B Nazanin', '', 10)
                self.pdf.cell(40, 7, get_display(reshape(data_pdf[item])), border=True, align='C', ln=2)
            else:
                self.pdf.set_font('Arial', '', 10)
                self.pdf.cell(40, 7, data_pdf[item], border=True, align='C', ln=2)

        self.pdf.rect(30, 145, 160, 50)

        self.pdf.set_xy(150, 145)
        self.pdf.set_font('B Nazanin', '', 10)
        self.pdf.cell(40, 7, get_display(reshape('نمودار فشار - زمان')), border=False, align='C', ln=2)
        self.pdf.image(FILE_NAME.PressurePlot + str(id_number) + '.png', 110, 154, 79, 40)

        self.pdf.set_xy(70, 145)
        self.pdf.set_font('B Nazanin', '', 10)
        self.pdf.cell(40, 7, get_display(reshape('نمودار وزن - زمان')), border=False, align='C', ln=2)
        self.pdf.image(FILE_NAME.WeightPlot + str(id_number) + '.png', 31, 154, 79, 40)

        self.pdf.rect(30, 197, 160, 15)
        self.pdf.set_xy(150, 197)
        self.pdf.set_font('B Nazanin', '', 10)
        self.pdf.cell(40, 7, get_display(reshape('توضیحات:')), border=False, align='R', ln=2)

        self.pdf.rect(30, 215, 160, 30)

        self.pdf.set_xy(150, 215)
        self.pdf.cell(40, 7, get_display(reshape('تایید کننده‌(مدیر فنی):')), border=False, align='R', ln=2)

        if isPersian(Globals.LAB_CTO):
            self.pdf.set_font('B Nazanin', '', 10)
            self.pdf.set_xy(110, 215)
            self.pdf.cell(40, 7, get_display(reshape(Globals.LAB_CTO)), border=False, align='R', ln=2)
        else:
            self.pdf.set_font('Arial', '', 10)
            self.pdf.set_xy(110, 215)
            self.pdf.cell(40, 7, Globals.LAB_CTO, border=False, align='R', ln=2)

        self.pdf.set_font('B Nazanin', '', 10)
        self.pdf.set_xy(150, 225)
        self.pdf.cell(40, 7, get_display(reshape('تاریخ و امضا')), border=False, align='R', ln=2)

        self.pdf.set_font('B Nazanin', '', 10)
        self.pdf.set_xy(150, 235)
        self.pdf.cell(40, 7, get_display(reshape('این گواهی با مهر و امضا آزمایشگاه دارای اعتبار میباشد')),
                      border=False, align='R', ln=2)

        self.pdf.set_xy(70, 215)
        self.pdf.cell(40, 7, get_display(reshape('تصویب کننده(مدیر آزمایشگاه):')), border=False, align='R', ln=2)

        if isPersian(Globals.LAB_CEO):
            self.pdf.set_font('B Nazanin', '', 10)
            self.pdf.set_xy(30, 215)
            self.pdf.cell(40, 7, get_display(reshape(Globals.LAB_CEO)), border=False, align='R', ln=2)
        else:
            self.pdf.set_font('Arial', '', 10)
            self.pdf.set_xy(30, 215)
            self.pdf.cell(40, 7, Globals.LAB_CEO, border=False, align='R', ln=2)

        self.pdf.set_font('B Nazanin', '', 10)
        self.pdf.set_xy(70, 225)
        self.pdf.cell(40, 7, get_display(reshape('تاریخ و امضا')), border=False, align='R', ln=2)

        self.pdf.rect(30, 247, 160, 20)

        self.pdf.set_xy(150, 247)
        self.pdf.cell(40, 7, get_display(reshape('آدرس و مشخصات آزمایشگاه:')), border=False, align='R', ln=2)

        if isPersian(Globals.LAB_ADDRESS):
            self.pdf.set_font('B Nazanin', '', 10)
            self.pdf.cell(40, 7, get_display(reshape(Globals.LAB_ADDRESS)), border=False, align='R', ln=2)
        else:
            self.pdf.set_font('Arial', '', 10)
            self.pdf.cell(40, 7, Globals.LAB_ADDRESS, border=False, align='R', ln=2)

        self.pdf.set_xy(30, 270)
        self.pdf.set_font('Arial', '', 10)
        self.pdf.cell(80, 5, 'www.zoufan.com 031-35721253', border=False, align='R', ln=0)

        self.pdf.set_font('B Nazanin', '', 10)
        self.pdf.cell(80, 5, get_display(reshape('نرم افزار تست گروه فنی مهندسی ذوفن')), border=False, align='R', ln=0)

        self.pdf.output(FILE_NAME.PdfFile + str(id_number) + '.pdf')
        self.pdf.close()
        messagebox.showinfo('PDF Create',
                            cft('فایل گزارش {} در پوشه ی PDF ایجاد شد.'.format(
                                FILE_NAME.PdfFile + str(id_number) + '.pdf')))

    def create_tables(self):
        self.connection.execute(
            "CREATE TABLE IF NOT EXISTS Customer(CustomerID INTEGER PRIMARY KEY,Customer text,PhoneNumber text);")
        self.connection.execute(
            "CREATE TABLE IF NOT EXISTS Operator(OperatorID INTEGER PRIMARY KEY,Operator text);"
        )
        self.connection.execute("CREATE TABLE IF NOT EXISTS Setting ("
                                "ID INTEGER PRIMARY KEY,"
                                "Setting1 text ,"
                                "Setting2 text ,"
                                "Setting3 text ,"
                                "Setting4 text ,"
                                "Setting5 text ,"
                                "Setting6 text ,"
                                "Setting7 text ,"
                                "Setting8 text ,"
                                "Setting9 text ,"
                                "Setting10 text,"
                                "Setting11 text,"
                                "Setting12 text,"
                                "Setting13 text,"
                                "Setting14 text,"
                                "Setting15 text,"
                                "Setting16 text,"
                                "Setting17 text,"
                                "Setting18 text);")

        self.connection.execute(
            "CREATE TABLE IF NOT EXISTS Report(ID INTEGER PRIMARY KEY,CustomerName text,OperatorName text,State text,Date text,Send text,DateDone text);"
        )

        self.connection.execute(
            "CREATE TABLE IF NOT EXISTS Test(TestID INTEGER PRIMARY KEY,"
            "MaxP text,"
            "MaxW text,"
            "V1 text,"
            "V2 text,"
            "PE text,"
            "TE text,"
            "K text,"
            "WaterWeight text,"
            "Percent text,"
            "Mode text,"
            "Result);"
        )
        self.connection.execute(
            'CREATE TABLE IF NOT EXISTS Simfa(ID INTEGER PRIMARY KEY,'
            'YearProduction text,'
            'YearExpiration text,'
            'WeightFirst text,'
            'WeightSecond text,'
            'PressureValueNormal text,'
            'PressureTimeNormal text,'
            'PressureValueTest text,'
            'PressureTimeTest text,'
            'VolumeFirst text,'
            'VolumeSecond text,'
            'VolumeNominal text,'
            'DischargeDuration text,'
            'CylinderType text,'
            'CylinderNumber text,'
            'CylinderSN text,'
            'CylinderCompany text,'
            'CylinderImage text,'
            'ReceptionNumber text,'
            'IsLegible text,'
            'IsNoDamage text,'
            'IsNoSpecialDamage text,'
            'IsLeakTestOk text,'
            'IsThreadOk text,'
            'IsWallThicknessOk text,'
            'Username text,'
            'Password text,'
            'ChartPT text,'
            'ChartVT text,'
            'LineID text);'
        )

    def read_data(self):
        self.connection.execute("SELECT * FROM Operator;")
        self.operator_data = self.connection.fetchall()
        for item in self.operator_data:
            self.operator_id.append(item[0])

        self.connection.execute("SELECT * FROM Customer;")
        self.customer_data = self.connection.fetchall()
        for item in self.customer_data:
            self.customer_id.append(item[0])

        self.connection.execute("SELECT * FROM Report;")
        self.report_data = self.connection.fetchall()

        self.connection.execute("SELECT * FROM Simfa;")
        self.simfa_data = self.connection.fetchall()

        self.connection.execute("SELECT * FROM Test;")
        self.test_data = self.connection.fetchall()

        self.connection.execute("SELECT * FROM Setting;")
        self.setting_data = self.connection.fetchall()

        if len(self.setting_data) == 0:
            self.connection.execute(
                "INSERT INTO Setting VALUES (0,'30','0','30','0','0.5','6','9','5','2000','0','0','10','300','15','9999','900','1','10');")

            self.database.commit()

            self.connection.execute("SELECT * FROM Setting;")
            self.setting_data = self.connection.fetchall()

        for num, item in enumerate(self.setting_val):
            self.setting_val[item] = self.setting_data[0][num + 1]

        self.simfa_id = int(self.setting_val['setting17'])

    def add_customer(self, data):
        id = 1
        while id in self.customer_id:
            id += 1

        self.connection.execute(
            'INSERT INTO Customer VALUES({},"{}","{}");'.format(int(id), data['Customer'], data['PhoneNumber'])
        )
        self.database.commit()
        self.connection.execute("SELECT * FROM Customer;")
        self.customer_data = self.connection.fetchall()
        for item in self.customer_data:
            self.customer_id.append(item[0])

    def remove_customer(self, id):
        self.connection.execute("DELETE FROM Customer where CustomerID = {}; ".format(id))
        self.database.commit()
        self.connection.execute("SELECT * FROM Customer;")
        self.customer_data = self.connection.fetchall()
        self.customer_id = []
        for item in self.customer_data:
            self.customer_id.append(item[0])

    def add_operator(self, name):
        id = 1
        while id in self.operator_id:
            id += 1

        self.connection.execute(
            'INSERT INTO Operator VALUES({},"{}");'.format(int(id), name)
        )
        self.database.commit()
        self.connection.execute("SELECT * FROM Operator;")
        self.operator_data = self.connection.fetchall()
        for item in self.operator_data:
            self.operator_id.append(item[0])

    def remove_operator(self, id):
        self.connection.execute("DELETE FROM Operator where OperatorID = {}; ".format(id))
        self.database.commit()
        self.connection.execute("SELECT * FROM Operator;")
        self.operator_data = self.connection.fetchall()
        self.operator_id = []
        for item in self.operator_data:
            self.operator_id.append(item[0])

    def update_simfa_id_setting(self):
        self.connection.execute("UPDATE Setting SET setting17 = '{}' WHERE ID = 0;".format(self.simfa_id))
        self.database.commit()
        self.read_data()

    def add_simfa_test(self, value, id_number=None, state=None):

        if id_number is None:

            self.connection.execute(
                'INSERT INTO Report VALUES({},"{}","{}","-","{}","-","-");'.format(self.simfa_id, value['CustomerName'],
                                                                                   value['OperatorName'], TODAY_DATE)
            )
            self.database.commit()
        else:
            self.remove_simfa_test(id_number, state)

            if state != 'done':
                self.connection.execute(
                    'INSERT INTO Report VALUES({},"{}","{}","-","{}","-","-");'.format(id_number, value['CustomerName'],
                                                                                       value['OperatorName'],
                                                                                       TODAY_DATE)
                )
                self.database.commit()
        st = ''
        for item in self.Simfa:

            if item == 'ID':
                if id_number is None:
                    st += str(self.simfa_id)
                    st += ','
                    self.simfa_id += 1
                else:
                    st += str(id_number)
                    st += ','
            elif item in value:
                st += '"' + str(value[item]) + '"'
                st += ','
            elif item == 'Username':
                st += "'" + DATABASE.SimfaConnection.Username + "'"
                st += ','
            elif item == 'Password':
                st += "'" + DATABASE.SimfaConnection.Password + "'"
                st += ','
            elif item == 'LineID':
                st += '0'
                st += ','

            else:
                st += '"-",'
        st = st[:-1]
        self.connection.execute(
            'INSERT INTO Simfa VALUES({});'.format(st)
        )
        self.database.commit()
        self.update_simfa_id_setting()
        self.read_data()

    def remove_simfa_test(self, id, state=None):
        self.connection.execute("DELETE FROM Simfa where ID = {}; ".format(id))
        self.database.commit()

        self.connection.execute("DELETE FROM Test where TestID = {}; ".format(id))
        self.database.commit()

        if state is None or state == 'edit':
            self.connection.execute("DELETE FROM Report where ID = {}; ".format(id))
            self.database.commit()

        self.read_data()

        if state is None:
            if id == self.simfa_id - 1:
                self.simfa_id -= 1
                self.update_simfa_id_setting()

    def edit_simfa_test(self, value, id_number):
        for item in value:
            if item == 'OperatorName':
                self.connection.execute('UPDATE Report SET {}="{}" where ID = {};'.format(item, value[item], id_number))
            elif item == 'CustomerName':
                self.connection.execute('UPDATE Report SET {}="{}" where ID = {};'.format(item, value[item], id_number))
            elif item == 'ID':
                continue
            else:
                self.connection.execute('UPDATE Simfa SET {}="{}" where ID = {};'.format(item, value[item], id_number))
            self.database.commit()
            self.read_data()

    def add_test_info(self, test_info):
        val = ''

        for item in test_info:
            if item == 'TestID':
                val += str(test_info['TestID']) + ','
            else:
                val += '"' + str(test_info[item]) + '",'

        val = val[:-1]

        self.connection.execute('INSERT INTO Test VALUES({});'.format(val))
        self.database.commit()

        self.read_data()

    def add_done_test(self, simfa_info, test_info, id_number):
        self.connection.execute('UPDATE Report SET State="Done" where ID = {};'.format(id_number))
        self.connection.execute('UPDATE Report SET DateDone="{}" where ID = {};'.format(TODAY_DATE, id_number))
        self.remove_simfa_test(id_number, 'Done')
        self.add_simfa_test(simfa_info, id_number, 'done')
        self.add_test_info(test_info)

    def send_data_to_simfa(self, id_number):
        data = None
        for item in self.simfa_data:
            if item[0] == id_number:
                data = item
                break
        flag = False
        for item in self.test_data:
            if item[0] == id_number:
                flag = True
        if not flag:
            messagebox.showerror("Dont Test", 'لطفا پس از انجام آزمایش ارسال نمایید')
            return
        simfa_data = {
            'ChartPT': [],
            'ChartVT': []
        }
        for num, item in enumerate(self.Simfa):
            simfa_data[item] = data[num]

        simfa_data.pop('ID')
        simfa_data['ChartPT'] = []
        simfa_data['ChartVT'] = []

        try:
            fw = open(data[-2], 'r')
            for item in fw:
                if 'Time' in item:
                    continue
                v = {'x': None, 'y': None}
                v['x'] = int(item.split('|')[0][:-2])
                v['y'] = float(item.split('|')[1][:-2])
                simfa_data['ChartVT'].append(v)
            fw.close()

            fp = open(data[-3], 'r')
            for item in fp:
                if 'Time' in item:
                    continue
                p = {'x': None, 'y': None}
                p['x'] = int(item.split('|')[0][:-2])
                p['y'] = float(item.split('|')[1][:-2])
                simfa_data['ChartPT'].append(p)
            fp.close()
        except:
            messagebox.showerror("no file", 'فایل های نمودار موجود نیست .')
        mes = self.SimfaConnection.send_simfa(simfa_data, id_number)
        print(simfa_data)
        self.connection.execute("UPDATE Report SET Send='{}' where ID = {};".format(mes, id_number))
        self.database.commit()
        self.read_data()

    def update_done_test(self, id_number, values):
        for num, item in enumerate(values):
            if item == 'TestID':
                continue
            elif item in self.Simfa:
                self.connection.execute(
                    'UPDATE Simfa SET {}="{}" where ID = {};'.format(item, values[item].get(), id_number))
            elif item in self.Test:
                if item == 'Mode':
                    if values[item].get() == cft('ژاکت'):
                        values[item].set('Jacket')
                    else:
                        values[item].set('NoneJacket')
                if item == 'Result':
                    if values[item].get() == cft('قبول'):
                        values[item].set('Accept')
                    else:
                        values[item].set('FAIL')

                self.connection.execute(
                    'UPDATE Test SET {}="{}" where TestID = {};'.format(item, values[item].get(), id_number))
        self.database.commit()
        self.read_data()


class Database:
    def __init__(self, window):
        self.root = window
        self.window = None

        self.TabelSelect = 'Simfa'
        self.TableFrame = None
        self.btnframe2 = None
        self.list = None
        self.vsb = None
        self.hsb = None

        self.CustomerTable = ('CustomerID', 'Customer', 'PhoneNumber')
        self.OperatorTable = ('OperatorID', 'Operator')
        # self.Test = ('TestID', 'MaxP', 'MaxW', 'V1', 'V2', 'PE', 'TE', 'Percent', 'Mode', 'Result')
        self.Test = DATABASE.Test
        self.Simfa = (
            'ID',
            'YearProduction',
            'YearExpiration',
            'WeightFirst',
            'WeightSecond',
            'PressureValueNormal',
            'PressureTimeNormal',
            'PressureValueTest',
            'PressureTimeTest',
            'VolumeFirst',
            'VolumeSecond',
            'VolumeNominal',
            'DischargeDuration',
            'CylinderType',
            'CylinderNumber',
            'CylinderSN',
            'CylinderCompany',
            'CylinderImage',
            'ReceptionNumber',
            'IsLegible',
            'IsNoDamage',
            'IsNoSpecialDamage',
            'IsLeakTestOk',
            'IsThreadOk',
            'IsWallThicknessOk',
            'Username',
            'Password',
            'ChartPT',
            'ChartVT',
            'LineID'
        )
        self.Report = (
            'ID', 'CustomerName', 'OperatorName', 'State', 'Date', 'Send', 'DateDone'
        )

        self.edit_done_test_enable = False

    def enable_test_edit(self, event):
        if self.edit_done_test_enable:
            messagebox.showinfo('test edit enable', cft('ویرایش تست های انجام شده  غیر فعال شده است.'))
            self.edit_done_test_enable = False
        else:
            messagebox.showinfo('test edit enable', cft('ویرایش تست های انجام شده فعال شده است.'))
            self.edit_done_test_enable = True

    # Create Main
    def create_window(self):
        global root
        self.window = Frame(self.root, bg=COLOR.CL1, height=WINDOWS_HEIGHT)
        self.window.pack(side='top', padx='5m', pady='1m', fill='x', expand='true')

        btnframe = Frame(self.window, bg=COLOR.CL1)
        btnframe.pack(side='top', padx='1m', pady='1m', fill='x')

        Button(btnframe, text=cft('مشتری'), bg=COLOR.CL1, fg=COLOR.CL2, font=FONT.PERSIAN_SMALL, width=10,
               command=self.create_list_customer).pack(
            side='right', padx='5m', pady='3m')

        Button(btnframe, text=cft('اپراتور'), bg=COLOR.CL1, fg=COLOR.CL2, font=FONT.PERSIAN_SMALL, width=10,
               command=self.create_list_operator).pack(
            side='right', padx='5m', pady='3m')

        Button(btnframe, text=cft('تست'), bg=COLOR.CL1, fg=COLOR.CL2, font=FONT.PERSIAN_SMALL, width=10,
               command=self.create_list_test).pack(
            side='right', padx='5m', pady='3m')

        Button(btnframe, text=cft('گزارش'), bg=COLOR.CL1, fg=COLOR.CL2, font=FONT.PERSIAN_SMALL, width=10,
               command=self.create_list_report).pack(
            side='right', padx='5m', pady='3m')

        Button(btnframe, text=cft('انجام شده'), bg=COLOR.CL1, fg=COLOR.CL2, font=FONT.PERSIAN_SMALL, width=10,
               command=self.create_list_done).pack(
            side='right', padx='5m', pady='3m')

        self.create_list_test()

    # Create Button List
    def create_list_test(self, flag=None):
        if self.TableFrame is not None:
            self.TableFrame.destroy()
            self.TableFrame.pack_forget()
            self.TableFrame = None
            self.btnframe2.destroy()
            self.btnframe2.pack_forget()
            self.btnframe2 = None

        self.TabelSelect = 'Simfa'
        self.btnframe2 = Frame(self.window, bg=COLOR.CL1)
        self.btnframe2.pack(side='top', padx='1m', pady='1m', fill='x')

        Button(self.btnframe2, text=cft('اضافه کردن'), bg=COLOR.CL1, fg=COLOR.CL2, font=FONT.PERSIAN_SMALL, width=10,
               command=self.add_test).pack(
            side='right', padx='5m', pady='3m')
        Button(self.btnframe2, text=cft('حذف'), bg=COLOR.CL1, fg=COLOR.CL2, font=FONT.PERSIAN_SMALL, width=10,
               command=lambda: self.remove_test_btn(list)).pack(
            side='right', padx='5m', pady='3m')

        Button(self.btnframe2, text=cft('ویرایش'), bg=COLOR.CL1, fg=COLOR.CL2, font=FONT.PERSIAN_SMALL, width=10,
               command=lambda: self.edit_test_btn(list)).pack(
            side='right', padx='5m', pady='3m')
        Button(self.btnframe2, text=cft('ارسال'), bg=COLOR.CL1, fg=COLOR.CL2, font=FONT.PERSIAN_SMALL, width=10,
               command=lambda: self.send_data(list)).pack(
            side='right', padx='5m', pady='3m')

        Button(self.btnframe2, text=cft('پیش فرض'), bg=COLOR.CL1, fg=COLOR.CL2, font=FONT.PERSIAN_SMALL, width=10,
               command=lambda: self.default_test(list)).pack(
            side='right', padx='5m', pady='3m')

        style = ttk.Style()
        style.configure("mystyle.Treeview", highlightthickness=0, bd=0,
                        font=('Calibri', 11), rowheight=50)  # Modify the font of the body
        style.configure("mystyle.Treeview.Heading", font=('Calibri', 14, 'bold'))  # Modify the font of the headings
        style.layout("mystyle.Treeview", [('mystyle.Treeview.treearea', {'sticky': 'nswe'})])  # Remove the borders

        self.TableFrame = Frame(self.window, bg=COLOR.CL1)
        self.TableFrame.pack(side='top', fill='x')

        list = ttk.Treeview(self.TableFrame, selectmode='browse', show='headings', style="mystyle.Treeview", height=5)
        vsb = Scrollbar(self.TableFrame, orient='vertical', command=list.yview, width='10m')
        hsb = Scrollbar(self.TableFrame, orient='horizontal', command=list.xview, width='10m')

        list.configure(xscrollcommand=hsb.set)
        list.configure(yscrollcommand=vsb.set)
        vsb.pack(side='right', fill='y')
        list.pack(side='top', fill='both')
        hsb.pack(side='bottom', fill='x')

        list['columns'] = self.Simfa
        list.tag_configure('odd', background='#E8E8E8', font=FONT.ENGLISH_SMALL)
        list.tag_configure('even', background='#DFDFDF', font=FONT.ENGLISH_SMALL)

        for item in self.Simfa:
            list.column(item, width=int(100 * Globals.ppm))
            list.heading(item, text=cft(item))

        number = 0
        for num, item in enumerate(reversed(DATABASE.simfa_data)):
            # for d in item:
            number += 1
            d = []
            for i in item:
                d.append(cft(str(i)))
            if num % 2 == 1:
                list.insert('', 'end', text=cft('item{}'.format(num)), values=d, tags='odd')
            else:
                list.insert('', 'end', text=cft('item{}'.format(num)), values=d, tags='even')
            if number >= Globals.SHOW_NUMBER:
                break

    def create_list_operator(self):
        global DATABASE
        if self.TableFrame is not None:
            self.TableFrame.destroy()
            self.TableFrame.pack_forget()
            self.TableFrame = None
            self.btnframe2.destroy()
            self.btnframe2.pack_forget()
            self.btnframe2 = None
        self.TabelSelect = 'Operator'
        self.btnframe2 = Frame(self.window, bg=COLOR.CL1)
        self.btnframe2.pack(side='top', padx='1m', pady='1m', fill='x')

        Button(self.btnframe2, text=cft('اضافه کردن'), bg=COLOR.CL1, fg=COLOR.CL2, font=FONT.PERSIAN_SMALL, width=10,
               command=self.add_operator).pack(
            side='right', padx='5m', pady='3m')
        Button(self.btnframe2, text=cft('حذف'), bg=COLOR.CL1, fg=COLOR.CL2, font=FONT.PERSIAN_SMALL, width=10,
               command=lambda: self.remove_operator(list)).pack(
            side='right', padx='5m', pady='3m')

        style = ttk.Style()
        style.configure("mystyle.Treeview", highlightthickness=0, bd=0,
                        font=('Calibri', 11), rowheight=50)  # Modify the font of the body
        style.configure("mystyle.Treeview.Heading", font=('Calibri', 14, 'bold'))  # Modify the font of the headings
        style.layout("mystyle.Treeview", [('mystyle.Treeview.treearea', {'sticky': 'nswe'})])  # Remove the borders

        self.TableFrame = Frame(self.window, bg=COLOR.CL1)
        self.TableFrame.pack(side='top', fill='x')

        list = ttk.Treeview(self.TableFrame, selectmode='browse', show='headings', style="mystyle.Treeview", height=5)
        vsb = Scrollbar(self.TableFrame, orient='vertical', command=list.yview, width='10m')
        hsb = Scrollbar(self.TableFrame, orient='horizontal', command=list.xview, width='10m')

        list.configure(xscrollcommand=hsb.set)
        list.configure(yscrollcommand=vsb.set)
        vsb.pack(side='right', fill='y')
        list.pack(side='top', fill='both')
        hsb.pack(side='bottom', fill='x')
        list['columns'] = self.OperatorTable

        list.tag_configure('odd', background='#E8E8E8', font=FONT.ENGLISH_SMALL)
        list.tag_configure('even', background='#DFDFDF', font=FONT.ENGLISH_SMALL)

        for item in self.OperatorTable:
            list.column(item, width=int(100 * Globals.ppm))
            list.heading(item, text=cft(item))

        for num, item in enumerate(DATABASE.operator_data):
            # for d in item:
            d = []
            for i in item:
                d.append(cft(str(i)))
            if num % 2 == 1:
                list.insert('', 'end', text=cft('item{}'.format(num)), values=d, tags='odd')
            else:
                list.insert('', 'end', text=cft('item{}'.format(num)), values=d, tags='even')

    def create_list_customer(self):
        global DATABASE
        if self.TableFrame is not None:
            self.TableFrame.destroy()
            self.TableFrame.pack_forget()
            self.TableFrame = None
            self.btnframe2.destroy()
            self.btnframe2.pack_forget()
            self.btnframe2 = None

        self.TabelSelect = 'Customer'
        self.btnframe2 = Frame(self.window, bg=COLOR.CL1)
        self.btnframe2.pack(side='top', padx='1m', pady='1m', fill='x')

        Button(self.btnframe2, text=cft('اضافه کردن'), bg=COLOR.CL1, fg=COLOR.CL2, font=FONT.PERSIAN_SMALL, width=10,
               command=self.add_customer).pack(
            side='right', padx='5m', pady='3m')
        Button(self.btnframe2, text=cft('حذف'), bg=COLOR.CL1, fg=COLOR.CL2, font=FONT.PERSIAN_SMALL, width=10,
               command=lambda: self.remove_customer(list)).pack(
            side='right', padx='5m', pady='3m')

        style = ttk.Style()
        style.configure("mystyle.Treeview", highlightthickness=0, bd=0,
                        font=('Calibri', 11), rowheight=50)  # Modify the font of the body
        style.configure("mystyle.Treeview.Heading", font=('Calibri', 14, 'bold'))  # Modify the font of the headings
        style.layout("mystyle.Treeview", [('mystyle.Treeview.treearea', {'sticky': 'nswe'})])  # Remove the borders

        self.TableFrame = Frame(self.window, bg=COLOR.CL1)
        self.TableFrame.pack(side='top', fill='x')

        list = ttk.Treeview(self.TableFrame, selectmode='browse', show='headings', style="mystyle.Treeview", height=5)
        vsb = Scrollbar(self.TableFrame, orient='vertical', command=list.yview, width='10m')
        hsb = Scrollbar(self.TableFrame, orient='horizontal', command=list.xview, width='10m')

        list.configure(xscrollcommand=hsb.set)
        list.configure(yscrollcommand=vsb.set)
        vsb.pack(side='right', fill='y')
        list.pack(side='top', fill='both')
        hsb.pack(side='bottom', fill='x')

        list['columns'] = self.CustomerTable

        list.tag_configure('odd', background='#E8E8E8', font=FONT.ENGLISH_SMALL)
        list.tag_configure('even', background='#DFDFDF', font=FONT.ENGLISH_SMALL)

        for item in self.CustomerTable:
            list.column(item, width=int(100 * Globals.ppm))
            list.heading(item, text=cft(item))

        for num, item in enumerate(DATABASE.customer_data):
            # for d in item:
            d = []
            for i in item:
                d.append(cft(str(i)))
            if num % 2 == 1:
                list.insert('', 'end', text=cft('item{}'.format(num)), values=d, tags='odd')
            else:
                list.insert('', 'end', text=cft('item{}'.format(num)), values=d, tags='even')

    def create_list_report(self):
        global DATABASE
        if self.TableFrame is not None:
            self.TableFrame.destroy()
            self.TableFrame.pack_forget()
            self.TableFrame = None
            self.btnframe2.destroy()
            self.btnframe2.pack_forget()
            self.btnframe2 = None

        self.TabelSelect = 'Report'
        self.btnframe2 = Frame(self.window, bg=COLOR.CL1)
        self.btnframe2.pack(side='top', padx='1m', pady='1m', fill='x')

        Button(self.btnframe2, text=cft('نمایش نمودار'), bg=COLOR.CL1, fg=COLOR.CL2, font=FONT.PERSIAN_SMALL, width=10,
               command=lambda: self.show_plot(list)).pack(
            side='right', padx='5m', pady='3m')

        Button(self.btnframe2, text=cft('چاپ'), bg=COLOR.CL1, fg=COLOR.CL2, font=FONT.PERSIAN_SMALL, width=10,
               command=lambda: self.report(list)).pack(
            side='right', padx='5m', pady='3m')

        Button(self.btnframe2, text=cft('خروجی اکسل'), bg=COLOR.CL1, fg=COLOR.CL2, font=FONT.PERSIAN_SMALL, width=15,
               command=self.excel_report).pack(
            side='right', padx='5m', pady='3m')

        style = ttk.Style()
        style.configure("mystyle.Treeview", highlightthickness=0, bd=0,
                        font=('Calibri', 11), rowheight=50)  # Modify the font of the body
        style.configure("mystyle.Treeview.Heading", font=('Calibri', 14, 'bold'))  # Modify the font of the headings
        style.layout("mystyle.Treeview", [('mystyle.Treeview.treearea', {'sticky': 'nswe'})])  # Remove the borders

        self.TableFrame = Frame(self.window, bg=COLOR.CL1)
        self.TableFrame.pack(side='top', fill='x')

        list = ttk.Treeview(self.TableFrame, selectmode='browse', show='headings', style="mystyle.Treeview", height=5)
        vsb = Scrollbar(self.TableFrame, orient='vertical', command=list.yview, width='10m')
        hsb = Scrollbar(self.TableFrame, orient='horizontal', command=list.xview, width='10m')

        list.configure(xscrollcommand=hsb.set)
        list.configure(yscrollcommand=vsb.set)
        vsb.pack(side='right', fill='y')
        list.pack(side='top', fill='both')
        hsb.pack(side='bottom', fill='x')
        list['columns'] = self.Report

        list.tag_configure('odd', background='#E8E8E8', font=FONT.ENGLISH_SMALL)
        list.tag_configure('even', background='#DFDFDF', font=FONT.ENGLISH_SMALL)

        for item in self.Report:
            list.column(item, width=int(100 * Globals.ppm))
            list.heading(item, text=cft(item))
        number = 0
        for num, item in enumerate(reversed(DATABASE.report_data)):
            # for d in item:
            d = []
            for i in item:
                d.append(str(i))
            if num % 2 == 1:
                list.insert('', 'end', text=cft('item{}'.format(num)), values=d, tags='odd')
            else:
                list.insert('', 'end', text=cft('item{}'.format(num)), values=d, tags='even')
            number += 1
            if number >= Globals.SHOW_NUMBER:
                break

    def create_list_done(self):
        global DATABASE
        if self.TableFrame is not None:
            self.TableFrame.destroy()
            self.TableFrame.pack_forget()
            self.TableFrame = None
            self.btnframe2.destroy()
            self.btnframe2.pack_forget()
            self.btnframe2 = None

        self.TabelSelect = 'Done'
        self.btnframe2 = Frame(self.window, bg=COLOR.CL1)
        self.btnframe2.pack(side='top', padx='1m', pady='1m', fill='x')

        if self.edit_done_test_enable:
            Button(self.btnframe2, text=cft('ویرایش'), bg=COLOR.CL1, fg=COLOR.CL2, font=FONT.PERSIAN_SMALL, width=10,
                   command=lambda: self.edit_done(list, 'edit')).pack(
                side='right', padx='5m', pady='3m')
        # Button(self.btnframe2, text='چاپ', bg=COLOR.CL1, fg=COLOR.CL2, font=FONT.PERSIAN_SMALL, width=10,
        #        command=None).pack(
        #     side='right', padx='5m', pady='3m')

        style = ttk.Style()
        style.configure("mystyle.Treeview", highlightthickness=0, bd=0,
                        font=('Calibri', 11), rowheight=50)  # Modify the font of the body
        style.configure("mystyle.Treeview.Heading", font=('Calibri', 14, 'bold'))  # Modify the font of the headings
        style.layout("mystyle.Treeview", [('mystyle.Treeview.treearea', {'sticky': 'nswe'})])  # Remove the borders

        self.TableFrame = Frame(self.window, bg=COLOR.CL1)
        self.TableFrame.pack(side='top', fill='x')

        list = ttk.Treeview(self.TableFrame, selectmode='browse', show='headings', style="mystyle.Treeview", height=5)
        vsb = Scrollbar(self.TableFrame, orient='vertical', command=list.yview, width='10m')
        hsb = Scrollbar(self.TableFrame, orient='horizontal', command=list.xview, width='10m')

        list.configure(xscrollcommand=hsb.set)
        list.configure(yscrollcommand=vsb.set)
        vsb.pack(side='right', fill='y')
        list.pack(side='top', fill='both')
        hsb.pack(side='bottom', fill='x')
        list['columns'] = self.Test

        list.tag_configure('odd', background='#E8E8E8', font=FONT.ENGLISH_SMALL)
        list.tag_configure('even', background='#DFDFDF', font=FONT.ENGLISH_SMALL)

        for item in self.Test:
            list.column(item, width=int(100 * Globals.ppm))
            list.heading(item, text=cft(item))
        a = []
        number = 0
        for num, item in enumerate(reversed(DATABASE.test_data)):
            # for d in item:
            # d = []
            # for i in item:
            #     d.append(cft(str(i)))
            if num % 2 == 1:
                list.insert('', 'end', text=cft('item{}'.format(num)), values=item, tags='odd')
            else:
                list.insert('', 'end', text=cft('item{}'.format(num)), values=item, tags='even')
            number += 1
            if number >= Globals.SHOW_NUMBER:
                break

    # Customer Add-Remove Button
    def add_customer(self):

        win = Toplevel(self.window)
        win.title("ADD Customer")
        win.geometry("+100+100")
        win.config(bg=COLOR.CL1)

        Label(win, text=cft('مشتری'), font=FONT.PERSIAN_SMALL, anchor='nw', bg=COLOR.CL1, fg=COLOR.CL2).grid(row=0,
                                                                                                             column=1,
                                                                                                             padx='5m',
                                                                                                             pady='5m')
        v1 = StringVar()
        v1.set('Customer')
        Entry(win, width=15, font=FONT.PERSIAN_SMALL, bg=COLOR.CL1, fg=COLOR.CL2, textvariable=v1).grid(row=0, column=0,
                                                                                                        padx='5m',
                                                                                                        pady='5m')

        Label(win, text=cft('شماره تماس'), font=FONT.PERSIAN_SMALL, anchor='nw', bg=COLOR.CL1, fg=COLOR.CL2).grid(row=1,
                                                                                                                  column=1,
                                                                                                                  padx='5m',
                                                                                                                  pady='5m')
        v2 = StringVar()
        v2.set("09300000000")
        Entry(win, width=15, font=FONT.PERSIAN_SMALL, bg=COLOR.CL1, fg=COLOR.CL2, textvariable=v2).grid(row=1, column=0,
                                                                                                        padx='5m',
                                                                                                        pady='5m')

        Button(win, text=cft('اضافه کردن'), font=FONT.PERSIAN_SMALL, bg=COLOR.CL1, fg=COLOR.CL2, width=15,
               command=lambda: self.add_customer_btn(win, v1, v2)).grid(row=2,
                                                                        column=1,
                                                                        padx='5m',
                                                                        pady='5m')

    def add_customer_btn(self, win, var1, var2):
        global DATABASE
        win.destroy()
        dict = {'Customer': var1.get(), 'PhoneNumber': var2.get()}
        DATABASE.add_customer(dict)
        self.create_list_customer()

    def remove_customer(self, list):
        global DATABASE
        remove_item = list.selection()

        if len(remove_item) > 0:
            item = list.item(list.selection()[0])['values']
            list.delete(list.selection()[0])
            id = item[0]
            DATABASE.remove_customer(id)

    # Operator Add_Remove Button
    def add_operator(self):

        win = Toplevel(self.window)
        win.title("ADD operator")
        win.geometry("+100+100")
        win.config(bg=COLOR.CL1)

        Label(win, text=cft('اپراتور'), font=FONT.PERSIAN_SMALL, anchor='nw', bg=COLOR.CL1, fg=COLOR.CL2).grid(row=0,
                                                                                                               column=1,
                                                                                                               padx='5m',
                                                                                                               pady='5m')
        v1 = StringVar()
        v1.set('operator')
        Entry(win, width=15, font=FONT.PERSIAN_SMALL, bg=COLOR.CL1, fg=COLOR.CL2, textvariable=v1).grid(row=0, column=0,
                                                                                                        padx='5m',
                                                                                                        pady='5m')

        Button(win, text=cft('اضافه کردن'), font=FONT.PERSIAN_SMALL, bg=COLOR.CL1, fg=COLOR.CL2, width=15,
               command=lambda: self.add_operator_btn(win, v1)).grid(row=2,
                                                                    column=1,
                                                                    padx='5m',
                                                                    pady='5m')

    def add_operator_btn(self, win, var1):
        global DATABASE
        win.destroy()
        # dict = {'Customer': var1.get(), 'PhoneNumber': var2.get()}
        DATABASE.add_operator(var1.get())
        self.create_list_operator()

    def remove_operator(self, list):
        global DATABASE
        remove_item = list.selection()

        if len(remove_item) > 0:
            item = list.item(list.selection()[0])['values']
            list.delete(list.selection()[0])
            id = item[0]
            DATABASE.remove_operator(id)

    # Test Add  Button
    def add_test(self, refresh=None):
        global DATABASE
        if len(DATABASE.customer_data) == 0:
            messagebox.showerror('Empty customer', cft('ابتدا مشتری را اضافه کنید'))
            return

        if len(DATABASE.operator_data) == 0:
            messagebox.showerror('Empty Operator', cft('ابتدا اپراتور را اضافه کنید'))
            return
        win = Toplevel(self.window)
        win.title("ADD Test")
        win.geometry("+10+10")
        win.config(bg=COLOR.CL1)

        test_item_label = {
            'ID': 'ID:',
            'OperatorName': 'نام اپراتور تست :',
            'CustomerName': 'نام مشتری :',
            'YearProduction': 'تاریخ تولید سیلندر‌:',
            'YearExpiration': 'تاریخ انقضا سیلندر:',
            'WeightFirst': 'وزن حک شده :',
            'WeightSecond': 'وزن اندازه گیری شده :',
            'VolumeNominal': 'حجم اسمی مخزن‌ :',
            'PressureValueTest': 'فشار تست :',
            'CylinderType': 'نوع مخزن:',
            'CylinderNumber': 'شماره مخزن:',
            'CylinderSN': 'شماره سریال مخزن:',
            'CylinderCompany': 'نام شرکت تولید کننده :',
            'ReceptionNumber': 'شماره پذیرش :',
            'IsLegible': 'خوانا بودن اطلاعات مخزن :',
            'IsNoDamage': 'مخزن عیب ظاهری ندارد :',
            'IsNoSpecialDamage': 'مخرن عیب خاض ظاهری ندارد :',
            'IsLeakTestOk': 'مخزن نشتی ندارد:',
            'IsThreadOk': 'رزوه ی مخزن مشکل ندارد:',
            'IsWallThicknessOk': 'ضخامت مخزن مجاز است : '
        }
        test_item_var = {
            'ID': StringVar(),
            'OperatorName': StringVar(),
            'CustomerName': StringVar(),
            'YearProduction': StringVar(),
            'YearExpiration': StringVar(),
            'WeightFirst': StringVar(),
            'WeightSecond': StringVar(),
            'VolumeNominal': StringVar(),
            'PressureValueTest': StringVar(),
            'CylinderType': StringVar(),
            'CylinderNumber': StringVar(),
            'CylinderSN': StringVar(),
            'CylinderCompany': StringVar(),
            'ReceptionNumber': StringVar(),
            'IsLegible': StringVar(),
            'IsNoDamage': StringVar(),
            'IsNoSpecialDamage': StringVar(),
            'IsLeakTestOk': StringVar(),
            'IsThreadOk': StringVar(),
            'IsWallThicknessOk': StringVar()
        }
        test_item_default = {
            'ID': StringVar(),
            'OperatorName': StringVar(),
            'CustomerName': StringVar(),
            'YearProduction': StringVar(),
            'YearExpiration': StringVar(),
            'WeightFirst': StringVar(),
            'WeightSecond': StringVar(),
            'VolumeNominal': StringVar(),
            'PressureValueTest': StringVar(),
            'CylinderType': StringVar(),
            'CylinderNumber': StringVar(),
            'CylinderSN': StringVar(),
            'CylinderCompany': StringVar(),
            'ReceptionNumber': StringVar(),
            'IsLegible': StringVar(),
            'IsNoDamage': StringVar(),
            'IsNoSpecialDamage': StringVar(),
            'IsLeakTestOk': StringVar(),
            'IsThreadOk': StringVar(),
            'IsWallThicknessOk': StringVar()
        }
        try:
            f = open(FILE_NAME.DefaultTestInfo, 'r', encoding='utf-8')
            for line in f:
                test_item_default[line[:-1].split(':')[0]].set(line[:-1].split(':')[1])
        except:
            messagebox.showerror('wrong value', cft('فایل تنظیمات دیفالت را به برنامه اضافه کنید'), parent=win)
        test_item_default['ID'].set(str(DATABASE.simfa_id))
        column = 0
        for num, item in enumerate(test_item_label):
            if num % 10 == 0:
                column += 2
            Label(win, text=cft(test_item_label[item]), bg=COLOR.CL1, fg=COLOR.CL2, font=FONT.PERSIAN_SMALL).grid(
                row=num % 10, column=column, padx='5m', pady='5m')

            if 'OperatorName' == item:
                sp = Spinbox(win, bg=COLOR.CL1, fg=COLOR.CL2, width=15, font=FONT.PERSIAN_SMALL,
                             textvariable=test_item_var['OperatorName'], state='readonly')
                sp.grid(row=num % 10, column=column + 1, padx='5m', pady='5m')

                a = []
                for i in DATABASE.operator_data:
                    a.append(str(i[0]) + '-' + cft(i[1]))

                sp.config(values=tuple(a))
                n = 0
                for num, i in enumerate(DATABASE.operator_data):
                    if str(i[0]) == test_item_default[item].get():
                        n = num
                        break
                for i in range(n):
                    sp.invoke('buttonup')

            elif 'CustomerName' == item:
                a = []
                for i in DATABASE.customer_data:
                    a.append(str(i[0]) + '-' + cft(i[1]))
                sp = ttk.Combobox(win, width=15, font=FONT.PERSIAN_SMALL,
                                  textvariable=test_item_var['CustomerName'], state='readonly')
                sp.grid(row=num % 10, column=column + 1, padx='5m', pady='5m')
                sp.config(values=tuple(a))

                if len(a) > 0:

                    try:
                        if int(test_item_default[item].get()) < len(a):
                            sp.current(int(test_item_default[item].get()) - 1)
                        else:
                            sp.current(0)
                    except:
                        sp.current(0)

            elif 'Is' in item:
                sp = Spinbox(win, bg=COLOR.CL1, fg=COLOR.CL2, width=15, font=FONT.PERSIAN_SMALL,
                             textvariable=test_item_var[item], state='readonly')
                sp.grid(row=num % 10, column=column + 1, padx='5m', pady='5m')
                sp.config(values=(cft('بله'), cft('خیر')))
                if '0' in test_item_default[item].get():
                    sp.invoke('buttonup')
            elif 'CylinderNumber' == item:
                sp = Spinbox(win, bg=COLOR.CL1, fg=COLOR.CL2, width=15, font=FONT.PERSIAN_SMALL,
                             textvariable=test_item_var[item], state='readonly')
                sp.grid(row=num % 10, column=column + 1, padx='5m', pady='5m')
                sp.config(values=('1', '2'))

                if test_item_default[item].get() == '2':
                    sp.invoke('buttonup')

            elif 'CylinderType' == item:
                sp = Spinbox(win, bg=COLOR.CL1, fg=COLOR.CL2, width=15, font=FONT.PERSIAN_SMALL,
                             textvariable=test_item_var[item], state='readonly')
                sp.grid(row=num % 10, column=column + 1, padx='5m', pady='5m')
                sp.config(values=(cft('۱-فلزی'), cft('۲-کامپوزیتی'), cft('۳-'), cft('۴-')))

                if int(test_item_default[item].get()) == 2:
                    sp.invoke('buttonup')
                elif int(test_item_default[item].get()) == 3:
                    sp.invoke('buttonup')
                    sp.invoke('buttonup')

                elif int(test_item_default[item].get()) == 4:
                    sp.invoke('buttonup')
                    sp.invoke('buttonup')
                    sp.invoke('buttonup')

            elif 'CylinderCompany' == item:
                try:
                    f = open(FILE_NAME.CylinderCompany, 'r', encoding='utf-8')
                    a = []
                    for i in f:
                        a.append(i[:-1])
                except:
                    pass
                sp = ttk.Combobox(win, width=15, font=FONT.PERSIAN_SMALL,
                                  textvariable=test_item_var[item], state='readonly')
                sp.grid(row=num % 10, column=column + 1, padx='5m', pady='5m')
                sp.config(values=(tuple(a)))
                sp.current(0)
                for num, i in enumerate(a):
                    if test_item_default[item].get() == i:
                        sp.current(num)

            elif 'ID' == item:
                Label(win, text=test_item_default['ID'].get(), bg=COLOR.CL1, fg=COLOR.CL2,
                      font=FONT.PERSIAN_SMALL).grid(
                    row=num % 10, column=column + 1, padx='5m', pady='5m')

            else:
                test_item_var[item].set(test_item_default[item].get())
                Entry(win, font=FONT.ENGLISH_SMALL, bg=COLOR.CL1, fg=COLOR.CL2, width=15,
                      textvariable=test_item_var[item]).grid(row=num % 10, column=column + 1, padx='5m', pady='5m')

        Button(win, text=cft('ذخیره کردن'), bg=COLOR.CL1, fg=COLOR.CL2, activebackground=COLOR.CL2,
               font=FONT.PERSIAN_SMALL,
               width='15', command=lambda: self.add_test_btn(None, win, test_item_var, refresh=refresh)).grid(row=10,
                                                                                                              column=3,
                                                                                                              padx='5m',
                                                                                                              pady='5m')
        win.bind('<Return>', lambda event: self.add_test_btn(event, win, test_item_var, refresh=refresh))

    def add_test_btn(self, event, win, test_val, id_number=None, flag=False, refresh=None):
        global DATABASE

        test_item_var = {
            'OperatorName': None,
            'CustomerName': None,
            'YearProduction': None,
            'YearExpiration': None,
            'WeightFirst': None,
            'WeightSecond': None,
            'VolumeNominal': None,
            'PressureValueTest': None,
            'CylinderType': None,
            'CylinderNumber': None,
            'CylinderSN': None,
            'CylinderCompany': None,
            'ReceptionNumber': None,
            'IsLegible': None,
            'IsNoDamage': None,
            'IsNoSpecialDamage': None,
            'IsLeakTestOk': None,
            'IsThreadOk': None,
            'IsWallThicknessOk': None
        }
        for item in test_val:
            if 'Is' in item:
                if test_val[item].get() == cft('بله'):
                    test_item_var[item] = 'True'
                else:
                    test_item_var[item] = 'False'
            elif 'OperatorName' == item:
                test_item_var[item] = test_val[item].get().split('-')[1]
            elif 'CustomerName' == item:
                test_item_var[item] = test_val[item].get().split('-')[1]

            elif 'CylinderType' == item:
                if '۱' in test_val[item].get():
                    test_item_var[item] = '1'
                elif '۲' in test_val[item].get():
                    test_item_var[item] = '2'
                elif '۳' in test_val[item].get():
                    test_item_var[item] = '3'
                elif '۴' in test_val[item].get():
                    test_item_var[item] = '4'

            else:
                test_item_var[item] = test_val[item].get()

        try:
            float(test_item_var['WeightFirst'])
        except:
            messagebox.showerror('wrong value', cft('وزن حک شده را با فرمت درست وارد کنید'), parent=win)
            return
        try:
            float(test_item_var['WeightSecond'])
        except:
            messagebox.showerror('wrong value', cft('وزن اندازه گیری شده را با فرمت درست وارد کنید'), parent=win)
            return

        try:
            float(test_item_var['VolumeNominal'])
        except:
            messagebox.showerror('wrong value', cft('حجم اسمی مخزن را با فرمت درست وارد کنید'), parent=win)
            return
        try:
            int(test_item_var['PressureValueTest'])
        except:
            messagebox.showerror('wrong value', cft('فشار تست را با فرمت درست وارد کنید'), parent=win)
            return
        try:
            int(test_item_var['ReceptionNumber'])
        except:
            messagebox.showerror('wrong value', cft('شماره پذیرش را با فرمت درست وارد کنید'), parent=win)
            return
        try:
            int(test_item_var['CylinderNumber'])
        except:
            messagebox.showerror('wrong value', cft('شماره سریال مخزن را با فرمت درست وارد کنید'), parent=win)
            return

        win.destroy()
        state = None
        if id_number is not None:
            state = 'edit'
        if flag:
            DATABASE.edit_simfa_test(test_item_var, id_number)
        else:
            DATABASE.add_simfa_test(test_item_var, id_number, state)
        if refresh is None:
            self.create_list_test("add")
        else:
            refresh.create_window()

    def remove_test_btn(self, list):
        global DATABASE
        remove_item = list.selection()
        if len(remove_item) > 0:
            item = list.item(list.selection()[0])['values']
            list.delete(list.selection()[0])
            id = item[0]
            DATABASE.remove_simfa_test(id)
        else:
            messagebox.showerror('No Selection', 'لطفا ابتدا یک تست را انتخاب کنید.')

    def edit_test_btn(self, list):
        global DATABASE
        remove_item = list.selection()
        if len(remove_item) == 0:
            messagebox.showerror('No Selection', cft('لطفا ابتدا یک تست را انتخاب کنید.'))
            return
        item = list.item(list.selection()[0])['values']
        id = item[0]

        report = None
        for item in DATABASE.report_data:
            if id == item[0]:
                report = item
                break

        flag = False
        if report[3] == 'Done':
            messagebox.showinfo('Test is Done', cft('تست قبلا انجام شده است .'))
            flag = True

        test = None

        for item in DATABASE.simfa_data:
            if id == item[0]:
                test = item
                break
        self.edit_test(test, report, flag)

    def edit_test(self, values, report, flag):
        global DATABASE

        win = Toplevel(self.window)
        win.title("Edit Test")
        win.geometry("+10+10")
        win.config(bg=COLOR.CL1)

        test_item_label = {
            'ID': 'ID:',
            'OperatorName': 'نام اپراتور تست :',
            'CustomerName': 'نام مشتری :',
            'YearProduction': 'تاریخ تولید سیلندر‌:',
            'YearExpiration': 'تاریخ انقضا سیلندر:',
            'WeightFirst': 'وزن حک شده :',
            'WeightSecond': 'وزن اندازه گیری شده :',
            'VolumeNominal': 'حجم اسمی مخزن‌ :',
            'PressureValueTest': 'فشار تست :',
            'CylinderType': 'نوع مخزن:',
            'CylinderNumber': 'شماره مخزن:',
            'CylinderSN': 'شماره سریال مخزن:',
            'CylinderCompany': 'نام شرکت تولید کننده :',
            'ReceptionNumber': 'شماره پذیرش :',
            'IsLegible': 'خوانا بودن اطلاعات مخزن :',
            'IsNoDamage': 'مخزن عیب ظاهری ندارد :',
            'IsNoSpecialDamage': 'مخرن عیب خاض ظاهری ندارد :',
            'IsLeakTestOk': 'مخزن نشتی ندارد:',
            'IsThreadOk': 'رزوه ی مخزن مشکل ندارد:',
            'IsWallThicknessOk': 'ضخامت مخزن مجاز است : '
        }
        test_item_var = {
            'ID': StringVar(),
            'OperatorName': StringVar(),
            'CustomerName': StringVar(),
            'YearProduction': StringVar(),
            'YearExpiration': StringVar(),
            'WeightFirst': StringVar(),
            'WeightSecond': StringVar(),
            'VolumeNominal': StringVar(),
            'PressureValueTest': StringVar(),
            'CylinderType': StringVar(),
            'CylinderNumber': StringVar(),
            'CylinderSN': StringVar(),
            'CylinderCompany': StringVar(),
            'ReceptionNumber': StringVar(),
            'IsLegible': StringVar(),
            'IsNoDamage': StringVar(),
            'IsNoSpecialDamage': StringVar(),
            'IsLeakTestOk': StringVar(),
            'IsThreadOk': StringVar(),
            'IsWallThicknessOk': StringVar()
        }
        test_item_default = {
            'ID': StringVar(),
            'OperatorName': StringVar(),
            'CustomerName': StringVar(),
            'YearProduction': StringVar(),
            'YearExpiration': StringVar(),
            'WeightFirst': StringVar(),
            'WeightSecond': StringVar(),
            'VolumeNominal': StringVar(),
            'PressureValueTest': StringVar(),
            'CylinderType': StringVar(),
            'CylinderNumber': StringVar(),
            'CylinderSN': StringVar(),
            'CylinderCompany': StringVar(),
            'ReceptionNumber': StringVar(),
            'IsLegible': StringVar(),
            'IsNoDamage': StringVar(),
            'IsNoSpecialDamage': StringVar(),
            'IsLeakTestOk': StringVar(),
            'IsThreadOk': StringVar(),
            'IsWallThicknessOk': StringVar()
        }

        for num, item in enumerate(self.Simfa):
            if item in test_item_default:
                test_item_default[item].set(values[num])

        test_item_default['CustomerName'].set(0)
        for item in DATABASE.customer_data:
            if item[1] == report[1]:
                test_item_default['CustomerName'].set(str(item[0]))
                break
        test_item_default['OperatorName'].set(0)
        for item in DATABASE.operator_data:
            if item[1] == report[2]:
                test_item_default['OperatorName'].set(str(item[0]))
                break

        test_item_default['ID'].set(str(values[0]))
        column = 0
        for num, item in enumerate(test_item_label):
            if num % 10 == 0:
                column += 2
            Label(win, text=cft(test_item_label[item]), bg=COLOR.CL1, fg=COLOR.CL2, font=FONT.PERSIAN_SMALL).grid(
                row=num % 10, column=column, padx='5m', pady='5m')

            if 'OperatorName' == item:
                sp = Spinbox(win, bg=COLOR.CL1, fg=COLOR.CL2, width=15, font=FONT.PERSIAN_SMALL,
                             textvariable=test_item_var['OperatorName'])
                sp.grid(row=num % 10, column=column + 1, padx='5m', pady='5m')

                a = []
                for i in DATABASE.operator_data:
                    a.append(str(i[0]) + '-' + cft(i[1]))

                sp.config(values=tuple(a))
                n = 0
                for num, i in enumerate(DATABASE.operator_data):
                    if str(i[0]) == test_item_default[item].get():
                        n = num
                        break
                for i in range(n):
                    sp.invoke('buttonup')

            elif 'CustomerName' == item:
                a = []
                for i in DATABASE.customer_data:
                    a.append(str(i[0]) + '-' + cft(i[1]))
                sp = ttk.Combobox(win, width=15, font=FONT.PERSIAN_SMALL,
                                  textvariable=test_item_var['CustomerName'])
                sp.grid(row=num % 10, column=column + 1, padx='5m', pady='5m')
                sp.config(values=tuple(a))

                n = 0
                for num, i in enumerate(DATABASE.customer_data):
                    if str(i[0]) == test_item_default[item].get():
                        n = num
                        break
                try:
                    if n < len(a):
                        sp.current(n)
                    else:
                        sp.current(0)
                except:
                    sp.current(0)

            elif 'Is' in item:
                sp = Spinbox(win, bg=COLOR.CL1, fg=COLOR.CL2, width=15, font=FONT.PERSIAN_SMALL,
                             textvariable=test_item_var[item])
                sp.grid(row=num % 10, column=column + 1, padx='5m', pady='5m')
                sp.config(values=(cft('بله'), cft('خیر')))
                if '0' in test_item_default[item].get():
                    sp.invoke('buttonup')
            elif 'CylinderNumber' == item:
                sp = Spinbox(win, bg=COLOR.CL1, fg=COLOR.CL2, width=15, font=FONT.PERSIAN_SMALL,
                             textvariable=test_item_var[item])
                sp.grid(row=num % 10, column=column + 1, padx='5m', pady='5m')
                sp.config(values=('1', '2'))

                if test_item_default[item].get() == '2':
                    sp.invoke('buttonup')

            elif 'CylinderType' == item:
                sp = Spinbox(win, bg=COLOR.CL1, fg=COLOR.CL2, width=15, font=FONT.PERSIAN_SMALL,
                             textvariable=test_item_var[item])
                sp.grid(row=num % 10, column=column + 1, padx='5m', pady='5m')
                sp.config(values=(cft('۱-فلزی'), cft('۲-کامپوزیتی'), cft('۳-'), cft('۴-')))

                if int(test_item_default[item].get()) == 2:
                    sp.invoke('buttonup')
                elif int(test_item_default[item].get()) == 3:
                    sp.invoke('buttonup')
                    sp.invoke('buttonup')

                elif int(test_item_default[item].get()) == 4:
                    sp.invoke('buttonup')
                    sp.invoke('buttonup')
                    sp.invoke('buttonup')

            elif 'CylinderCompany' == item:
                try:
                    f = open(FILE_NAME.CylinderCompany, 'r', encoding='utf-8')
                    a = []
                    for i in f:
                        a.append(i[:-1])
                except:
                    pass
                sp = ttk.Combobox(win, width=15, font=FONT.PERSIAN_SMALL,
                                  textvariable=test_item_var[item])
                sp.grid(row=num % 10, column=column + 1, padx='5m', pady='5m')
                sp.config(values=(tuple(a)))
                sp.current(0)
                for num, i in enumerate(a):
                    if test_item_default[item].get() == i:
                        sp.current(num)

            elif 'ID' == item:
                Label(win, text=test_item_default['ID'].get(), bg=COLOR.CL1, fg=COLOR.CL2,
                      font=FONT.PERSIAN_SMALL).grid(
                    row=num % 10, column=column + 1, padx='5m', pady='5m')

            else:
                test_item_var[item].set(test_item_default[item].get())
                if flag and item == 'PressureValueTest':
                    Entry(win, font=FONT.ENGLISH_SMALL, bg=COLOR.CL1, fg=COLOR.CL2, width=15,
                          textvariable=test_item_var[item], state='disable').grid(row=num % 10, column=column + 1,
                                                                                  padx='5m', pady='5m')
                else:
                    Entry(win, font=FONT.ENGLISH_SMALL, bg=COLOR.CL1, fg=COLOR.CL2, width=15,
                          textvariable=test_item_var[item]).grid(row=num % 10, column=column + 1, padx='5m', pady='5m')

        Button(win, text=cft('ویرایش'), bg=COLOR.CL1, fg=COLOR.CL2, activebackground=COLOR.CL2, font=FONT.PERSIAN_SMALL,
               width='15', command=lambda: self.add_test_btn(None, win, test_item_var, report[0], flag)).grid(row=10,
                                                                                                              column=3,
                                                                                                              padx='5m',
                                                                                                              pady='5m')
        win.bind('<Return>', lambda event: self.add_test_btn(event, win, test_item_var))

    def default_test(self, list):
        remove_item = list.selection()
        if len(remove_item) == 0:
            messagebox.showerror('No Selection', 'لطفا ابتدا یک تست را انتخاب کنید.')
            return
        item_default = {
            'OperatorName': StringVar(),
            'CustomerName': StringVar(),
            'YearProduction': StringVar(),
            'YearExpiration': StringVar(),
            'WeightFirst': StringVar(),
            'WeightSecond': StringVar(),
            'VolumeNominal': StringVar(),
            'PressureValueTest': StringVar(),
            'CylinderType': StringVar(),
            'CylinderNumber': StringVar(),
            'CylinderSN': StringVar(),
            'CylinderCompany': StringVar(),
            'ReceptionNumber': StringVar(),
            'IsLegible': StringVar(),
            'IsNoDamage': StringVar(),
            'IsNoSpecialDamage': StringVar(),
            'IsLeakTestOk': StringVar(),
            'IsThreadOk': StringVar(),
            'IsWallThicknessOk': StringVar()
        }
        item = list.item(list.selection()[0])['values']
        id = item[0]
        for num, i in enumerate(self.Simfa):
            if i in item_default:
                item_default[i].set(str(item[num]))
        for j in DATABASE.report_data:
            if j[0] == id:
                item_default['CustomerName'].set('0')
                for customer in DATABASE.customer_data:
                    if j[1] == cft(customer[1]):
                        item_default['CustomerName'].set(str(customer[0]))
                item_default['OperatorName'].set('0')
                for operator in DATABASE.operator_data:
                    if j[2] == cft(operator[1]):
                        item_default['OperatorName'].set(str(operator[0]))

                break
        txt = ''
        f = open(FILE_NAME.DefaultTestInfo, 'w', encoding='utf-8')
        for item in item_default:
            s = str(item) + ":"
            if 'Is' in item:
                if 'True' in item_default[item].get():
                    s += '1'
                else:
                    s += '0'
            else:
                s += item_default[item].get()

            s += '\n'
            f.write(s)
        f.close()

        messagebox.showinfo('Default', cft('اطلاعات تست {} به عنوان پیش فرض تنظیم شد.'.format(id)))

    def send_data(self, list):
        remove_item = list.selection()
        if len(remove_item) == 0:
            messagebox.showerror('No Selection', cft('لطفا ابتدا یک تست را انتخاب کنید.'))
            return
        item = list.item(list.selection()[0])['values']
        id = item[0]
        DATABASE.send_data_to_simfa(int(id))

    #########report
    def show_plot(self, list):
        remove_item = list.selection()
        if len(remove_item) == 0:
            messagebox.showerror('No Selection', cft('لطفا ابتدا یک تست را انتخاب کنید.'))
            return
        item = list.item(list.selection()[0])['values']
        if item[3] != 'Done':
            messagebox.showerror("No Done Test", cft('لطفا ابتدا تست را انجام دهید.'))
            return
        id = item[0]

        # the figure that will contain the plot
        try:
            y1 = []
            f = open(FILE_NAME.WeightTestFile + str(id) + '.HsW', 'r', encoding='utf-8')
            for i in f:
                if 'Time' in i:
                    continue
                y1.append(float(i.split('|')[1][:-2]))
            f.close()

            y2 = []
            f = open(FILE_NAME.PressureTestFile + str(id) + '.HsP', 'r', encoding='utf-8')
            for i in f:
                if 'Time' in i:
                    continue
                y2.append(float(i.split('|')[1][:-2]))
            f.close()
        except:
            messagebox.showerror('file missed', cft('فایل های تست ذخیره شده پیدا نشد.'))
            return

        win = Toplevel(self.window, bg=COLOR.CL1)
        win.title('Plot')
        win.geometry('+10+10')
        Label(win, text=cft('نمودار زمان - وزن'), bg=COLOR.CL1, fg=COLOR.CL2, font=FONT.PERSIAN_SMALL).pack(side='top',
                                                                                                            padx='5m',
                                                                                                            pady='5m')
        fig_weight = Figure(figsize=(12, 3), dpi=win.winfo_fpixels('1i'))
        plot1 = fig_weight.add_subplot(111)
        plot1.plot(y1)
        fig_weight.savefig(FILE_NAME.WeightPlot + str(id) + '.png')
        Weight_canvas = FigureCanvasTkAgg(fig_weight, master=win)
        Weight_canvas.draw()
        Weight_canvas.get_tk_widget().pack(side='top', padx='5m', pady='5m')
        Label(win, text=cft('نمودار زمان - فشار'), bg=COLOR.CL1, fg=COLOR.CL2, font=FONT.PERSIAN_SMALL).pack(side='top',
                                                                                                             padx='5m',
                                                                                                             pady='5m')
        fig_Pressure = Figure(figsize=(12, 3), dpi=win.winfo_fpixels('1i'))
        plot2 = fig_Pressure.add_subplot(111)
        plot2.plot(y2)
        Pressure_canvas = FigureCanvasTkAgg(fig_Pressure, master=win)
        fig_Pressure.savefig(FILE_NAME.PressurePlot + str(id) + '.png')

        Pressure_canvas.draw()
        Pressure_canvas.get_tk_widget().pack(side='top', padx='5m', pady='5m')

        Button(win, text='OK', width=10, bg=COLOR.CL1, fg=COLOR.CL2, font=FONT.ENGLISH_SMALL,
               command=lambda: self.ok_plot(win)).pack(side='top',
                                                       padx='5m', pady='5m')

    def ok_plot(self, win):
        win.destroy()

    def report(self, list):
        global DATABASE, root
        remove_item = list.selection()
        if len(remove_item) == 0:
            messagebox.showerror('No Selection', cft('لطفا ابتدا یک تست را انتخاب کنید.'))
            return
        item = list.item(list.selection()[0])['values']
        if item[3] != 'Done':
            messagebox.showerror("No Done Test", 'لطفا ابتدا تست را انجام دهید.')
            return
        id = item[0]
        # DATABASE.create_pdf(id)

        # the figure that will contain the plot
        try:
            y1 = []
            f = open(FILE_NAME.WeightTestFile + str(id) + '.HsW', 'r', encoding='utf-8')
            for i in f:
                if 'Time' in i:
                    continue
                y1.append(float(i.split('|')[1][:-2]))
            f.close()

            y2 = []
            f = open(FILE_NAME.PressureTestFile + str(id) + '.HsP', 'r', encoding='utf-8')
            for i in f:
                if 'Time' in i:
                    continue
                y2.append(float(i.split('|')[1][:-2]))
            f.close()
        except:
            messagebox.showerror('file missed', cft('فایل های تست ذخیره شده پیدا نشد.'))
            return

        fig_weight = Figure(figsize=(12, 3), dpi=root.winfo_fpixels('1i'))
        plot1 = fig_weight.add_subplot(111)
        plot1.plot(y1)
        fig_weight.savefig(FILE_NAME.WeightPlot + str(id) + '.png')

        fig_Pressure = Figure(figsize=(12, 3), dpi=root.winfo_fpixels('1i'))
        plot2 = fig_Pressure.add_subplot(111)
        plot2.plot(y2)
        fig_Pressure.savefig(FILE_NAME.PressurePlot + str(id) + '.png')

        DATABASE.create_pdf(id)

    def excel_report(self):
        global DATABASE
        files = [('Excel Workbook', '*.xlsx')]
        filename = asksaveasfilename(filetypes=[('Excel Workbook', '*.xlsx')],
                                     defaultextension=[('Excel Workbook', '*.xlsx')], initialfile='HS_Report.xlsx')
        # print('h:', filename)
        if filename == '':
            return
        workbook = xlsxwriter.Workbook(filename)

        header_format = workbook.add_format()
        header_format.set_bg_color('yellow')
        header_format.set_bold()
        header_format.set_align('center')

        cell_format = workbook.add_format()
        cell_format.set_align('center')

        operator_worksheet = workbook.add_worksheet('اپراتور')
        operator_worksheet.right_to_left()

        for num, item in enumerate(DATABASE.OperatorTable):
            operator_worksheet.write(0, num, item)
            operator_worksheet.set_column(num, num, 30)
        operator_worksheet.set_row(0, 20, header_format)

        for num, item in enumerate(DATABASE.operator_data):
            for n, i in enumerate(item):
                operator_worksheet.write(num + 1, n, i)
            operator_worksheet.set_row(num + 1, 20, cell_format)

        customer_worksheet = workbook.add_worksheet('مشتری')
        customer_worksheet.right_to_left()

        for num, item in enumerate(DATABASE.CustomerTable):
            customer_worksheet.write(0, num, item)
            customer_worksheet.set_column(num, num, 30)
        customer_worksheet.set_row(0, 20, header_format)

        for num, item in enumerate(DATABASE.customer_data):
            for n, i in enumerate(item):
                customer_worksheet.write(num + 1, n, i)
            customer_worksheet.set_row(num + 1, 20, cell_format)

        report_worksheet = workbook.add_worksheet('خلاصه وضعیت تست')
        report_worksheet.right_to_left()

        for num, item in enumerate(DATABASE.ReportTable):
            report_worksheet.write(0, num, item)
            report_worksheet.set_column(num, num, 30)
        report_worksheet.set_row(0, 20, header_format)

        for num, item in enumerate(DATABASE.report_data):
            for n, i in enumerate(item):
                if n == 1 or n == 2:
                    i = cft(i)
                report_worksheet.write(num + 1, n, i)
            report_worksheet.set_row(num + 1, 20, cell_format)

        simfa_worksheet = workbook.add_worksheet('سیمفا')
        simfa_worksheet.right_to_left()

        for num, item in enumerate(DATABASE.Simfa):
            simfa_worksheet.write(0, num, item)
            simfa_worksheet.set_column(num, num, 30)
        simfa_worksheet.set_row(0, 20, header_format)

        for num, item in enumerate(DATABASE.simfa_data):
            for n, i in enumerate(item):
                simfa_worksheet.write(num + 1, n, i)
            simfa_worksheet.set_row(num + 1, 20, cell_format)

        test_worksheet = workbook.add_worksheet('انجام شده')
        test_worksheet.right_to_left()

        for num, item in enumerate(DATABASE.Test):
            test_worksheet.write(0, num, item)
            test_worksheet.set_column(num, num, 30)
        test_worksheet.set_row(0, 20, header_format)

        for num, item in enumerate(DATABASE.test_data):
            for n, i in enumerate(item):
                test_worksheet.write(num + 1, n, i)
            test_worksheet.set_row(num + 1, 20, cell_format)

        workbook.close()

        messagebox.showinfo('Report done', cft('خروجی اکسل ذخیره شد.'))

    ###### done
    def edit_done(self, list, mode):
        global DATABASE, root
        remove_item = list.selection()
        if len(remove_item) == 0:
            messagebox.showerror('No Selection', cft('لطفا ابتدا یک تست را انتخاب کنید.'))
            return
        item = list.item(list.selection()[0])['values']
        id_number = int(item[0])

        test_info = {
            'TestID': '1',
            'MaxP': '0',
            'MaxW': '0',
            'V1': '0',
            'V2': '0',
            'PE': '0',
            'TE': '0',
            'Percent': '0',
            'Mode': '0',
            'Result': '0',
            'PressureValueNormal': '0',
            'PressureTimeNormal': '0',
            'PressureValueTest': '0',
            'PressureTimeTest': '0',
            'DischargeDuration': '0',
            'Username': 'test',
            'Password': 'test',
            'VolumeFirst': '0',
            'VolumeSecond': '0',
            'ChartPT': '../..',
            'ChartVT': '../..',

        }
        test_default = {
            'TestID': StringVar(),
            'MaxP': StringVar(),
            'MaxW': StringVar(),
            'V1': StringVar(),
            'V2': StringVar(),
            'PE': StringVar(),
            'TE': StringVar(),
            'Percent': StringVar(),
            'Mode': StringVar(),
            'Result': StringVar(),
            'PressureValueNormal': StringVar(),
            'PressureTimeNormal': StringVar(),
            'PressureValueTest': StringVar(),
            'PressureTimeTest': StringVar(),
            'DischargeDuration': StringVar(),
            'Username': StringVar(),
            'Password': StringVar(),
            'VolumeFirst': StringVar(),
            'VolumeSecond': StringVar(),
            'ChartPT': StringVar(),
            'ChartVT': StringVar(),

        }

        for item in DATABASE.test_data:
            if int(item[0]) == id_number:
                for num, i in enumerate(DATABASE.Test):
                    if i in test_default:
                        test_default[i].set(item[num])
                        test_info[i] = item[num]
                break

        for item in DATABASE.simfa_data:
            if int(item[0]) == id_number:
                for num, i in enumerate(DATABASE.Simfa):
                    if i in test_default:
                        test_default[i].set(item[num])
                        test_info[i] = item[num]
                break
        if mode == 'edit':
            pass
        window = Toplevel(self.window)
        window.config(bg=COLOR.CL1)
        window.title('edit done test')
        window.geometry('+10+10')

        column = 0

        for num, item in enumerate(test_info):
            if 'Chart' in item:
                continue
            if 'Volume' in item:
                continue
            if num == 11:
                column = 2
            Label(window, text=cft(item), font=FONT.ENGLISH_SMALL, bg=COLOR.CL1, fg=COLOR.CL2, width=20).grid(
                row=num % 11,
                column=column,
                padx='5m',
                pady='5m')
            if 'TestID' in item:
                if mode == 'edit':
                    Label(window, text=cft(test_default[item].get()), font=FONT.ENGLISH_SMALL, bg=COLOR.CL1,
                          fg=COLOR.CL2,
                          width=10).grid(
                        row=num % 11,
                        column=column + 1,
                        padx='5m',
                        pady='5m')
                else:
                    sp = Spinbox(window, bg=COLOR.CL1, fg=COLOR.CL2, width=10, font=FONT.PERSIAN_SMALL,
                                 state='readonly')
                    sp.grid(row=num % 11, column=column + 1, padx='5m', pady='5m')
                    sp.config(values=(cft('ژاکت'), cft('بدون ژاکت')))

            elif 'Mode' in item:
                sp = Spinbox(window, bg=COLOR.CL1, fg=COLOR.CL2, width=10, font=FONT.PERSIAN_SMALL,
                             state='readonly', textvariable=test_default[item])
                sp.grid(row=num % 11, column=column + 1, padx='5m', pady='5m')
                sp.config(values=(cft('ژاکت'), cft('بدون ژاکت ')))

                if 'NoneJacket' in test_info[item]:
                    sp.invoke('buttonup')

            elif 'Result' in item:
                sp = Spinbox(window, bg=COLOR.CL1, fg=COLOR.CL2, width=10, font=FONT.PERSIAN_SMALL,
                             state='readonly', textvariable=test_default[item])
                sp.grid(row=num % 11, column=column + 1, padx='5m', pady='5m')
                sp.config(values=(cft('قبول'), cft('مردود')))
                if 'FAIL' in test_info[item]:
                    sp.invoke('buttonup')

            else:
                Entry(window, font=FONT.ENGLISH_SMALL, bg=COLOR.CL1, fg=COLOR.CL2, width=10,
                      textvariable=test_default[item]).grid(row=num % 11,
                                                            column=column + 1,
                                                            padx='5m', pady='5m')

        Button(window, text=cft('ویرایش'), bg=COLOR.CL1, fg=COLOR.CL2, width=15, activebackground=COLOR.CL2,
               font=FONT.PERSIAN_SMALL, command=lambda: self.edit_done_ok(None, window, test_default, id_number)).grid(
            row=13,
            column=3,
            padx='5m',
            pady='5m')
        window.bind('<Return>', lambda event: self.edit_done_ok(event, window, test_default, id_number))

    def edit_done_ok(self, event, window, value, id_number):
        global DATABASE

        try:
            int(value['MaxP'].get())
        except:
            messagebox.showerror('Wrong Value', cft('MaxP را با فرمت درست وارد کنید'), parent=window)
            return
        try:
            float(value['MaxW'].get())
        except:
            messagebox.showerror('Wrong Value', cft('MaxW را با فرمت درست وارد کنید'), parent=window)
            return

        try:
            float(value['V1'].get())
        except:
            messagebox.showerror('Wrong Value', cft('V1 را با فرمت درست وارد کنید'), parent=window)
            return

        try:
            float(value['V2'].get())
        except:
            messagebox.showerror('Wrong Value', cft('V2 را با فرمت درست وارد کنید'), parent=window)
            return
        try:
            float(value['PE'].get())
        except:
            messagebox.showerror('Wrong Value', cft('PE را با فرمت درست وارد کنید'), parent=window)
            return
        try:
            float(value['TE'].get())
        except:
            messagebox.showerror('Wrong Value', cft('TE را با فرمت درست وارد کنید'), parent=window)
            return
        try:
            float(value['Percent'].get())
        except:
            messagebox.showerror('Wrong Value', cft('Percent را با فرمت درست وارد کنید'), parent=window)
            return

        try:
            int(value['PressureValueNormal'].get())
        except:
            messagebox.showerror('Wrong Value', cft('فشار نشت یابی اول را با فرمت درست وارد کنید'), parent=window)
            return
        try:
            int(value['PressureTimeNormal'].get())
        except:
            messagebox.showerror('Wrong Value', cft('زمان نشت یابی اول را با فرمت درست وارد کنید'), parent=window)
            return

        try:
            int(value['PressureValueTest'].get())
        except:
            messagebox.showerror('Wrong Value', cft('فشار تست را با فرمت درست وارد کنید'), parent=window)
            return

        try:
            int(value['PressureTimeTest'].get())
        except:
            messagebox.showerror('Wrong Value', cft('زمان محاسبه را با فرمت درست وارد کنید'), parent=window)
            return

        try:
            int(value['DischargeDuration'].get())
        except:
            messagebox.showerror('Wrong Value', cft('زمان تخلیه مخزن را با فرمت درست وارد کنید'), parent=window)
            return

        window.destroy()
        value['VolumeFirst'].set(value['TE'].get())
        value['VolumeSecond'].set(value['PE'].get())
        DATABASE.update_done_test(id_number, value)
        self.create_list_done()

    # Destroy window

    def destroy_window(self):
        if self.window is not None:
            self.window.destroy()
            self.window = None


class Test:
    def __init__(self, window, mode, db):
        self.root = window
        self.window = None
        self.mode = mode
        self.toolbox = None
        self.showbox = None

        self.pressure_label = None
        self.weight_label = None
        self.time_label = None
        self.time_txt_label = None
        self.start_btn = None
        self.Led_canvas = None

        self.db = db

        self.pressure_val = []
        self.weight_val = []

        self.weight1 = '-'
        self.weight2 = '-'

        self.result = '-'
        self.PE = 0
        self.TE = 0
        self.Percent = 0
        self.max_w = 0
        self.max_p = 0
        self.V1 = 0
        self.V2 = 0

        self.start = False
        self.state = 0
        self.ltimer = 0

        self.test_id = StringVar()

        self.value_text = {
            'ID': 'ID',
            'ReceptionNumber': 'شماره پذیرش',
            'CylinderSN': 'شماره سریال مخزن:',
            'CylinderType': 'نوع مخزن:',
            'WeightFirst': 'وزن حک شده :',
            'PressureValueTest': 'فشار تست :',
            'CylinderCompany': 'نام شرکت تولید کننده :',
            'WeightChange': 'میزان مجاز تغییرات',
            'PressureChange': 'تلرانس قطع پمپ',
            'PressureTestOne': 'فشار تست نشتی اول'
        }

        self.value_var = {
            'ID': StringVar(),
            'ReceptionNumber': StringVar(),
            'CylinderSN': StringVar(),
            'CylinderType': StringVar(),
            'WeightFirst': StringVar(),
            'PressureValueTest': StringVar(),
            'CylinderCompany': StringVar(),
            'WeightChange': StringVar(),
            'PressureChange': StringVar(),
            'PressureTestOne': StringVar()
        }

        self.test_information = {
            'PressureValueTest': None,  # فشار آزمون
            'PressureValueTestOne': None,  # فشار تست نشتی اول
            'TimeTest': None,
            'PressureTest': None,
            'TimeCalculateOne': None,
            'TimeCalculateTwo': None,
            'ReversTimeOne': None,
            'ReversTimeTwo': None,
            'RelieveTime': None,
            'WeightChange': None,
            'PressureChange': None,
        }
        if self.mode == "NoneJacket":
            self.value_text['K'] = 'ضریب K'
            self.value_var['K'] = StringVar()
            self.test_information['K'] = None

            self.value_text['WaterWeight'] = 'وزن آب درون سیلندر'
            self.value_var['WaterWeight'] = StringVar()
            self.test_information['WaterWeight'] = None

    def create_window(self, test_number=None):
        # global root
        self.destroy_window()
        self.window = Frame(self.root, bg=COLOR.CL1, height=WINDOWS_HEIGHT)
        self.window.pack(side='top', padx='5m', pady='1m', fill='x', expand='true')

        self.showbox = Frame(self.window, bg=COLOR.CL1)
        self.showbox.pack(side='right', padx='5m', pady='5m')

        Label(self.showbox, text=cft('فشار آزمون'), font=FONT.PERSIAN_BOLD, bg=COLOR.CL1, fg=COLOR.CL2).grid(row=0,
                                                                                                             column=0,
                                                                                                             padx='20m',
                                                                                                             pady='5m')
        Label(self.showbox, text=cft('حجم آب تزریق شده'), font=FONT.PERSIAN_BOLD, bg=COLOR.CL1, fg=COLOR.CL2).grid(
            row=0,
            column=1,
            padx='20m',
            pady='5m')

        self.pressure_label = Label(self.showbox, text=cft('0'), font=FONT.ENGLISH_NUMBER, bg=COLOR.CL1, fg='#E84C3D')
        self.pressure_label.grid(row=1, column=0, padx='10m', pady='10')

        self.weight_label = Label(self.showbox, text=cft('0'), font=FONT.ENGLISH_NUMBER, bg=COLOR.CL1, fg='#E84C3D')
        self.weight_label.grid(row=1, column=1, padx='10m', pady='10')

        if self.mode == 'NoneJacket':
            self.time_txt_label = Label(self.showbox, text=cft('{}'.format(HW.temp)), font=FONT.ENGLISH_NUMBER2,
                                        bg=COLOR.CL1, fg='Blue')
            self.time_txt_label.grid(row=2, column=1, padx='5m', pady='5m')
        else:
            self.time_txt_label = Label(self.showbox, text=cft(''), font=FONT.PERSIAN_BOLD, bg=COLOR.CL1,
                                        fg=COLOR.CL2)
            self.time_txt_label.grid(row=2, column=1, padx='5m', pady='5m')

        self.Led_canvas = Canvas(self.showbox, width='65m', height='25m', bg=COLOR.CL1)
        self.Led_canvas.grid(row=2, column=0, padx='5m', pady='5m')

        self.Led_canvas.create_oval('5m', '5m', '20m', '20m', fill='red', tag='relay_led_1')
        self.Led_canvas.create_text('12m', '12m', text=cft('R1'), fill='Black', font=FONT.ENGLISH_BOLD)
        self.Led_canvas.create_oval('25m', '5m', '40m', '20m', fill='red', tag='relay_led_2')
        self.Led_canvas.create_text('32m', '12m', text=cft('R2'), fill='Black', font=FONT.ENGLISH_BOLD)
        self.Led_canvas.create_oval('45m', '5m', '60m', '20m', fill='red', tag='relay_led_3')
        self.Led_canvas.create_text('52m', '12m', text=cft('R3'), fill='Black', font=FONT.ENGLISH_BOLD)

        Button(self.showbox, text=cft('Emergency'), bg='red', fg='White', width=10, font=FONT.ENGLISH_BIG,
               activebackground='red', command=self.emergency_click).grid(row=3,
                                                                          column=0,
                                                                          padx='5m',
                                                                          pady='5m')

        # self.time_label = Label(self.showbox, text='', font=FONT.ENGLISH_NUMBER, bg=COLOR.CL1, fg=COLOR.CL2)
        # self.time_label.grid(row=3, column=1, padx='5m', pady='5m')

        self.time_label = Button(self.showbox, text=cft('Tare'), font=FONT.ENGLISH_BOLD, bg=COLOR.CL1, fg=COLOR.CL2,
                                 command=HW.send_tare, width=10)
        self.time_label.grid(row=3, column=1, padx='5m', pady='5m')

        self.toolbox = Frame(self.window, bg=COLOR.CL1)
        self.toolbox.pack(side='right', padx='5m', pady='5m')

        tests = []
        for item in DATABASE.report_data:
            if item[3] == '-':
                tests.append(item[0])
        self.value_var['ID'].set('-')

        # if test_number is None:
        #     if len(tests) == 0:
        #         test_number = 0
        #     else:
        #         test_number = tests[0]

        if len(tests) == 0:
            for item in self.value_var:
                self.value_var[item].set('-')
        else:
            data = 0
            for num, item in enumerate(DATABASE.simfa_data):
                if item[0] == test_number:
                    data = num
                    break
            for num, item in enumerate(DATABASE.Simfa):
                if item in self.value_var:
                    self.value_var[item].set(DATABASE.simfa_data[data][num])

                if item == 'PressureValueTest':
                    self.test_information['PressureTest'] = int(DATABASE.simfa_data[data][num])
                    self.value_var['PressureTestOne'].set(str(int(int(DATABASE.simfa_data[data][num]) * (2 / 3))))

        self.value_var['WeightChange'].set(Globals.DEFAULT_WEIGHT_CHANGE)
        self.value_var['PressureChange'].set(Globals.DEFAULT_PRESSURE_CHANGE)

        if self.mode == "NoneJacket":
            self.value_var['K'].set(Globals.DEFAULT_K)
            self.value_var['WaterWeight'].set(Globals.DEFAULT_WATER_WEIGHT)
        column = 0
        for num, item in enumerate(self.value_text):
            if num % 7 == 0:
                column += 2

            Label(self.toolbox, text=cft(self.value_text[item]), bg=COLOR.CL1, fg=COLOR.CL2,
                  font=FONT.PERSIAN_SMALL).grid(
                row=num % 7,
                column=column,
                padx='5m',
                pady='5m')

            if item == 'ID':
                id_bar = ttk.Combobox(self.toolbox, font=FONT.ENGLISH_SMALL, width=6, state='readonly')
                self.value_var[item].set('-')
                id_bar.config(values=tuple(tests), textvariable=self.value_var[item], justify='center')
                id_bar.grid(row=num % 7, column=column + 1, padx='5m', pady='5m')
                id_bar.bind("<<ComboboxSelected>>", lambda event: self.id_click(event, id_bar))
                # if len(tests) != 0:
                #     id_bar.current(0)
                if test_number is not None:
                    self.value_var['ID'].set(str(test_number))
            elif item == 'WeightChange' or item == 'PressureChange' or item == 'PressureTestOne' or item == 'K' or item == 'WaterWeight':
                Entry(self.toolbox, bg=COLOR.CL1, bd=1, fg=COLOR.CL2, width=10, font=FONT.ENGLISH_SMALL,
                      insertbackground=COLOR.CL2, textvariable=self.value_var[item]).grid(row=num % 7,
                                                                                          column=column + 1, padx='5m',
                                                                                          pady='5m')
            else:
                Label(self.toolbox, text=cft(self.value_var[item].get()), bg=COLOR.CL1, fg=COLOR.CL2,
                      font=FONT.ENGLISH_SMALL).grid(row=num % 7, column=column + 1, padx='5m', pady='5m')

        self.start_btn = Button(self.toolbox, text=cft('Start'), bg='green', fg='white', activebackground='green',
                                width=10,
                                font=FONT.ENGLISH_SMALL, command=self.start_btn_click)
        self.start_btn.grid(row=5, column=4)

        self.add_btn = Button(self.toolbox, text=cft('add'), bg='green', fg='white', activebackground='green',
                              width=10,
                              font=FONT.ENGLISH_SMALL, command=self.add_test_click)
        self.add_btn.grid(row=5, column=5)

        self.state = 0
        # self.finish_window()

    def add_test_click(self):
        self.db.add_test(refresh=self)
        self.destroy_window()
        self.create_window()

    def id_click(self, event, spin_box):
        self.destroy_window()
        self.create_window(int(self.value_var['ID'].get()))

    def emergency_click(self):
        global HW

        if not HW._isOpen:
            messagebox.showerror('Not Connect', cft('دستگاه متصل نمی باشد'))
            return

        if Globals.SYSTEM_TYPE == 0:
            for i in range(5):
                HW.off_all()
                sleep(0.01)
        else:
            HW.off_all()
        self.start = False
        self.state = 0
        self.destroy_window()
        self.create_window()

    def start_btn_click(self):
        global HW, DATABASE
        if not HW._isOpen:
            messagebox.showerror('Not Connect', cft('دستگاه متصل نمی باشد'))
            return

        if self.value_var['ID'].get() == '-':
            messagebox.showerror('ID test', cft('تست مورد نظر را انتخاب کنید'))
            return
        self.test_information['PressureValueTest'] = int(self.value_var['PressureValueTest'].get())
        try:
            self.test_information['PressureValueTestOne'] = int(self.value_var['PressureTestOne'].get())
        except:
            messagebox.showerror('wrong value', cft('فشار تست نشتی اول را با فرمت درست وارد کنید'))
            return

        self.test_information['TimeCalculateOne'] = int(DATABASE.setting_val['setting6'])
        self.test_information['TimeCalculateTwo'] = int(DATABASE.setting_val['setting7'])
        self.test_information['ReversTimeOne'] = int(DATABASE.setting_val['setting8'])
        self.test_information['ReversTimeTwo'] = int(DATABASE.setting_val['setting12'])
        self.test_information['RelieveTime'] = int(DATABASE.setting_val['setting18'])

        try:
            self.test_information['WeightChange'] = int(self.value_var['WeightChange'].get())
        except:
            messagebox.showerror('wrong value', cft('میزان تغییرات مجاز را با فرمت درست وارد کنید'))
            return

        try:
            self.test_information['PressureChange'] = int(self.value_var['PressureChange'].get())
        except:
            messagebox.showerror('wrong value', cft('تلرانس قطع پمپ را با فرمت درست وارد کنید'))
            return

        if self.mode == 'NoneJacket':
            try:
                self.test_information['K'] = float(self.value_var['K'].get())
            except:
                messagebox.showerror('wrong value', cft('مقدار K را با فرمت درست وارد کنید'))
                return
            try:
                self.test_information['WaterWeight'] = float(self.value_var['WaterWeight'].get())
            except:
                messagebox.showerror('wrong value', cft('وزن آب درون سیلندر را با فرمت درست وارد کنید'))
                return
            self.test_information['TimeTest'] = int(DATABASE.setting_val['setting1'])
            self.test_information['PressureTest'] = int(DATABASE.setting_val['setting2'])
        else:
            self.test_information['PressureTest'] = int(DATABASE.setting_val['setting4'])
            self.test_information['TimeTest'] = int(DATABASE.setting_val['setting3'])
            self.test_information['K'] = '-'
            self.test_information['WaterWeight'] = '-'

        # print(self.test_information)
        HW.off_all()
        self.state = 0
        self.start = True

    def destroy_window(self):
        if self.window is not None:
            self.window.destroy()
            self.window = None

    def finish_window(self):
        global DATABASE

        self.destroy_window()
        self.state = 8
        self.start = False

        self.window = Frame(self.root, bg=COLOR.CL1, height=WINDOWS_HEIGHT)
        self.window.pack(side='top', padx='5m', pady='1m', fill='x', expand='true')

        Label(self.window, text=cft('نتیجه ی آزمون'), font=FONT.PERSIAN_BIG, bg=COLOR.CL1, fg=COLOR.CL2).pack(
            side='top',
            padx='5m',
            pady='5m',
            fill='x')
        st = 'RESULT: {} \nV1 = {} , V2 = {} , PE = {} , TE ={} , Percent = {} % \n'.format(
            self.result, self.V1, self.V2, self.PE, self.TE, self.Percent)
        if self.mode == 'NoneJacket':
            st += 'Max_p : {} , Max_w : {} , K : {} , WaterWeight : {}'.format(self.max_p, self.max_w,
                                                                               self.test_information['K'],
                                                                               self.test_information['WaterWeight'])

        if self.result == 'LEAKAGE' or self.result == 'FAIL':
            Label(self.window, text=cft(st), font=FONT.ENGLISH_BOLD, bg=COLOR.CL1, fg='red').pack(sid='top', padx='5m',
                                                                                                  pady='5m', fill='x')
        else:
            Label(self.window, text=cft(st), font=FONT.ENGLISH_BOLD, bg=COLOR.CL1, fg='green').pack(sid='top',
                                                                                                    padx='5m',
                                                                                                    pady='5m', fill='x')
        btn_frame = Frame(self.window, bg=COLOR.CL1)
        btn_frame.pack(side='top', padx='5m', pady='5m', fill='x')

        Button(btn_frame, text=cft('تست جدید'), width=10, bg=COLOR.CL1, fg=COLOR.CL2, activebackground=COLOR.CL1,
               font=FONT.PERSIAN_SMALL, command=self.create_window).pack(
            side='right', padx='5m', pady='5m')
        # Button(btn_frame, text=cft('گزارش'), width=10, bg=COLOR.CL1, fg=COLOR.CL2, activebackground=COLOR.CL1,
        #        font=FONT.PERSIAN_SMALL).pack(
        #     side='right', padx='5m', pady='5m')
        Button(btn_frame, text=cft('ارسال'), width=10, bg=COLOR.CL1, fg=COLOR.CL2, activebackground=COLOR.CL1,
               font=FONT.PERSIAN_SMALL,
               command=lambda: DATABASE.send_data_to_simfa(int(self.value_var['ID'].get()))).pack(
            side='right', padx='5m', pady='5m')

        test_info = {
            'TestID': int(self.value_var['ID'].get()),
            'MaxP': str(self.max_p),
            'MaxW': str(self.max_w),
            'V1': str(self.V1),
            'V2': str(self.V2),
            'PE': str(self.PE),
            'TE': str(self.TE),
            'K': str(self.test_information['K']),
            'WaterWeight': str(self.test_information['WaterWeight']),
            'Percent': str(self.Percent),
            'Mode': self.mode,
            'Result': self.result
        }
        simfa_info = {
            'ID': int(self.value_var['ID'].get())
        }
        data = None
        for item in DATABASE.simfa_data:
            if int(self.value_var['ID'].get()) == item[0]:
                data = item
                break

        for num, item in enumerate(DATABASE.Simfa):
            if item == 'ID':
                continue
            simfa_info[item] = data[num]

        simfa_info['PressureValueNormal'] = self.test_information['PressureValueTestOne']
        simfa_info['PressureTimeNormal'] = self.test_information['ReversTimeOne']
        simfa_info['PressureValueTest'] = self.test_information['PressureValueTest']
        simfa_info['PressureTimeTest'] = self.test_information['ReversTimeTwo']
        simfa_info['VolumeFirst'] = self.TE
        simfa_info['VolumeSecond'] = self.PE
        simfa_info['DischargeDuration'] = self.test_information['RelieveTime']
        simfa_info['ChartPT'] = FILE_NAME.PressureTestFile + self.value_var['ID'].get() + '.HsP'
        simfa_info['ChartVT'] = FILE_NAME.WeightTestFile + self.value_var['ID'].get() + '.HsW'

        fw = open(FILE_NAME.WeightTestFile + self.value_var['ID'].get() + '.HsW', 'w')
        fw.write('Time\t\t|Weight\t\n')

        for num, i in enumerate(self.weight_val):
            st = '{}\t\t|{}\t\n'.format(num, i)
            fw.write(st)
        fw.close()

        fp = open(FILE_NAME.PressureTestFile + self.value_var['ID'].get() + '.HsP', 'w')
        fp.write('Time\t\t|Pressure\t\n')

        for num, i in enumerate(self.pressure_val):
            st = '{}\t\t|{}\t\n'.format(num, i)
            fp.write(st)
        fp.close()

        DATABASE.add_done_test(simfa_info, test_info, int(self.value_var['ID'].get()))

    def update(self):
        global HW, last_time
        if self.state > 0 and time.time() - self.ltimer > 1:
            self.ltimer = time.time()
            try:
                self.weight_val.append(float(HW.value))
                self.pressure_val.append(int(HW.pressure))
            except:
                pass
        if self.start:
            if self.state == 0:  # tare and set all relay off
                self.time_label.destroy()
                self.time_label = Label(self.showbox, text=cft(''), font=FONT.ENGLISH_NUMBER, bg=COLOR.CL1,
                                        fg=COLOR.CL2)
                self.time_label.grid(row=3, column=1, padx='5m', pady='5m')
                self.start_btn.grid_forget()
                self.pressure_val = []
                self.weight_val = []

                self.weight1 = '-'
                self.weight2 = '-'

                self.result = '-'
                self.PE = 0
                self.TE = 0
                self.Percent = 0
                self.max_w = 0
                self.max_p = 0
                self.V1 = 0
                self.V2 = 0

                try:

                    HW.send_tare()
                    sleep(0.1)
                    if Globals.SYSTEM_TYPE == 0:
                        HW.send_tare()
                        sleep(0.1)
                        HW.send_tare()
                        sleep(0.1)
                    HW.relay1_off()
                    HW.relay2_off()
                    HW.relay3_off()
                    if Globals.SYSTEM_TYPE == 0:
                        sleep(1)
                    HW.relay1_on()
                    if Globals.SYSTEM_TYPE == 0:
                        sleep(0.5)
                    HW.relay1_on()

                    if self.test_information['PressureValueTestOne'] == 0:
                        self.state = 3
                    else:
                        self.state = 1
                        last_time = time.time()
                except:
                    self.state = 0

            elif self.state == 1:
                try:
                    if int(HW.pressure) >= (self.test_information['PressureValueTestOne'] - self.test_information[
                        'PressureChange']):  # دو سوم عدد فشار
                        # تیک در فشار فلان تست انجام شود
                        HW.relay1_off()
                        self.state = 2
                        last_time = time.time()
                except:
                    pass

            elif self.state == 2:

                Time = time.time() - last_time
                Time = round(Time)
                self.time_label.config(text=cft(str(self.test_information['ReversTimeOne'] - Time)))

                self.time_txt_label.config(font=FONT.PERSIAN_BOLD2)
                self.time_txt_label.config(fg=COLOR.CL2)
                self.time_txt_label.config(text=cft('معکوس شمار اول'))
                if Time >= self.test_information[
                    "TimeCalculateOne"] and self.weight1 == '-':  # زمان برای ‌ذخیره عدد وزن برای نشتی
                    self.weight1 = float(HW.value)

                if Time >= self.test_information[
                    'TimeCalculateTwo'] and self.weight2 == '-':  # زمان مقایسه برای عدد وزن با وزن ذخیره شده
                    self.weight2 = float(HW.value)

                if Time >= self.test_information['ReversTimeOne']:  # طول زمان معکوس شمار اول
                    self.time_txt_label.config(text=cft(''))
                    self.state = 3
                    if self.weight1 != '-' and self.weight2 != '-':
                        if abs(self.weight1 - self.weight2) > self.test_information['WeightChange']:
                            # if abs(self.weight1 - self.weight2) >= 0:
                            self.result = 'LEAKAGE'
                            messagebox.showwarning('LEAKAGE',
                                                   cft("نشتی در دستگاه وجود دارد!دستگاه را مجددا بررسی نمایید "))
                            self.state = 0
                            self.start = False
                            self.create_window()
                            return

                    HW.relay1_on()

            elif self.state == 3:
                try:
                    if int(HW.pressure) >= self.test_information['PressureValueTest']:  # فشار تست
                        HW.relay1_off()
                        last_time = time.time()
                        self.state = 4
                except:
                    pass

            elif self.state == 4:
                global root
                Time = time.time() - last_time
                Time = round(Time)
                self.time_label.config(text=cft(str(self.test_information['ReversTimeTwo'] - Time)))
                self.time_txt_label.config(text=cft('معکوس شمار دوم'))
                self.time_txt_label.config(font=FONT.PERSIAN_BOLD)
                self.time_txt_label.config(fg=COLOR.CL2)
                if Time >= self.test_information['ReversTimeTwo']:  # طول زمان معکوس شمار دوم در فشار آزمون
                    # messagebox.showinfo('Relieve', 'لطفا شیر تخلیه را باز کنید.')
                    win = Toplevel(self.window)
                    win.title('Relieve')
                    win.config(bg='red')
                    w = int(root.winfo_width() / 2)
                    h = int(root.winfo_height() / 2)
                    # print("PR :h={},w{}".format(h, w))
                    win.geometry('+{}+{}'.format(w, h))
                    Label(win, text=cft('لطفا شیر تخلیه را باز کنید.'), bg='red', fg='white',
                          font=FONT.PERSIAN_BOLD).pack(padx='10m', pady='10m')
                    Button(win, text=cft('OK'), bg=COLOR.CL1, fg=COLOR.CL2, width=10, font=FONT.ENGLISH_BOLD,
                           command=lambda: win.destroy()).pack(padx='5m', pady='5m')

                    self.time_txt_label.config(text=cft(''))
                    self.max_p = max(self.pressure_val)
                    self.max_w = max(self.weight_val)
                    HW.relay2_on()
                    self.state = 5

            elif self.state == 5:

                try:
                    if int(HW.pressure) <= self.test_information['PressureTest']:  # فشار برای شروع محاسبه
                        self.state = 6
                        last_time = time.time()

                except:
                    pass

            elif self.state == 6:
                Time = time.time() - last_time
                Time = round(Time)
                self.time_label.config(text=cft(str(self.test_information['TimeTest'] - Time)))
                self.time_txt_label.config(text=cft('زمان محاسبه تست'))
                if Time > self.test_information['TimeTest']:  # b* طول زمان محاسبه با ژاکت
                    self.time_txt_label.config(text=cft(''))
                    self.V2 = self.weight_val[-1] + self.weight_val[-2] + self.weight_val[-3]
                    self.V2 = round(self.V2 / 3, 2)
                    self.V1 = max(self.weight_val)

                    if self.mode == 'Jacket':
                        if self.V2 <= 0:
                            self.V2 = 0.1
                        self.PE = self.V2
                        self.TE = self.V1
                    else:
                        if self.V2 >= 0.0:
                            self.V2 = -0.1
                        self.PE = abs(self.V2)
                        self.V1 = min(self.weight_val)
                        m = float(self.test_information['WaterWeight']) + abs(self.V1)

                        # deltav = m * self.test_information['PressureValueTest'] * (float(self.test_information['K']) - (
                        #         0.68 * self.test_information['PressureValueTest'] / 100000)) / 1000
                        deltav = m * self.max_p * (float(self.test_information['K']) - (
                                0.68 * self.max_p / 100000)) / 1000

                        self.TE = abs(self.V1) - deltav
                        self.TE = round(self.TE, 2)
                    try:
                        self.Percent = (self.PE / self.TE) * 100
                        self.Percent = round(self.Percent, 2)

                        if self.Percent > 10:
                            self.result = 'FAIL'
                        else:
                            self.result = 'Accept'
                    except:
                        self.result = 'FAIL'
                    last_time = time.time()
                    self.state = 7

            elif self.state == 7:
                Time = time.time() - last_time
                Time = round(Time)
                self.time_label.config(text=cft(str(self.test_information['RelieveTime'] - Time)))
                self.time_txt_label.config(text=cft('زمان بستن شیر برقی'))
                if Time > self.test_information['RelieveTime']:  # زمان قطع شیر C*
                    self.time_txt_label.config(text=cft(''))
                    HW.relay2_off()
                    self.state = 8
                    self.finish_window()
        if self.window is not None:
            if self.state == 8:
                return
            self.pressure_label.config(text=cft(str(HW.pressure)))
            self.weight_label.config(text=cft(str(HW.value)))
            if self.mode == 'NoneJacket' and self.state == 0:
                self.time_txt_label.config(text=cft(str(HW.temp)))
                self.time_txt_label.config(fg='blue')
                self.time_txt_label.config(font=FONT.ENGLISH_NUMBER2)

            if HW.control_relay['pressure'] == 'on':
                self.Led_canvas.itemconfig('relay_led_1', fill='green')
            else:
                self.Led_canvas.itemconfig('relay_led_1', fill='red')

            if HW.control_relay['relieve'] == 'on':
                self.Led_canvas.itemconfig('relay_led_2', fill='green')
            else:
                self.Led_canvas.itemconfig('relay_led_2', fill='red')

            if HW.control_relay['water'] == 'on':
                self.Led_canvas.itemconfig('relay_led_3', fill='green')
            else:
                self.Led_canvas.itemconfig('relay_led_3', fill='red')


class MakeWindow:
    def __init__(self):
        global HW, root
        HW.off_all()
        self.Window = root
        self.MainWindow = Frame(self.Window, bg=COLOR.CL1, height=WINDOWS_HEIGHT)
        self.MainWindow.pack(side='top', padx='5m', pady='1m', fill='x', expand='true')

        self.StatusBar = Label(self.Window, text="status bar", anchor='nw', font=FONT.ENGLISH_BOLD)
        self.StatusBar.pack(side='bottom', fill='x', expand='true')
        self.create_toolbox()

        self.database = Database(self.MainWindow)
        self.manual = Manual(self.MainWindow)

        self.JacketTest = Test(self.MainWindow, 'Jacket', self.database)
        self.NoneJacketTest = Test(self.MainWindow, 'NoneJacket', self.database)

        self.setting = Setting(self.MainWindow)
        # self.setting.create_window()
        self.Window.bind('<Control-Alt-Key-s>', self.setting.simfa2_visible_enable)
        self.Window.bind('<Control-Alt-Key-S>', self.setting.simfa2_visible_enable)
        self.Window.bind('<Control-Alt-Key-e>', self.database.enable_test_edit)
        self.Window.bind('<Control-Alt-Key-E>', self.database.enable_test_edit)

        self.Window.bind('<Control-Key-n>', self.database.add_test)
        self.Window.bind('<Control-Key-N>', self.database.add_test)
        self.create_manual()

        self.MenuBar = Menu(self.Window)

        filemenu = Menu(self.MenuBar, tearoff=0)
        filemenu.add_command(label=cft("ژاکت تست"), font=FONT.PERSIAN_SMALL, command=self.create_jacket_test)
        filemenu.add_command(label=cft("بدون ژاکت تست"), font=FONT.PERSIAN_SMALL, command=self.create_none_jacket_test)
        filemenu.add_separator()
        filemenu.add_command(label=cft("تست جدید"), font=FONT.PERSIAN_SMALL, command=self.database.add_test)
        filemenu.add_separator()
        filemenu.add_command(label=cft("خروج"), font=FONT.PERSIAN_SMALL, command=exit)

        SettingMenu = Menu(self.MenuBar, tearoff=0)
        SettingMenu.add_command(label=cft("تنظیمات اصلی"), font=FONT.PERSIAN_SMALL, command=self.setting.add_window)
        SettingMenu.add_command(label=cft("پورت"), font=FONT.PERSIAN_SMALL, command=HW.create_connect_window)
        SettingMenu.add_command(label=cft("مجاز تغییرات"), font=FONT.PERSIAN_SMALL, command=self.setting.edit_change)
        SettingMenu.add_separator()
        SettingMenu.add_command(label=cft("تغییر فونت"), font=FONT.PERSIAN_SMALL, command=self.change_font)
        SettingMenu.add_command(label=cft("رنگ پس زمینه"), font=FONT.PERSIAN_SMALL,
                                command=self.change_back_ground_color)

        self.MenuBar.add_cascade(label=cft('تست'), menu=filemenu, font=FONT.PERSIAN_SMALL)
        self.MenuBar.add_cascade(label=cft('تنظیمات'), menu=SettingMenu, font=FONT.PERSIAN_SMALL)
        self.Window.config(menu=self.MenuBar)

    def create_toolbox(self):
        Button(self.Window, text=cft('کنترل دستی'), width=15, height=2, bg=COLOR.CL1, fg=COLOR.CL2,
               activebackground=COLOR.CL4, font=FONT.PERSIAN_SMALL, command=self.create_manual).pack(side='right',
                                                                                                     padx='5m',
                                                                                                     pady='5m')

        Button(self.Window, text=cft('تست با ژاکت'), width=15, height=2, bg=COLOR.CL1, fg=COLOR.CL2,
               activebackground=COLOR.CL4, font=FONT.PERSIAN_SMALL, command=self.create_jacket_test).pack(side='right',
                                                                                                          padx='5m',
                                                                                                          pady='5m')

        Button(self.Window, text=cft('تست بدون ژاکت'), width=15, height=2, bg=COLOR.CL1, fg=COLOR.CL2,
               activebackground=COLOR.CL4, font=FONT.PERSIAN_SMALL, command=self.create_none_jacket_test).pack(
            side='right', padx='5m', pady='5m')

        Button(self.Window, text=cft('پایگاه داده'), width=15, height=2, bg=COLOR.CL1, fg=COLOR.CL2,
               activebackground=COLOR.CL4, font=FONT.PERSIAN_SMALL, command=self.create_database).pack(side='right',
                                                                                                       padx='5m',
                                                                                                       pady='5m')

        Button(self.Window, text=cft('تنظیمات'), width=15, height=2, bg=COLOR.CL1, fg=COLOR.CL2,
               activebackground=COLOR.CL4, font=FONT.PERSIAN_SMALL, command=self.create_setting).pack(side='right',
                                                                                                      padx='5m',
                                                                                                      pady='5m')
        Button(self.Window, text=cft('خاموش کردن'), width=15, height=2, bg=COLOR.CL1, fg=COLOR.CL2,
               activebackground=COLOR.CL4, font=FONT.PERSIAN_SMALL, command=self.power_off).pack(side='right',
                                                                                                 padx='5m',
                                                                                                 pady='5m')

    def create_database(self):
        if self.JacketTest.start or self.NoneJacketTest.start:
            return
        self.destroy_all()
        self.database.create_window()

    def create_manual(self):
        if self.JacketTest.start or self.NoneJacketTest.start:
            return
        self.destroy_all()
        self.manual.create_window()

    def create_jacket_test(self):
        if self.JacketTest.start or self.NoneJacketTest.start:
            return
        self.destroy_all()
        self.JacketTest.create_window()

    def create_none_jacket_test(self):
        if self.JacketTest.start or self.NoneJacketTest.start:
            return
        self.destroy_all()
        self.NoneJacketTest.create_window()

    def create_setting(self):
        if self.JacketTest.start or self.NoneJacketTest.start:
            return
        self.destroy_all()
        self.setting.create_window()

    def destroy_all(self):
        self.database.destroy_window()
        self.manual.destroy_window()
        self.JacketTest.destroy_window()
        self.NoneJacketTest.destroy_window()
        self.setting.destroy_window()

    def update(self):
        global HW
        self.manual.update()
        self.JacketTest.update()
        self.NoneJacketTest.update()

        st = ''
        if HW._isOpen:
            st = 'Connect'
        else:
            st = 'Not Connect'
        self.StatusBar.config(
            text=cft(
                'PORT : {}  | Baudrate : {} | status : {} | Version : {} | Date : {}'.format(HW.port_name, HW.baudrate,
                                                                                             st,
                                                                                             Globals.VERSION,
                                                                                             TODAY_DATE)))

    def power_off(self):
        if SYSTEMOS != "Windows":
            os.system("sudo poweroff")

    def change_font(self):
        window = Toplevel(self.Window)
        window.config(bg=COLOR.CL1)
        window.title("Font Size")
        window.geometry("+100+100")

        entry = []
        var = []

        for num, item in enumerate(Globals.font_size):
            if item == 'Color':
                continue
            Label(window, text=cft(item), width=15, bg=COLOR.CL1, fg=COLOR.CL2, font=FONT.ENGLISH_BOLD).grid(row=num,
                                                                                                             column=0,
                                                                                                             padx='5m',
                                                                                                             pady='5m')
            var.append(StringVar())
            var[num].set(str(Globals.font_size[item]))
            entry.append(
                Entry(window, width=15, textvariable=var[num], bg=COLOR.CL1, fg=COLOR.CL2, font=FONT.ENGLISH_BOLD)
            )
            entry[num].grid(row=num, column=1, padx='5m', pady='5m')

        Button(window, text='OK', bg=COLOR.CL1, fg=COLOR.CL2, width=15, font=FONT.ENGLISH_SMALL,
               command=lambda: self.ok_font(window, var)).grid(row=8, column=0,
                                                               padx='5m', pady='5m')

        Button(window, text='Cancel', bg=COLOR.CL1, fg=COLOR.CL2, width=15, font=FONT.ENGLISH_SMALL,
               command=lambda: self.cancel_btn(window)).grid(row=8, column=1,
                                                             padx='5m',
                                                             pady='5m')

    def ok_font(self, window, var):
        for num, item in enumerate(font_size):
            if item == 'Color':
                continue
            Globals.font_size[item] = var[num].get()
        f = open(FILE_NAME.Font_size, 'w', encoding='utf-8')
        txt = json.dumps(Globals.font_size)
        f.write(txt)
        f.close()
        messagebox.showerror('Need Restart', cft("برای اعمال تنظیمات برنامه را دوباره راه اندازی کنید"))
        window.destroy()
        exit()

    def cancel_btn(self, window):
        window.destroy()

    def change_back_ground_color(self):
        color = askcolor()[-1]
        if color is not None:
            font_size['Color'] = color
            f = open(FILE_NAME.Font_size, 'w', encoding='utf-8')
            txt = json.dumps(font_size)
            print(txt)
            f.write(txt)
            f.close()
            messagebox.showerror('Need Restart', cft("برای اعمال تنظیمات برنامه را دوباره راه اندازی کنید"))
            exit()


def update_window(Main):
    global root, HW
    HW.read_data()
    Main.update()

    root.after(10, lambda: update_window(Main))


def Main():
    global root, HW, DATABASE
    root = Tk()
    root.title(cft("HydroTest"))
    root.iconphoto(True, PhotoImage(file=ICON))
    root.tk.call('tk', 'scaling', 1.0)
    Globals.ppm = root.winfo_fpixels('1m')

    try:
        f = open(FILE_NAME.Font_size, encoding='utf-8')
        txt = f.read()
        Globals.font_size = eval(txt)
        # print(Globals.font_size)
    except:
        print("hello")
        f = open(FILE_NAME.Font_size, 'w', encoding='utf-8')
        result = json.dumps(Globals.font_size)
        f.write(result)
        f.close()
    for item in font_size:
        if item == 'Color':
            continue
        Globals.font_size[item] = int(Globals.font_size[item])

    FONT.PERSIAN_BOLD = ('B Nazanin', '{}'.format(int(Globals.font_size['PersianBold'] * Globals.ppm)), 'bold')
    FONT.PERSIAN_BIG = ('B Nazanin', '{}'.format(int(Globals.font_size['PersianBig'] * Globals.ppm)))
    FONT.PERSIAN_SMALL = ('B Nazanin', '{}'.format(int(Globals.font_size['PersianSmall'] * Globals.ppm)))

    FONT.ENGLISH_SMALL = ('Arialn', '{}'.format(int(Globals.font_size['EnglishSmall'] * Globals.ppm)))
    FONT.ENGLISH_BIG = ('Arialn', '{}'.format(int(Globals.font_size['EnglishBig'] * Globals.ppm)))
    FONT.ENGLISH_BOLD = ('Arialn', '{}'.format(int(Globals.font_size['EnglishBold'] * Globals.ppm)), 'bold')

    FONT.ENGLISH_NUMBER = ('Seven Segment', '{}'.format(int(Globals.font_size['SevenSegment1'] * Globals.ppm)), 'bold')
    FONT.ENGLISH_NUMBER2 = ('Seven Segment', '{}'.format(int(Globals.font_size['SevenSegment2'] * Globals.ppm)), 'bold')
    COLOR.CL1 = font_size['Color']
    root.config(bg=COLOR.CL1)
    root.update_idletasks()
    if SYSTEMOS == "Windows":
        root.state('zoomed')
    else:
        # root.attributes('-zoomed', True)
        root.geometry("{}x{}+0+0".format(root.winfo_screenwidth(), root.winfo_screenheight()))

    HW = HardWare()
    Main = MakeWindow()
    root.bind_all('<Control-Key-p>', HW.create_connect_window)
    root.bind_all('<Control-Key-P>', HW.create_connect_window)

    DATABASE.SimfaConnection.connect_to_simfa()
    root.after(10, lambda: update_window(Main))
    root.mainloop()


if __name__ == '__main__':

    # a = [-1, -2, -3, -4, -5, 6]

    DATABASE = DataBaseSql()

    try:
        f = open(FILE_NAME.DefaultChange, encoding='utf-8')
        txt = f.read()
        Globals.DEFAULT_WEIGHT_CHANGE = txt.split('\n')[0]
        Globals.DEFAULT_PRESSURE_CHANGE = txt.split('\n')[1]
        Globals.WEIGHT_ZARIB = float(txt.split('\n')[2])
        Globals.WEIGHT_TELO = float(txt.split('\n')[3])
        Globals.PRESSURE_ZARIB = float(txt.split('\n')[4])
        Globals.PRESSURE_TELO = float(txt.split('\n')[5])
        Globals.SHOW_NUMBER = int(txt.split('\n')[6])
        Globals.DEFAULT_K = txt.split('\n')[7]
        Globals.DEFAULT_WATER_WEIGHT = txt.split('\n')[8]
        f.close()
    except:
        f = open(FILE_NAME.DefaultChange, 'w', encoding='utf-8')
        f.write(str(Globals.DEFAULT_WEIGHT_CHANGE) + '\n')
        f.write(str(Globals.DEFAULT_PRESSURE_CHANGE) + '\n')
        f.write(str(Globals.WEIGHT_ZARIB) + '\n')
        f.write(str(Globals.WEIGHT_TELO) + '\n')
        f.write(str(Globals.PRESSURE_ZARIB) + '\n')
        f.write(str(Globals.PRESSURE_TELO) + '\n')
        f.write(str(Globals.SHOW_NUMBER) + '\n')
        f.write(str(Globals.DEFAULT_K) + '\n')
        f.write(str(Globals.DEFAULT_WATER_WEIGHT))
        f.close()
    try:
        f = open(FILE_NAME.LabConfig, encoding='utf-8')
        txt = f.read()
        Globals.LAB_NAME = txt.split('\n')[0]
        Globals.LAB_CEO = txt.split('\n')[1]
        Globals.LAB_CTO = txt.split('\n')[2]
        Globals.LAB_PHONE = txt.split('\n')[3]
        Globals.LAB_ADDRESS = txt.split('\n')[4]
        Globals.LAB_LOGO = txt.split('\n')[5]
        f.close()
    except:
        f = open(FILE_NAME.LabConfig, 'w', encoding='utf-8')
        f.write(Globals.LAB_NAME + '\n')
        f.write(Globals.LAB_CEO + '\n')
        f.write(Globals.LAB_CTO + '\n')
        f.write(Globals.LAB_PHONE + '\n')
        f.write(Globals.LAB_ADDRESS + '\n')
        f.write(Globals.LAB_LOGO)
        f.close()
    try:
        f = open(FILE_NAME.TYPE, encoding='utf-8')
        txt = f.read()
        Globals.SYSTEM_TYPE = int(txt)
        f.close()
    except:
        f = open(FILE_NAME.TYPE, 'w', encoding='utf-8')
        txt = f.write(str(Globals.SYSTEM_TYPE))
        f.close()

    TODAY_DATE = Gregorian(datetime.date.today()).persian_string()
    Main()
