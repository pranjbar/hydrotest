import os
import re
import datetime
from platform import system

from arabic_reshaper.arabic_reshaper import reshape
from bidi.algorithm import get_display

r = os.path.dirname(__file__)
ppm = 1
SYSTEMOS = system()

WEIGHT_TELO = 0
WEIGHT_ZARIB = 1

PRESSURE_TELO = 0
PRESSURE_ZARIB = 1

SHOW_NUMBER = 10

WINDOWS_HEIGHT = '150m'
VERSION = '1400/06/01'

DEFAULT_WEIGHT_CHANGE = '1'
DEFAULT_PRESSURE_CHANGE = '0'
DEFAULT_K = '0.1'
DEFAULT_WATER_WEIGHT = '1'
TODAY_DATE = ''

LAB_NAME = 'A'
LAB_CEO = 'B'
LAB_CTO = 'C'
LAB_PHONE = 'D'
LAB_ADDRESS = 'E'
LAB_LOGO = 'E'
ICON = os.path.join(r, 'Utils/ICON.png')

SYSTEM_TYPE = 0

font_size = {
        'PersianBold': '10',
        'PersianBig': '10',
        'PersianSmall': '5',
        'EnglishSmall': '3',
        'EnglishBig': '5',
        'EnglishBold': '5',
        'SevenSegment1': '33',
        'SevenSegment2': '18',
        'Color': '#ADD8E6'
    }

class FILE_NAME:
    CylinderCompany = os.path.join(r, 'ConfigFile/Brand.Hsconf')
    DefaultTestInfo = os.path.join(r, 'ConfigFile/DefaultTestInfo.Hsconf')
    SerialPort = os.path.join(r, 'ConfigFile/SerialPort.Hsconf')
    PressureTestFile = os.path.join(r, 'Tests/Pressure_Test_')
    WeightTestFile = os.path.join(r, 'Tests/Weight_Test_')
    SimfaFile = os.path.join(r, 'Simfa/Simfa_Send_')
    SimfaUser1 = os.path.join(r, 'ConfigFile/Simfa1.Hsconf')
    SimfaUser2 = os.path.join(r, 'ConfigFile/Simfa2.Hsconf')
    DefaultChange = os.path.join(r, 'ConfigFile/DefaultChange.Hsconf')
    LabConfig = os.path.join(r, 'ConfigFile/LabConfig.Hsconf')
    PressurePlot = os.path.join(r, 'Tests/Pressure_Plot_')
    WeightPlot = os.path.join(r, 'Tests/Weight_Plot_')
    PdfFile = os.path.join(r, 'PDF/TEST_')
    DataBase = os.path.join(r, 'HStest.db')
    TYPE = os.path.join(r, 'ConfigFile/Type.Hsconf')
    Font_farsi = os.path.join(r, 'Utils/B Nazanin.ttf')
    Font_size = os.path.join(r, 'ConfigFile/FontSize.Hsconf')


class COLOR:
    # CL1 = '#2C3F50'
    # CL2 = '#E84C3D'
    # CL1 = '#D3D4D4'
    CL1 = '#ADD8E6'
    CL2 = 'black'
    CL3 = '#ECF0F1'
    CL4 = '#3297DB'
    CL5 = '#060606'


class FONT:
    PERSIAN_BOLD = ('B Nazanin', '45', 'bold')
    PERSIAN_BOLD2 = ('B Nazanin', '30', 'bold')
    PERSIAN_BIG = ('Arialn', '45')
    PERSIAN_SMALL = ('Arialn', '20')

    ENGLISH_SMALL = ('Arialn', '20')
    ENGLISH_BIG = ('Arialn', '35')
    ENGLISH_BOLD = ('Arialn', '20', 'bold')

    ENGLISH_NUMBER = ('Seven Segment', '120', 'bold')
    ENGLISH_NUMBER2 = ('Seven Segment', '90', 'bold')


class Gregorian:

    def __init__(self, *date):
        # Parse date
        if len(date) == 1:
            date = date[0]
            if type(date) is str:
                m = re.match(r'^(\d{4})\D(\d{1,2})\D(\d{1,2})$', date)
                if m:
                    [year, month, day] = [int(m.group(1)), int(m.group(2)), int(m.group(3))]
                else:
                    raise Exception("Invalid Input String")
            elif type(date) is datetime.date:
                [year, month, day] = [date.year, date.month, date.day]
            elif type(date) is tuple:
                year, month, day = date
                year = int(year)
                month = int(month)
                day = int(day)
            else:
                raise Exception("Invalid Input Type")
        elif len(date) == 3:
            year = int(date[0])
            month = int(date[1])
            day = int(date[2])
        else:
            raise Exception("Invalid Input")

        # Check the validity of input date
        try:
            datetime.datetime(year, month, day)
        except:
            raise Exception("Invalid Date")

        self.gregorian_year = year
        self.gregorian_month = month
        self.gregorian_day = day

        # Convert date to Jalali
        d_4 = year % 4
        g_a = [0, 0, 31, 59, 90, 120, 151, 181, 212, 243, 273, 304, 334]
        doy_g = g_a[month] + day
        if d_4 == 0 and month > 2:
            doy_g += 1
        d_33 = int(((year - 16) % 132) * .0305)
        a = 286 if (d_33 == 3 or d_33 < (d_4 - 1) or d_4 == 0) else 287
        if (d_33 == 1 or d_33 == 2) and (d_33 == d_4 or d_4 == 1):
            b = 78
        else:
            b = 80 if (d_33 == 3 and d_4 == 0) else 79
        if int((year - 10) / 63) == 30:
            a -= 1
            b += 1
        if doy_g > b:
            jy = year - 621
            doy_j = doy_g - b
        else:
            jy = year - 622
            doy_j = doy_g + a
        if doy_j < 187:
            jm = int((doy_j - 1) / 31)
            jd = doy_j - (31 * jm)
            jm += 1
        else:
            jm = int((doy_j - 187) / 30)
            jd = doy_j - 186 - (jm * 30)
            jm += 7
        self.persian_year = jy
        self.persian_month = jm
        self.persian_day = jd

    def persian_tuple(self):
        return self.persian_year, self.persian_month, self.persian_day

    def persian_string(self, date_format="{}-{}-{}"):
        return date_format.format(self.persian_year, self.persian_month, self.persian_day)


class Persian:

    def __init__(self, *date):
        # Parse date
        if len(date) == 1:
            date = date[0]
            if type(date) is str:
                m = re.match(r'^(\d{4})\D(\d{1,2})\D(\d{1,2})$', date)
                if m:
                    [year, month, day] = [int(m.group(1)), int(m.group(2)), int(m.group(3))]
                else:
                    raise Exception("Invalid Input String")
            elif type(date) is tuple:
                year, month, day = date
                year = int(year)
                month = int(month)
                day = int(day)
            else:
                raise Exception("Invalid Input Type")
        elif len(date) == 3:
            year = int(date[0])
            month = int(date[1])
            day = int(date[2])
        else:
            raise Exception("Invalid Input")

        # Check validity of date. TODO better check (leap years)
        if year < 1 or month < 1 or month > 12 or day < 1 or day > 31 or (month > 6 and day == 31):
            raise Exception("Incorrect Date")

        self.persian_year = year
        self.persian_month = month
        self.persian_day = day

        # Convert date
        d_4 = (year + 1) % 4
        if month < 7:
            doy_j = ((month - 1) * 31) + day
        else:
            doy_j = ((month - 7) * 30) + day + 186
        d_33 = int(((year - 55) % 132) * .0305)
        a = 287 if (d_33 != 3 and d_4 <= d_33) else 286
        if (d_33 == 1 or d_33 == 2) and (d_33 == d_4 or d_4 == 1):
            b = 78
        else:
            b = 80 if (d_33 == 3 and d_4 == 0) else 79
        if int((year - 19) / 63) == 20:
            a -= 1
            b += 1
        if doy_j <= a:
            gy = year + 621
            gd = doy_j + b
        else:
            gy = year + 622
            gd = doy_j - a
        for gm, v in enumerate([0, 31, 29 if (gy % 4 == 0) else 28, 31, 30, 31, 30, 31, 31, 30, 31, 30, 31]):
            if gd <= v:
                break
            gd -= v

        self.gregorian_year = gy
        self.gregorian_month = gm
        self.gregorian_day = gd

    def gregorian_tuple(self):
        return self.gregorian_year, self.gregorian_month, self.gregorian_day

    def gregorian_string(self, date_format="{}-{}-{}"):
        return date_format.format(self.gregorian_year, self.gregorian_month, self.gregorian_day)

    def gregorian_datetime(self):
        return datetime.date(self.gregorian_year, self.gregorian_month, self.gregorian_day)


def isPersian(text):
    if text in str(text.encode('utf-8')):
        # print(text)
        return False
    else:
        return True


def cft(txt):
    if SYSTEMOS == "Windows":
        return txt
    else:
        return get_display(reshape(txt))
